SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `fd_galeria`
-- ----------------------------
DROP TABLE IF EXISTS `fd_galeria`;
CREATE TABLE `fd_galeria` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `destaque` int(1) NOT NULL DEFAULT '0',
  `mais_vendido` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `order` int(3) NOT NULL DEFAULT '1',
  `id_galeria_categorias` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for `fd_galeria_categorias`
-- ----------------------------
DROP TABLE IF EXISTS `fd_galeria_categorias`;
CREATE TABLE `fd_galeria_categorias` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `tag` varchar(60) NOT NULL,
  `creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `order` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for `fd_galeria_imagens`
-- ----------------------------
DROP TABLE IF EXISTS `fd_galeria_imagens`;
CREATE TABLE `fd_galeria_imagens` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) DEFAULT NULL,
  `file_name` varchar(150) NOT NULL,
  `creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `order` tinyint(2) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `galeria_id` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for `fd_users`
-- ----------------------------
DROP TABLE IF EXISTS `fd_users`;
CREATE TABLE `fd_users` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `user` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `last_login` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of fd_users
-- ----------------------------
INSERT INTO `fd_users` VALUES ('1', 'fator', '9082ba3efef15e4070388182f3e82b2df3fb3c62', 'Fator Digital', 'roberto@fatordigital.com.br', '2014-02-19 20:26:26', '1', '0000-00-00 00:00:00');
