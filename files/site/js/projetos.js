 var projetos = {
    abrir: function() {
        $('.abre-projeto').on('click touchstart', function(e){
            e.preventDefault();
            var $categoria = $(this).data('categoria'),
                $id = $(this).data('id');

            $('#'+$categoria+'-'+$id).fadeIn(500);

            $('.trabalho-'+$categoria).trigger('click');
        });
    },
    fechar: function() {
        $('.fechar-projeto').on('click touchstart', function(e){
            e.preventDefault();
            var $id = $(this).attr('id');

            $id = $id.replace('fechar-','');

            $('#'+$id).fadeOut(500);

        });
    }
};