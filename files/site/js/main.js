$(document).ready(function(){
    projetos.abrir();
    projetos.fechar();

    biografia.abrir();
    biografia.fechar();

    novidades.abrir();
    novidades.fechar();

    $('.logo-topo').on('click', function(e){
        e.preventDefault();
        $('html,body').animate({
              scrollTop: 0
                }, 1000);
    });

    $("form#contato").validate({
        rules:{
            nome:{
                required: true
            },
            email:{
                required: true, email: true
            },
            telefone: {
                required: true
            },
            mensagem: {
                required: true
            }
        },
        submitHandler: function(form) {
            var $inputs = $(form).serialize(),
                $action = $(form).attr('action'),
                $method = $(form).attr('method');
            $.ajax({
                type: $method,
                url: $action,
                data: $inputs
            })
            .done(function(response){
                if (response == 'true') {
                    $('form#contato')[0].reset();
                    $('p.errors').css('background','#13c500').html('Mensagem enviada com sucesso.').fadeIn(500);
                } else {
                    $('p.errors').html('Houve um erro ao tentar enviar a sua mensagem, por favor, tente novamente.').fadeIn(500);
                }
            });
            return false;
        },
        invalidHandler: function(e, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                $("p.errors").html('*Preencha ou corrija os campos marcados acima.').fadeIn(500);
            } else {
                $("p.errors").fadeOut(500);
            }
        },
    });

    $("form#newsletter").validate({
        rules:{
            email:{
                required: true, email: true
            }
        },
        submitHandler: function(form) {
            var $inputs = $(form).serialize(),
                $action = $(form).attr('action'),
                $method = $(form).attr('method');
            $.ajax({
                type: $method,
                url: $action,
                data: $inputs
            })
            .done(function(response){
                if (response == 'true') {
                    $('form#newsletter')[0].reset();
                    $('p.errors-n').css('background','#13c500').html('E-mail cadastro com sucesso').fadeIn(500);
                } else {
                    $('p.errors-n').html('Este e-mail já consta em nosso banco de dados.').fadeIn(500);
                }
            });
        },
        invalidHandler: function(e, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                $("p.errors-n").html('*Preencha ou corrija os campos marcados acima.').fadeIn(500);
            } else {
                $("p.errors-n").fadeOut(500);
            }
        },
    });

    $('section.novidades .novidade').hover(function(){
        $(this).children('a').css({'text-decoration':'underline !important'});
    });

    //var baseURL = $('base').attr('href');

    if($('body').hasClass('body-novidade')) {
        $(window).load(function(){
            if($('#novidades').show()) {
            $('html,body').animate({
              scrollTop: $('[name="novidade"]').offset().top - 90
                }, 1000);
            }

        });
    } else if($('body').hasClass('body-bibliografia')) {
         $(window).load(function(){
            $('html,body').animate({
              scrollTop: $('.bib').offset().top - 90
                }, 1000);
        });
    }

    $('#controla-tabs').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 120,
        itemMargin: 5,
        asNavFor: '#tabs'
    });

    $('#tabs').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        sync: "#controla-tabs"
    }); 

    $('.flex-direction-nav a').empty();

    $('section.barra-topo img.menu-mobile').click(function(){
        $('section.barra-topo div.menu-mobile').slideToggle(500);
    });

    $(".lista-comites").owlCarousel({
      //autoPlay: 3000, //Set AutoPlay to 3 seconds
      items : 3,
      navigation: true
    });

    $('body').on('click','a[href*=#]:not([href=#]):not([href*=#novidade-])', function(e) {
        e.preventDefault();
        var $hash = this.hash;
        $hash = $hash.replace('#','');
        $hash = $('[name=' + $hash +']');
        $('html,body').animate({
              scrollTop: $hash.offset().top - 85
            }, 1000);
        /*if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          console.log(target);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html,body').animate({
              scrollTop: target.offset().top
            }, 1000);

            return false;
          }
        }*/
    });
    /*$(".slider-novidades").tabs({fx:{opacity: "toggle"}}).tabs("rotate", 5000, true);*/

    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    }

});