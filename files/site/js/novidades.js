var novidades = {
	abrir: function() {
		$('.abre-novidade').on('click', function(e){
			e.preventDefault();
			var $id = $(this).attr('id'),
				$url = $(this).data('url');

			$url = $url.replace(baseURL, '');

	        history.pushState({}, '', $url);

	        $('.novidade-ajax').hide();

			$('#novidade-'+$id).fadeIn(500);

			$('#novidade').click();
		});
		/*$('body').on('click touchstart', '.abre-novidade', function(e){
	        e.preventDefault();
	        var $interna = $('.ajax-novidade'),
	            $url = $(this).data('url'),
	            $id = $(this).attr('id'),
	            $link = 'ajax-novidade/'+$id;

	        $url = $url.replace(baseURL, '');

	        history.pushState({}, '', $url);
	        $interna.delay(400).fadeIn(600).load($link).fadeIn(600);
	    });*/
	},
	fechar: function() {
		$('.fechar').on('click', function(e){
			e.preventDefault();
            var $id = $(this).attr('id');

            $id = $id.replace('fechar-novidade-','');

            history.pushState({}, '', '');

            $('#novidade-'+$id).fadeOut(500);
		});
		/*$('body').on('click touchstart', '.fechar', function(e){
	        e.preventDefault();
	        var $body = $('.novidades .container'),
	            $lista = $('.novidades .container .lista'),
	            $interna = $('.ajax-novidade'),
	            $url = $(this).attr('href');

	        $interna
	            .fadeOut(600);

	        if($.browser.msie == true && $.browser.version < 10.0) {

	            window.location.hash = '#!' + $url;

	        } else {

	            history.pushState({}, '', $url);

	        }
	    });*/
	}
};