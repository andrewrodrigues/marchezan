<?php

class GaleriaController extends FD_Controller {

    public function __construct() {
        $this->setMenu('default_menu');
        parent::__construct();
        if(!$this->isLoggedIn()){ redirect(base_url().'fatorcms');}
    }

    public function index(){
        $this->load->model('galeria_model','_galeria');
        $this->loadCSSVendor(vendor_dynamic_tables_css());
        $this->loadJSVendorBottom(vendor_dynamic_tables_js());
        $this->loadJSBottom(dynamic_tables_js());
        $data = array();
        $data['galerias'] = $this->_galeria->getGalerias();
        $this->load->view('galeria',$data);
    }

    public function adicionar(){
        $this->load->model('galeria_model','_galeria');
        if($this->input->post('galeria_adicionar')){
            if($this->_galeria->insertGaleria(
                array(
                    'name'=>$this->input->post('galeria_nome'),
                    'destaque'=>$this->input->post('galeria_destaque'),
                    'mais_vendido'=>$this->input->post('galeria_maisvendidos'),
                    'status'=>$this->input->post('galeria_status'),
                    'id_galeria_categorias'=>$this->input->post('galeria_categoria')
                )
            )){
               $this->session->set_flashdata('success_msg',true);
            }else{
               $this->session->set_flashdata('error_msg',true);
            }
            redirect(base_url().'fatorcms/galeria');
        }else{
            $data = array();
            $data['galeria_categorias'] = $this->_galeria->getGaleriaCategorias();
            $this->load->view('galeria_adicionar_view',$data);
        }
    }

    public function editar($id){
        $data = array();
        $this->loadJS('jquery-ui-1.10.4.sortable.min');

        $this->load->model('galeria_model','_galeria');
        if($this->input->post('galeria_editar')==="true"){
            if($this->_galeria->updateGaleria($id,
                array(
                    'name' => $this->input->post('galeria_nome'),
                    'destaque' => $this->input->post('galeria_destaque'),
                    'mais_vendido' => $this->input->post('galeria_maisvendidos'),
                    'status' => $this->input->post('galeria_status'),
                    'id_galeria_categorias' => $this->input->post('galeria_categoria')
                )
            )){
               $this->session->set_flashdata('update_msg',true);
            }else{
               $this->session->set_flashdata('error_msg',true);
            }
            redirect(base_url().'fatorcms/galeria');
        }else{
            $data['galeria_info'] = $this->_galeria->getGaleriaInfo($id);
            $data['galeria_imagens'] = $this->_galeria->getGaleriaImagens($id);
            $data['galeria_categorias'] = $this->_galeria->getGaleriaCategorias();
        }
        $this->load->view('galeria_editar_view',$data);
    }

    public function excluir($id){
        $this->isAjax();
        $this->load->model('galeria_model','_galeria');
        if($this->_galeria->deleteGaleria($id)){
            $this->session->set_flashdata('delete_success',true);
        }else{
            $this->session->set_flashdata('error_msg',true);
        }
        redirect(base_url().'fatorcms/galeria');
    }

    public function excluir_imagem($id,$id_galeria){
      $this->isAjax();
      $this->load->model('galeria_model','_galeria');
      if($this->_galeria->deleteImagem($id)){
        $this->session->set_flashdata('imagem_delete_success',true);
      }else{
        $this->session->set_flashdata('imagem_delete_error_msg',true);
      }
      $this->session->set_flashdata('tab_visualizar_imagens',true);
      redirect(base_url().'fatorcms/galeria/editar/'.$id_galeria);
    }

    public function enviar_imagens(){
        $this->load->model('galeria_model','_galeria');
        $data = array();
        $data['galerias'] = $this->_galeria->getGalerias();
        $this->load->view('galeria_envia_imagens_view',$data);
    }

    public function upload(){
        $this->isAjax();
        $this->load->library("uploadhandler");
    }

    public function salvar_imagem($galeria,$imagem){
        $this->isAjax();
        $this->load->model('galeria_model','_galeria');
        if($this->_galeria->insertImagem($galeria,$imagem)){
            return true;
        }else{return false;}
    }

    public function salvar_imagens_info(){
        $this->isAjax();
        $this->load->model('galeria_model','_galeria');
        $array_imgs = explode(',',$this->input->post('imagens_order'));
        $order = 1;
        foreach($array_imgs as $img){
            if($this->_galeria->saveImagemOrder($img,array('order'=>$order))){ $return = true; }else{ $return = false; };
            $order++;
        }
        if($return){
            $this->session->set_flashdata('update_msg',true);
            $this->session->set_flashdata('tab_visualizar_imagens',true);
        }else{
            $this->session->set_flashdata('error_msg',true);
            $this->session->set_flashdata('tab_visualizar_imagens',true);
        }
        redirect(base_url().'fatorcms/galeria/editar/'.$this->input->post('galeria_id'));

    }


}