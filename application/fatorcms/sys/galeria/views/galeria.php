<?php //debugVarAndDie($galerias); ?>

<!-- modal : delete galeria confirmation -->
<div class="modal fade" id="deleteConfirmation">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Excluir Galeria</h4>
            </div>
            <div class="modal-body">
                <p>Você deseja excluir a galeria <strong rel="modal_galeria_nome">galeria_nome</strong> e todas as suas imagens?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-warning" onclick="">Excluir</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">

                <ul class="breadcrumb">
                    <li><a href="<?php getLink(); ?>"><i class="fa fa-home"></i>Home</a></li>
                    <li><a href="<?php getLink('galeria'); ?>">Galeria de Imagens</a></li>
                    <li class="active">Gerenciar Galeria</li>
                </ul>

                <?php if($this->session->flashdata('delete_success')){ ?>
                    <div class="alert alert-success fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Sucesso!</strong> Sua galeria foi excluída com sucesso.
                    </div>
                <?php } ?>

                <?php if($this->session->flashdata('success_msg')){ ?>
                <div class="alert alert-success fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="fa fa-times"></i>
                    </button>
                    <strong>Sucesso!</strong> Sua galeria foi adicionada com sucesso.
                </div>
                <?php } ?>

                <?php if($this->session->flashdata('update_msg')){ ?>
                    <div class="alert alert-success fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Sucesso!</strong> Sua galeria foi atualizada com sucesso.
                    </div>
                <?php } ?>

                <?php if($this->session->flashdata('error_msg')){ ?>
                <div class="alert alert-danger fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="fa fa-times"></i>
                    </button>
                    <strong>Erro!</strong> Ocorreu um erro ao efetuar esta operação.
                </div>
                <?php } ?>

                <section class="panel">
                    <header class="panel-heading">
                        Galeria de Imagens
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                         </span>
                    </header>
                    <div class="panel-body">

                        <div class="panel-body">
                        <div class="btn-group pull-right">
                            <button class="btn btn-primary" onclick="location.href='<?php getLink('galeria/adicionar'); ?>'" type="button">Nova Galeria</button>
                        </div>
                        </div>

                        <div class="panel-body">
                        <div class="adv-table">
                        <table  class="display table table-bordered table-striped" id="dynamic-table">
                        <thead>
                        <tr>
                            <th>Nome</th>
                            <!--<th>Categoria</th>-->
                            <th>Imagens</th>
                            <th>Destaque</th>
                            <th>Mais Vendido</th>
                            <th>Status</th>
                            <th>Data de Criação</th>
                            <th width="140">Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($galerias as $galeria){ ?>
                        <tr>
                            <td><?=$galeria['name']?></td>
                            <!--<td><?=$galeria['categoria_nome']?></td>-->
                            <td><?=$galeria['total_imagens']?></td>
                            <td>
                                <?php
                                if($galeria['destaque']=='1'){
                                    echo '<span class="label label-success">Sim</span>';
                                }else { echo '<span class="label label-danger">Não</span>'; }
                                ?>
                            </td>
                            <td>
                                <?php
                                if($galeria['mais_vendido']=='1'){
                                    echo '<span class="label label-success">Sim</span>';
                                }else { echo '<span class="label label-danger">Não</span>'; }
                                ?>
                            </td>
                            <td>
                                <?php
                                if($galeria['status']=='1'){
                                    echo '<span class="label label-success">Online</span>';
                                }else { echo '<span class="label label-danger">Offline</span>'; }
                                ?>
                            </td>
                            <td><?=date("d/m/Y - H:s",strtotime($galeria['creation']))?></td>
                            <td>
                                <button class="btn btn-warning btn-xs" onclick="location.href='<?php getLink('galeria/editar/'.$galeria['id']); ?>'" type="button">Editar</button>
                                <button class="btn btn-danger btn-xs" onclick="javascript:showDeleteConfirmation('<?=$galeria['id']?>','<?=$galeria['name'];?>');" type="button">Excluir</button>
                            </td>
                        </tr>
                        <?php } ?>
                        </tbody>
<!--                        <tfoot>
                        <tr>
                            <th>Rendering engine</th>
                            <th>Browser</th>
                            <th>Platform(s)</th>
                            <th class="hidden-phone">Engine version</th>
                            <th class="hidden-phone">CSS grade</th>
                        </tr>
                        </tfoot>-->
                        </table>
                        </div>
                        </div>

                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>

<script type="text/javascript">
    function showDeleteConfirmation(id,name){
        $('#deleteConfirmation').modal('show');
        $('#deleteConfirmation').find('[rel="modal_galeria_nome"]').text(name);
        $('#deleteConfirmation').find('.btn-warning').attr('onclick','javascript:deleteConfirmation('+id+');');
    }
    function deleteConfirmation(id){
        location.href='<?php getLink('galeria/excluir/');?>'+id;
    }


</script>