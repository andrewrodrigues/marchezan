<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-sm-12">

                <ul class="breadcrumb">
                    <li><a href="<?php getLink(); ?>"><i class="fa fa-home"></i>Home</a></li>
                    <li><a href="<?php getLink('galeria'); ?>">Galeria de Imagens</a></li>
                    <li class="active">Editar Galeria</li>
                </ul>

                <?php if($this->session->flashdata('update_msg')){ ?>
                    <div class="alert alert-success fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Sucesso!</strong> Sua galeria foi atualizada com sucesso.
                    </div>
                <?php } ?>

                <?php if($this->session->flashdata('imagem_delete_success')){ ?>
                  <div class="alert alert-success fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                      <i class="fa fa-times"></i>
                    </button>
                    <strong>Sucesso!</strong> Imagem excluída com sucesso.
                  </div>
                <?php } ?>

                <?php if($this->session->flashdata('error_msg')){ ?>
                    <div class="alert alert-danger fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Erro!</strong> Ocorreu um erro ao efetuar esta operação.
                    </div>
                <?php } ?>

                <section class="panel">
                    <header class="panel-heading tab-bg-dark-navy-blue ">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#galeria">Galeria</a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#visualizar-imagens">Visualizar Imagens</a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#enviar-imagens">Enviar Imagens</a>
                            </li>
                        </ul>
                    </header>
                    <div class="panel-body">
                        <!-- Galeria Info -->
                        <div class="tab-content">
                            <div id="galeria" class="tab-pane active">
                                <form role="form" method="post" action="">
                                    <div class="form-group">
                                        <label for="">Nome</label>
                                        <input type="text" name="galeria_nome" class="form-control" id="exampleInputEmail1" <?=$galeria_info[0]['name']!='' ? 'value="'.$galeria_info[0]['name'].'"' : 'placeholder="Nome da Galeria"';?>>
                                    </div>

                                    <!--
                                    <div class="form-group">
                                        <label for="">Categoria</label>
                                        <select name="galeria_categoria" class="form-control m-bot15" autocomplete="off">
                                            <?php foreach($galeria_categorias as $categoria){ ?>
                                            <option value="<?=$categoria['id'];?>" <?=$galeria_info[0]['id_galeria_categorias']==$categoria['id'] ? 'selected="selected"' : '';?>><?=$categoria['name'];?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    -->
                                    <div class="form-group">
                                        <label for="">Destaque</label>
                                        <select name="galeria_destaque" class="form-control m-bot15" autocomplete="off">
                                            <option value="0" <?=$galeria_info[0]['destaque']==0 ? 'selected="selected"' : '';?>>Não</option>
                                            <option value="1" <?=$galeria_info[0]['destaque']==1 ? 'selected="selected"' : '';?>>Sim</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Mais Vendidos</label>
                                        <select name="galeria_maisvendidos" class="form-control m-bot15" autocomplete="off">
                                            <option value="0" <?=$galeria_info[0]['mais_vendido']==0 ? 'selected="selected"' : '';?>>Não</option>
                                            <option value="1" <?=$galeria_info[0]['mais_vendido']==1 ? 'selected="selected"' : '';?>>Sim</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Status</label>
                                        <select name="galeria_status" class="form-control m-bot15">
                                            <option value="1" <?=$galeria_info[0]['status']==1 ? 'selected="selected"' : '';?>>Online</option>
                                            <option value="0" <?=$galeria_info[0]['status']==0 ? 'selected="selected"' : '';?>>Offline</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="galeria_editar" value="true" />
                                        <button type="submit" class="btn btn-info">Salvar</button>
                                    </div>
                                 </form>
                            </div>
                            <!-- End :: Galeria Info -->

                            <!-- Galeria Visualizar Imagens -->
                            <div id="visualizar-imagens" class="tab-pane">
                                <form action="<?php getLink('galeria/salvar-imagens-order') ?>" method="post">
                                    <div class="btn-group pull-right">
                                        <button class="btn btn-primary" type="submit">Salvar Ordem</button>
                                    </div>
                                    Clique sob uma imagem e arraste-a entre as outras para alterar sua ordem.
                                    <div id="gallery" class="media-gal">
                                        <?php foreach($galeria_imagens as $imagens){ ?>
                                        <div class="images item" rel="<?=$imagens['id'];?>">
                                            <a href="javascript:void();" data-toggle="modal">
                                                <img src="<?php getUploadedFile($imagens['file_name']); ?>" alt="" />
                                            </a>

                                            <div class="panel-body">
                                            <a class="btn btn-danger btn-xs" href="<?php getLink('galeria/excluir-imagem/'.$imagens['id'].'/'.$galeria_info[0]['id']); ?>">Excluir</a>
                                            </div>
                                        </div>
                                        <?php } ?>
                                     </div>
                                    <input type="hidden" id="input_imagens_order" name="imagens_order" value="" />
                                    <input type="hidden" name="galeria_id" value="<?=$galeria_info[0]['id'];?>" />
                                </form>
                            </div>
                            <!-- End :: Galeria Visualizar Imagens -->

                            <!-- Galeria Enviar Imagens -->
                            <div id="enviar-imagens" class="tab-pane">
                                <section class="panel">
                                    <div class="panel-body">
                                        <div id="fileupload">
                                            <!-- Upload function on action form -->
                                            <form action="<?php getLink('galeria/enviar-imagens/upload'); ?>" id="form_envia_imagens" method="POST" enctype="multipart/form-data">
                                                <input type="hidden" name="form_envia_imagem_id" value="<?=$galeria_info[0]['id'];?>" />
                                                <div class="form-group">
                                                    <label></label><br/>
                                                    Imagens: .jpg, .jpeg, .png e .gif  / Tamanho: 635px (altura) por 450px (largura)
                                                </div>
                                                <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                                                <div class="row fileupload-buttonbar">
                                                    <div class="col-lg-7">
                                                        <!-- The fileinput-button span is used to style the file input field as button -->
                                                        <span class="btn btn-success fileinput-button">
                                                            <i class="fa fa-plus"></i>
                                                            <span>Adicionar arquivos...</span>
                                                            <input type="file" name="files[]" multiple>
                                                        </span>
                                                        <button type="submit" class="btn btn-primary start">
                                                            <i class="fa fa-upload"></i>
                                                            <span>Enviar Todos</span>
                                                        </button>
                                                        <button type="reset" class="btn btn-warning cancel">
                                                            <i class="fa fa-times"></i>
                                                            <span>Cancelar Envio</span>
                                                        </button>
                                                        <!--
                                                        <button type="button" class="btn btn-danger delete">
                                                            <i class="glyphicon glyphicon-trash"></i>
                                                            <span>Excluir</span>
                                                        </button>
                                                        <input type="checkbox" class="toggle">
                                                        -->
                                                        <!-- The loading indicator is shown during file processing -->
                                                        <span class="fileupload-loading"></span>
                                                    </div>
                                                    <!-- The global progress information -->
                                                    <div class="col-lg-5 fileupload-progress fade">
                                                        <!-- The global progress bar -->
                                                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                                            <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                                        </div>
                                                        <!-- The extended global progress information -->
                                                        <div class="progress-extended">&nbsp;</div>
                                                    </div>
                                                </div>
                                                <!-- The table listing the files available for upload/download -->
                                                <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>

                                            </form>
                                        </div>
                                        <!-- The blueimp Gallery widget -->
                                        <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
                                            <div class="slides"></div>
                                            <h3 class="title"></h3>
                                            <a class="prev">‹</a>
                                            <a class="next">›</a>
                                            <a class="close">×</a>
                                            <a class="play-pause"></a>
                                            <ol class="indicator"></ol>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <!-- End :: Galeria Enviar Imagens -->

                        </div>
                    </div>
                </section>



                <!-- old code down here -->





    </div>
    </div>
    <!-- page end-->
</section>
</section>

<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="fs fa-times"></i>
                    <span>Excluir</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="fs fa-times"></i>
                    <span>Cancelar</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>

<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
        <tr class="template-upload fade">
            <td>
                <span class="preview"></span>
            </td>
            <td>
                <p class="name">{%=file.name%}</p>
                {% if (file.error) { %}
                    <div><span class="label label-danger">Error</span> {%=file.error%}</div>
                {% } %}
            </td>
            <td>
                <p class="size">{%=o.formatFileSize(file.size)%}</p>
                {% if (!o.files.error) { %}
                    <div class="progress progress-striped active" role="progressbar" rel="{%=file.name%}" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
                {% } %}
            </td>
            <td class="progress-status-text" rel="{%=file.name%}">
                {% if (!o.files.error && !i && !o.options.autoUpload) { %}
                    <button class="btn btn-primary start">
                        <i class="fa fa-upload"></i>
                        <span>Enviar</span>
                    </button>
                {% } %}
                {% if (!i) { %}
                    <button class="btn btn-warning cancel">
                        <i class="fa fa-times"></i>
                        <span>Cancelar</span>
                    </button>
                {% } %}
            </td>
        </tr>
    {% } %}
</script>

<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="<?php echo base_url(); ?>files/fatorcms/vendor_packages/fileupload/js/vendor/jquery.ui.widget.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="http://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="http://blueimp.github.io/JavaScript-Load-Image/js/load-image.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="http://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- blueimp Gallery script -->
<script src="http://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="<?php echo base_url(); ?>files/fatorcms/vendor_packages/fileupload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="<?php echo base_url(); ?>files/fatorcms/vendor_packages/fileupload/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="<?php echo base_url(); ?>files/fatorcms/vendor_packages/fileupload/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="<?php echo base_url(); ?>files/fatorcms/vendor_packages/fileupload/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="<?php echo base_url(); ?>files/fatorcms/vendor_packages/fileupload/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="<?php echo base_url(); ?>files/fatorcms/vendor_packages/fileupload/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="<?php echo base_url(); ?>files/fatorcms/vendor_packages/fileupload/js/jquery.fileupload-validate.js"></script>
<!-- The File Upload user interface plugin -->
<script src="<?php echo base_url(); ?>files/fatorcms/vendor_packages/fileupload/js/jquery.fileupload-ui.js"></script>
<!-- The main application script -->
<script type="text/javascript">
    <?php if($this->session->flashdata('tab_visualizar_imagens')){ ?>
    $(document).ready(function(){
      $('.nav-tabs li:eq(1) a').tab('show');
    });
    <?php } ?>

    $(function () {
        'use strict';
        var url = "<?=base_url()?>fatorcms/galeria/enviar-imagens/upload";
        $('#fileupload').fileupload({
            url: url,
            dataType: 'json',
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    $('<p/>').text(file.name).appendTo('#files');
                    $('.progress[rel="'+file.oldname+'"]').fadeOut();
                    $('[rel="'+file.oldname+'"] button').fadeOut();
                    $('[rel="'+file.oldname+'"]').html('<div class="alert alert-success">Envio Completo</div>');

                    var galeria = $('input[name="form_envia_imagem_id"]').val();

                    $.ajax({
                        type: "GET",
                        dataType: "json",
                        //data: $('#form_envia_imagens').serialize(),
                        url: "<?=base_url()?>fatorcms/galeria/enviar-imagens/salvar_imagem/"+galeria+"/"+file.name,
                        beforeSend: function() {},
                        error: function() {},
                        complete: function() {},
                        success: function() {}
                    });

                    //console.log(file.oldname);
                });
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .progress-bar').css(
                    'width',
                    progress + '%'
                );
            }
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
    });

    // Initialize drag and drop images
    $('#gallery').sortable({
        opacity: 0.5,
        stop: function(event, ui) {
            $('input[name="imagens_order"]').val(getSortableOrder());
        }
      }
    ).disableSelection();

    function getSortableOrder(){
        var order = $('#gallery').sortable('toArray', {attribute:'rel'});
        return order;
    }

    // Set value of input as initial order
    $( document ).ready(function() {
        $('input[name="imagens_order"]').val(getSortableOrder());
    });

</script>
<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
<!--[if (gte IE 8)&(lt IE 10)]>
<script src="<?php echo base_url(); ?>files/fatorcms/vendor_packages/fileupload/js/cors/jquery.xdr-transport.js"></script>
<![endif]-->