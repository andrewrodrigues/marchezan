<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-sm-12">

                <ul class="breadcrumb">
                    <li><a href="<?php getLink(); ?>"><i class="fa fa-home"></i>Home</a></li>
                    <li><a href="<?php getLink('galeria'); ?>">Galeria de Imagens</a></li>
                    <li class="active">Adicionar Galeria</li>
                </ul>

                <section class="panel">
                    <header class="panel-heading">
                        Galeria de Imagens
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                         </span>
                    </header>
                    <div class="panel-body">
                        <div class="position-center">
                            <form role="form" method="post" action="">
                                <div class="form-group">
                                    <label for="">Nome</label>
                                    <input type="text" name="galeria_nome" class="form-control" id="exampleInputEmail1" placeholder="Nome da Galeria">
                                </div>
                                <!--
                                <div class="form-group">
                                    <label for="">Categoria</label>
                                    <select name="galeria_categoria" class="form-control m-bot15" autocomplete="off">
                                        <?php foreach($galeria_categorias as $categoria){ ?>
                                            <option value="<?=$categoria['id'];?>" <?=$categoria['id']=='1' ? 'selected="selected"' : '';?>><?=$categoria['name'];?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                -->
                                <div class="form-group">
                                    <label for="">Destaque</label>
                                    <select name="galeria_destaque" class="form-control m-bot15">
                                        <option value="0">Não</option>
                                        <option value="1">Sim</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Mais Vendidos</label>
                                    <select name="galeria_maisvendidos" class="form-control m-bot15">
                                        <option value="0">Não</option>
                                        <option value="1">Sim</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Status</label>
                                    <select name="galeria_status" class="form-control m-bot15">
                                        <option value="1">Online</option>
                                        <option value="0">Offline</option>
                                    </select>
                                </div>
                                <input type="hidden" name="galeria_adicionar" value="true" />
                                <button type="submit" class="btn btn-info">Salvar</button>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>