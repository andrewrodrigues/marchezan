<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <div class="row">
            <div class="col-sm-12">
                <section class="panel">
                    <header class="panel-heading">
                        Enviar Imagens
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-cog"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                         </span>
                    </header>
                    <div class="panel-body">
                        <div id="fileupload">
                            <!-- Upload function on action form -->
                            <form action="<?php getLink('galeria/enviar-imagens/upload'); ?>" id="form_envia_imagens" method="POST" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label>Galerias</label><br/>
                                    Selecione qual galeria você gostaria de adicionar as imagens.
                                </div>
                                <div class="form-group">
                                    <select name="form_galeria" class="form-control m-bot15" autocomplete="off">
                                        <?php
                                        foreach($galerias as $galeria){
                                            echo '<option value="'.$galeria['id'].'">'.$galeria['name'].'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                                <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                                <div class="row fileupload-buttonbar">
                                    <div class="col-lg-7">
                                        <!-- The fileinput-button span is used to style the file input field as button -->
                                        <span class="btn btn-success fileinput-button">
                                            <i class="fa fa-plus"></i>
                                            <span>Adicionar arquivos...</span>
                                            <input type="file" name="files[]" multiple>
                                        </span>
                                         <button type="submit" class="btn btn-primary start">
                                            <i class="fa fa-upload"></i>
                                            <span>Enviar Todos</span>
                                        </button>
                                        <button type="reset" class="btn btn-warning cancel">
                                            <i class="fa fa-times"></i>
                                            <span>Cancelar Envio</span>
                                        </button>
                                        <!--
                                        <button type="button" class="btn btn-danger delete">
                                            <i class="glyphicon glyphicon-trash"></i>
                                            <span>Excluir</span>
                                        </button>
                                        <input type="checkbox" class="toggle">
                                        -->
                                        <!-- The loading indicator is shown during file processing -->
                                        <span class="fileupload-loading"></span>
                                    </div>
                                    <!-- The global progress information -->
                                    <div class="col-lg-5 fileupload-progress fade">
                                        <!-- The global progress bar -->
                                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                            <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                        </div>
                                        <!-- The extended global progress information -->
                                        <div class="progress-extended">&nbsp;</div>
                                    </div>
                                </div>
                                <!-- The table listing the files available for upload/download -->
                                <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>

                            </form>
                        </div>
                        <!-- The blueimp Gallery widget -->
                        <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
                            <div class="slides"></div>
                            <h3 class="title"></h3>
                            <a class="prev">‹</a>
                            <a class="next">›</a>
                            <a class="close">×</a>
                            <a class="play-pause"></a>
                            <ol class="indicator"></ol>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>

