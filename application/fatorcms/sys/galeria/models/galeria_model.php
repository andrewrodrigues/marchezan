<?php

class galeria_model extends FD_Model {

    function __construct() {
        parent::__construct();
    }

    public function getGalerias(){
        $query = $this->db->query('SELECT fd_marchezan_galeria.id, fd_marchezan_galeria.`name`, fd_marchezan_galeria.destaque, fd_marchezan_galeria.mais_vendido, fd_marchezan_galeria.`status`, fd_marchezan_galeria.creation, fd_marchezan_galeria.`order`, (SELECT Count(fd_marchezan_galeria_imagens.id) FROM fd_marchezan_galeria_imagens WHERE fd_marchezan_galeria_imagens.galeria_id=fd_marchezan_galeria.id ) AS total_imagens, (SELECT fd_marchezan_galeria_categorias.`name` FROM fd_marchezan_galeria_categorias WHERE fd_marchezan_galeria_categorias.id=fd_marchezan_galeria.id_galeria_categorias) AS categoria_nome FROM fd_marchezan_galeria ORDER BY fd_marchezan_galeria.creation DESC');
        return $query->result_array();
    }

    public function insertGaleria($data){
        if($this->db->insert('fd_marchezan_galeria',$data)){
            return true;
        }else{
            return false;
        }
    }

    public function updateGaleria($id,$data){
        $this->db->where('id',$id);
        if($this->db->update('fd_marchezan_galeria',$data)){
            return true;
        }else{
            return false;
        }
    }

    public function insertImagem($galeria,$imagem){
        $data = array(
            'title' => $imagem,
            'file_name' => $imagem,
            'status' => 1,
            'galeria_id' => $galeria
        );
        if($this->db->insert('fd_marchezan_galeria_imagens',$data)){
            return true;
        }else{
            return false;
        }
    }

    public function deleteGaleria($id){
        if($this->db->delete('fd_marchezan_galeria', array('id' => $id))){
            if($this->db->delete('fd_marchezan_galeria_imagens', array('galeria_id' => $id))){
                return true;
            }
            return false;
        }
        return false;
    }

    public function deleteImagem($id){
      if($this->db->delete('fd_marchezan_galeria_imagens', array('id' => $id))){
         return true;
      }
      return false;
    }


    public function getGaleriaInfo($id){
        $this->db->select('fd_marchezan_galeria.id,fd_marchezan_galeria.name,fd_marchezan_galeria.destaque,fd_marchezan_galeria.mais_vendido,fd_marchezan_galeria.status,fd_marchezan_galeria.order,fd_marchezan_galeria.id_galeria_categorias');
        $this->db->where('fd_marchezan_galeria.id',$id);
        return $this->db->get('fd_marchezan_galeria')->result_array();
    }

    public function getGaleriaImagens($id){
        $this->db->select('fd_marchezan_galeria_imagens.id,fd_marchezan_galeria_imagens.title,fd_marchezan_galeria_imagens.file_name,fd_marchezan_galeria_imagens.order,fd_marchezan_galeria_imagens.status');
        $this->db->where('fd_marchezan_galeria_imagens.galeria_id',$id);
        $this->db->order_by('fd_marchezan_galeria_imagens.order','ASC');
        return $this->db->get('fd_marchezan_galeria_imagens')->result_array();
    }

    public function getGaleriaCategorias(){
        $this->db->select('fd_marchezan_galeria_categorias.id,fd_marchezan_galeria_categorias.`name`,fd_marchezan_galeria_categorias.tag,fd_marchezan_galeria_categorias.creation,fd_marchezan_galeria_categorias.`status`,fd_marchezan_galeria_categorias.order');
        $this->db->order_by('fd_marchezan_galeria_categorias.`name`','ASC');
        return $this->db->get('fd_marchezan_galeria_categorias')->result_array();
    }

    public function saveImagemOrder($img_id,$order){
        $this->db->where('id',$img_id);
        if($this->db->update('fd_marchezan_galeria_imagens',$order)){
            return true;
        }else{
            return false;
        }
    }

} 