<?php

class auth_model extends FD_Model {

    function __construct() {
        parent::__construct();
    }

    public function checkAuth($user, $password){
        $this->db->select('fd_marchezan_users.user,fd_marchezan_users.password');
        $this->db->where('fd_marchezan_users.user',$user);
        $this->db->where('fd_marchezan_users.password',$password);
        if($this->db->get('fd_marchezan_users')->num_rows()===1){
            return true;
        }else{
            return false;
        }
    }

    public function getIdByUser($user){
        $this->db->select('fd_marchezan_users.id');
        $this->db->where('fd_marchezan_users.user',$user);
        $row = $this->db->get('fd_marchezan_users')->row_array();
        return $row['id'];
    }

    public function getNameByUser($user){
        $this->db->select('fd_marchezan_users.name');
        $this->db->where('fd_marchezan_users.user',$user);
        $row = $this->db->get('fd_marchezan_users')->row_array();
        return $row['name'];
    }

} 