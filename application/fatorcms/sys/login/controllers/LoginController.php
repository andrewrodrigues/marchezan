<?php


class LoginController extends FD_Controller {

    public function __construct() {
        $this->setLayout('login_layout');
        parent::__construct();
    }

    public function index(){
        if($this->isLoggedIn()){
            redirect(base_url().'fatorcms/dashboard');
        }

        $this->setTitle('Login');
        $this->load->view('login_index');
    }

    public function login(){
        if($this->isLoggedIn()){
            redirect(base_url().'fatorcms/dashboard');
        }
        $user = $this->input->post('user');
        $password = encrypt_password($this->input->post('password'));

        $this->load->model('auth_model','_auth');

        if($this->_auth->checkAuth($user, $password)){
            $this->session->sess_expire_on_close = true;
            $this->session->set_userdata('auth','true');
            $this->session->set_userdata('user_id',$this->_auth->getIdByUser($user));
            $this->session->set_userdata('user_name',$this->_auth->getNameByUser($user));
            redirect(base_url().'fatorcms/dashboard');
        }else{
            $this->session->set_flashdata('login_error',true);
            redirect(base_url().'fatorcms');
        }
    }

    public function logout(){
        $this->session->unset_userdata('auth');
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('user_name');
        $this->session->set_flashdata('logout_error',true);
        redirect(base_url().'fatorcms');
    }


}