<?php //debugVarAndDie($galerias); ?>

<!-- modal : delete cep confirmation -->
<div class="modal fade" id="deleteConfirmation">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Excluir Usuário</h4>
            </div>
            <div class="modal-body">
                <p>Você deseja excluir o Usuário: <strong rel="usuario_nome">usuario_nome</strong>?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-warning" onclick="">Excluir</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">

                <ul class="breadcrumb">
                    <li><a href="<?php getLink(); ?>"><i class="fa fa-home"></i>Home</a></li>
                    <li class="active">Newsletter</li>
                </ul>

                <?php if($this->session->flashdata('delete_success')){ ?>
                    <div class="alert alert-success fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Sucesso!</strong> Usuário excluído com sucesso.
                    </div>
                <?php } ?>

                <?php if($this->session->flashdata('success_msg')){ ?>
                <div class="alert alert-success fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="fa fa-times"></i>
                    </button>
                    <strong>Sucesso!</strong> Usuário adicionado com sucesso.
                </div>
                <?php } ?>

                <?php if($this->session->flashdata('update_msg')){ ?>
                    <div class="alert alert-success fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Sucesso!</strong> Usuário atualizado com sucesso.
                    </div>
                <?php } ?>

                <?php if($this->session->flashdata('error_msg')){ ?>
                <div class="alert alert-danger fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="fa fa-times"></i>
                    </button>
                    <strong>Erro!</strong> Ocorreu um erro ao efetuar esta operação.
                </div>
                <?php } ?>

                <section class="panel">
                    <header class="panel-heading">
                        Newsletter
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                         </span>
                    </header>
                    <div class="panel-body">


                        <div class="panel-body">
                        <div class="adv-table">
                        <table  class="display table table-bordered table-striped" id="dynamic-table">
                        <thead>
                        <tr>
                            <th width="15%">ID</th>
                            <th width="18%">E-mail</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($newsletter as $news){ ?>
                          <tr>
                            <td>
                              <?php echo $news->id; ?>
                            </td>
                            <td><?php echo $news->email; ?></td>
                          </tr>
                        <?php } ?>
                        </tbody>
<!--                        <tfoot>
                        <tr>
                            <th>Rendering engine</th>
                            <th>Browser</th>
                            <th>Platform(s)</th>
                            <th class="hidden-phone">Engine version</th>
                            <th class="hidden-phone">CSS grade</th>
                        </tr>
                        </tfoot>-->
                        </table>
                        </div>
                        </div>

                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>

<script type="text/javascript">
    function showDeleteConfirmation(id,name){
        $('#deleteConfirmation').modal('show');
        $('#deleteConfirmation').find('[rel="usuario_nome"]').text(name);
        $('#deleteConfirmation').find('.btn-warning').attr('onclick','javascript:deleteConfirmation('+id+');');
    }

    function deleteConfirmation(id){
        location.href='<?php getLink('usuarios/excluir/');?>'+id;
    }


</script>