<?php

class NewsletterController extends FD_Controller {

  public function __construct() {
    $this->setMenu('default_menu');
    parent::__construct();
    if(!$this->isLoggedIn()){ redirect(base_url().'fatorcms');}
  }

  public function index(){
    /* Dynamic Tables Block */
    $this->loadCSSVendor(vendor_dynamic_tables_css());
    $this->loadJSVendorBottom(vendor_dynamic_tables_js());
    $this->loadJSBottom(dynamic_tables_js());
    /* End - Dynamic Tables Block */

    $data = array();
    $this->load->model('newsletter_model');
    $data['newsletter'] = $this->newsletter_model->getAllNewsletter();

    $this->load->view('newsletter_view',$data);
  }
}