<?php

class categorias_model extends FD_Model {

    function __construct() {
        parent::__construct();
    }

    public function getAllCategorias(){
        return $this->db->get('fd_marchezan_categorias')->result();
    }

    public function getCategoria($id){
      $this->db->where('fd_marchezan_categorias.id',$id);
      return $this->db->get('fdci_categorias')->row();
    }

    public function insertCategoria($data){
        if($this->db->insert('fd_marchezan_categorias',$data)){
            return true;
        }else{
            return false;
        }
    }

    public function updateCategoria($id,$data){
        $this->db->where('id',$id);
        if($this->db->update('fd_marchezan_categorias',$data)){
            return true;
        }else{
            return false;
        }
    }

    public function deleteCategoria($id){
        if($this->db->delete('fd_marchezan_categorias', array('id' => $id))){
            return true;
        }
        return false;
    }


} 