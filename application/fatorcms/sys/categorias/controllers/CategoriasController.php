<?php

class CategoriasController extends FD_Controller {

  public function __construct() {
    $this->setMenu('default_menu');
    parent::__construct();
    if(!$this->isLoggedIn()){ redirect(base_url().'fatorcms');}
  }

  public function index(){
    /* Dynamic Tables Block */
    $this->loadCSSVendor(vendor_dynamic_tables_css());
    $this->loadJSVendorBottom(vendor_dynamic_tables_js());
    $this->loadJSBottom(dynamic_tables_js());
    /* End - Dynamic Tables Block */

    $data = array();
    $this->load->model('categorias_model');
    $data['categorias'] = $this->categorias_model->getAllCategorias();

    $this->load->view('categorias_view',$data);
  }


  public function adicionar(){
    if($this->input->post('categoria_adicionar')==="true"){
      $data_insert = array(
        'nome' => $this->input->post('nome'),
        'tag' => removeAccents(strtolower(trim($this->input->post('nome'))))
      );
      $this->load->model('categorias_model');
      if($this->categorias_model->insertCategoria($data_insert)){
        $this->session->set_flashdata('success_msg',true);
      }
      redirect(base_url().'fatorcms/categorias');
    }
    $this->load->model('categorias_model');
    $data['categorias_model'] = count($this->categorias_model->getAllCategorias());
    $this->load->view('categorias_adicionar_view',$data);
  }


  public function editar($id){
    $this->load->model('categorias_model');
    $data['categoria'] = $this->categorias_model->getCategoria($id);

    if($this->input->post('categoria_editar')==="true"){
      $data_update = array(
        'nome' => $this->input->post('nome')
      );
      if($this->categorias_model->updateCategoria($id,$data_update)){
        $this->session->set_flashdata('update_msg',true);
      }else{
        $this->session->set_flashdata('error_msg',true);
      }
      redirect(base_url().'fatorcms/categorias/');
    }
    $this->load->view('categorias_editar_view',$data);
  }

  public function excluir($id){
    $this->isAjax();
    $this->load->model('categorias_model');
    $item = $this->categorias_model->getCategoria($id);
    if($this->categorias_model->deleteCategoria($id)){
      $this->session->set_flashdata('delete_success',true);
    }else{
      $this->session->set_flashdata('error_msg',true);
    }
    redirect(base_url().'fatorcms/categorias');
  }

}