<?php

class UsuariosController extends FD_Controller {

  public function __construct() {
    $this->setMenu('default_menu');
    parent::__construct();
    if(!$this->isLoggedIn()){ redirect(base_url().'fatorcms');}
  }

  public function index(){
    /* Dynamic Tables Block */
    $this->loadCSSVendor(vendor_dynamic_tables_css());
    $this->loadJSVendorBottom(vendor_dynamic_tables_js());
    $this->loadJSBottom(dynamic_tables_js());
    /* End - Dynamic Tables Block */

    $data = array();
    $this->load->model('usuarios_model');
    $data['usuarios'] = $this->usuarios_model->getAllUsuarios();

    $this->load->view('usuarios_view',$data);
  }


  public function adicionar(){
    if($this->input->post('usuario_adicionar')==="true"){
      $this->session->set_userdata('fields',array(
        'name' => $this->input->post('name'),
        'user' => $this->input->post('user'),
        'email' => $this->input->post('email'),
        'password' => $this->input->post('password')
      ));
      $this->load->model('usuarios_model');
      if($this->usuarios_model->hasUser($this->input->post('user'))==null){
        $data_insert = array(
          'name' => $this->input->post('name'),
          'user' => $this->input->post('user'),
          'email' => $this->input->post('email'),
          'password' => encrypt_password($this->input->post('password'))
        );
        if($this->usuarios_model->insertUsuario($data_insert)){
          $this->session->unset_userdata('fields');
          $this->session->set_flashdata('success_msg',true);
        }
        redirect(base_url().'fatorcms/usuarios');
      }else{
        $this->session->set_flashdata('duplicated_msg',true);
        redirect(base_url().'fatorcms/usuarios/adicionar');
      }
    }
    $this->load->view('usuarios_adicionar_view');
  }


  public function editar($id){
    redirect(base_url().'fatorcms/usuarios');
  }

  public function excluir($id){
    $this->isAjax();
    $this->load->model('usuarios_model');
    if($this->usuarios_model->deleteUsuario($id)){
      $this->session->set_flashdata('delete_success',true);
    }else{
      $this->session->set_flashdata('error_msg',true);
    }
    redirect(base_url().'fatorcms/usuarios');
  }

}