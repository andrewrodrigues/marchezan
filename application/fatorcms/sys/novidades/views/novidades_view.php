<?php //debugVarAndDie($galerias); ?>

<!-- modal : delete cep confirmation -->
<div class="modal fade" id="deleteConfirmation">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Excluir Novidade</h4>
            </div>
            <div class="modal-body">
                <p>Você deseja excluir a Novidade <strong rel="name">name</strong>?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-warning" onclick="">Excluir</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">

                <ul class="breadcrumb">
                    <li><a href="<?php getLink(); ?>"><i class="fa fa-home"></i>Home</a></li>
                    <li><a href="<?php getLink('smartphone'); ?>">Gerenciar Novidades</a></li>
                    <li class="active">Lista de Novidades</li>
                </ul>

                <?php if($this->session->flashdata('delete_success')){ ?>
                    <div class="alert alert-success fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Sucesso!</strong> Novidade excluída com sucesso.
                    </div>
                <?php } ?>

                <?php if($this->session->flashdata('success_msg')){ ?>
                <div class="alert alert-success fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="fa fa-times"></i>
                    </button>
                    <strong>Sucesso!</strong> <?php echo $this->session->flashdata('success_msg'); ?> Novidade adicionada com sucesso.
                </div>
                <?php } ?>

                <?php if($this->session->flashdata('update_msg')){ ?>
                    <div class="alert alert-success fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                        <strong>Sucesso!</strong> Novidade atualizada com sucesso.
                    </div>
                <?php } ?>

                <?php if($this->session->flashdata('error_msg')){ ?>
                <div class="alert alert-danger fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="fa fa-times"></i>
                    </button>
                    <strong>Erro!</strong> Ocorreu um erro ao efetuar esta operação.
                </div>
                <?php } ?>

                <section class="panel">
                    <header class="panel-heading">
                        Gerenciamento de Novidades
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                         </span>
                    </header>
                    <div class="panel-body">

                        <div class="panel-body">
                        <div class="btn-group pull-right">
                            <button class="btn btn-primary" onclick="location.href='<?php getLink('novidades/adicionar'); ?>'" type="button">Cadastrar Novidade</button>
                        </div>
                        </div>

                        <div class="panel-body">
                        <div class="adv-table">
                        <table  class="display table table-bordered table-striped" id="dynamic-table2">
                        <thead>
                        <tr>
                            <th width="150">Data</th>
                            <th width="15%">Imagem</th>
                            <th width="">Titulo/Resumo</th>
                            <th width="100px">Tipo</th>
                            <th>Status</th>
                            <th width="14%">Ações</th>
                        </tr>
                        </thead>
                        <tbody id="content">
                          <tr class="content-loading">
                            <td colspan="6">
                              Carregando dados...
                            </td>
                          </tr>

                        </tbody>
<!--                        <tfoot>
                        <tr>
                            <th>Rendering engine</th>
                            <th>Browser</th>
                            <th>Platform(s)</th>
                            <th class="hidden-phone">Engine version</th>
                            <th class="hidden-phone">CSS grade</th>
                        </tr>
                        </tfoot>-->
                        </table>
                        </div>
                        </div>

                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>

<script type="text/javascript">
    function showDeleteConfirmation(id,name){
        $('#deleteConfirmation').modal('show');
        $('#deleteConfirmation').find('[rel="name"]').text(name);
        $('#deleteConfirmation').find('.btn-warning').attr('onclick','javascript:deleteConfirmation('+id+');');
    }

    function deleteConfirmation(id){
        location.href='<?php getLink('novidades/excluir/');?>'+id;
    }

    $(document).ready(function(){
      var request = $.ajax({
        url: "<?php echo getLink('novidades/ajax_getAllNovidades'); ?>",
        dataType: "html"
      });
      request.done(function(data){
        $('.content-loading').remove();
        $('#content').append(data);
        $('#dynamic-table2').DataTable({
            "aaSorting": []
        });
      });
    });





</script>