<?php if(isset($_GET['dev'])){ debugVarAndDie($this->session->userdata('campos_preenchidos')); } ?>

<section id="main-content">
  <section class="wrapper">
    <!-- page start-->

    <div class="row">
      <div class="col-sm-12">

        <ul class="breadcrumb">
          <li><a href="<?php getLink(); ?>"><i class="fa fa-home"></i>Home</a></li>
          <li><a href="<?php getLink('novidades'); ?>">Gerenciar Novidades</a></li>
          <li class="active">Cadastrar Novidade</li>
        </ul>

        <?php if($this->session->flashdata('success_msg')){ ?>
          <div class="alert alert-success fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
              <i class="fa fa-times"></i>
            </button>
            <strong>Sucesso!</strong> SmartPhone atualizado com sucesso.
          </div>
        <?php } ?>

        <?php if($this->session->flashdata('error_msg')){ ?>
          <div class="alert alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
              <i class="fa fa-times"></i>
            </button>
            <strong>Erro!</strong> Ocorreu um erro ao efetuar esta operação.
          </div>
        <?php } ?>

        <?php if(isset($upload_error_msg)){ ?>
          <div class="alert alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
              <i class="fa fa-times"></i>
            </button>
            <strong>Atenção!</strong> Utilize imagens dos formatos .jpg, .gif e .png. A imagem deverá ter no máximo 2 MB.
          </div>
        <?php } ?>

        <?php if(isset($upload_arquivo_error_msg)){ ?>
          <div class="alert alert-danger fade in">
            <button data-dismiss="alert" class="close close-sm" type="button">
              <i class="fa fa-times"></i>
            </button>
            <strong>Atenção!</strong> Utilize arquivos dos formatos .jpg, .gif, .png, .doc, .zip, .rar, .pdf e .txt. O arquivo deverá ter no máximo 10 MB.
          </div>
        <?php } ?>

        <section class="panel">
          <div class="panel-body">
            <div class="tab-content">
              <div id="galeria" class="tab-pane active">
                <form role="form" method="post" action="" enctype="multipart/form-data">
                  <div class="form-group">
                    <label for="">Status</label>
                    <select name="status" id="" class="form-control">
                        <?php if($campos_preenchidos['status']=='1'){ ?>
                            <option value="1">Publicado</option>
                            <option value="0">Oculto</option>
                        <?php }else{ ?>
                            <option value="0">Oculto</option>
                            <option value="1">Publicado</option>
                        <?php } ?>
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="data">Data:</label>
                    <input class="form-control default-date-picker" name="data" type="text" size="16" value="<?php echo isset($campos_preenchidos['data']) ? $campos_preenchidos['data'] : ''; ?>" required="">
                  </div>

                  <!-- <div class="form-group">
                    <label for="">Publicação</label> - Caso sim, ela deixa de ser uma Novidade e passa a ser uma Publicação
                    <select name="publicacao" id="select_publicacao" class="form-control" autocomplete="off">
                      <?php if($campos_preenchidos['publicacao']==1){ ?>
                        <option value="1">Sim</option>
                        <option value="0">Não</option>
                      <?php }else{ ?>
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                      <?php } ?>
                    </select>
                  </div> -->

                  <div class="form-gorup">
                    <label>Cores para os boxes</label>
                    <div class="clearfix"></div>
                    <label><input type="radio" name="cor" value="azul"<?php echo $campos_preenchidos['cor'] == 'azul' ? ' checked="checked"' : '' ?>> Azul</label>
                    <label><input type="radio" name="cor" value="verde"<?php echo $campos_preenchidos['cor'] == 'verde' ? ' checked="checked"' : '' ?>> Verde</label>
                    <label><input type="radio" name="cor" value="amarelo"<?php echo $campos_preenchidos['cor'] == 'amarelo' ? ' checked="checked"' : '' ?>> Amarelo</label>
                    <label><input type="radio" name="cor" value="marrom"<?php echo $campos_preenchidos['cor'] == 'marrom' ? ' checked="checked"' : '' ?>> Marrom</label>
                  </div>

                  <div class="form-group">
                    <label for="">Título</label>
                    <input type="text" name="titulo" maxlength="50" class="form-control" value="<?php echo isset($campos_preenchidos['titulo']) ? $campos_preenchidos['titulo'] : ''; ?>" required>
                    <label for="">Resumo</label>
                    <textarea name="resumo" id="" cols="30" rows="5" class="form-control" required><?php echo isset($campos_preenchidos['resumo']) ? $campos_preenchidos['resumo'] : ''; ?></textarea>
                    <label for="">Conteúdo</label>
                    <textarea name="conteudo" id="conteudo" cols="30" rows="10" class="form-control"><?php echo isset($campos_preenchidos['conteudo']) ? $campos_preenchidos['conteudo'] : ''; ?></textarea>
                  </div>
                  <div class="form-group">
                    <label for="">Imagem</label>
                    <input type="file" name="imagem" />
                    <?php if(isset($campos_preenchidos['imagem'])): ?>
                      <img src="<?php echo getUploadedFile('imagens/'.$campos_preenchidos['imagem']); ?>" width="250" alt=""/>
                      <input type="hidden" name="imagem_hidden" value="<?php echo $campos_preenchidos['imagem']; ?>"/>
                    <?php endif; ?>
                  </div>
                  <!-- <div class="form-group">
                    <label for="">Categoria</label>
                    <select name="categoria" id="" class="form-control" autocomplete="off">
                      <?php foreach($categorias as $categoria): ?>
                        <option value="<?php echo $categoria->id; ?>"><?php echo $categoria->nome; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div> -->

                  <div id="form_download" <?php if(isset($campos_preenchidos['publicacao']) AND $campos_preenchidos['publicacao']==1){}else{ echo 'style="display:none;"'; } ?>>
                    <div class="form-group">
                      <label for="">Arquivo Download</label>
                      <input type="file" name="arquivo" />
                      <?php if(isset($campos_preenchidos['arquivo']) AND !$this->session->flashdata('upload_arquivo_error_msg')): ?>
                        <a href="<?php echo getUploadedFile('imagens/'.$campos_preenchidos['arquivo']); ?>" target="_blank"><?php echo $campos_preenchidos['arquivo']; ?></a>
                        <input type="hidden" name="arquivo_hidden" value="<?php echo $campos_preenchidos['arquivo']; ?>"/>
                      <?php endif; ?>
                      <label for="">Link para Download</label>
                      <input type="text" name="url_download" maxlength="50" class="form-control" value="<?php echo isset($campos_preenchidos['url_download']) ? $campos_preenchidos['url_download'] : ''; ?>">
                    </div>
                    <div class="form-group">
                      <label for="">Necessário Cadastro?</label>
                      <select name="download_cadastro" id="select_download" class="form-control">
                          <?php if($campos_preenchidos['download_cadastro']==1){ ?>
                              <option value="1">Sim</option>
                              <option value="0">Não</option>
                          <?php }else{ ?>
                              <option value="0">Não</option>
                              <option value="1">Sim</option>
                          <?php } ?>
                      </select>
                    </div>

                    <div id="form_cadastro" <?php if(isset($campos_preenchidos['download_cadastro']) AND $campos_preenchidos['download_cadastro']==1){}else{ echo 'style="display:none;"'; } ?>>
                      <div class="form-group">
                        <label for="">Formulário de Cadastro</label>
                        <br/>
                        Campos necessários:
                        <br/><br/>
                        <input type="checkbox" name="download_form_nome" value="sim" checked="checked" disabled/> Nome <br/>
                        <input type="checkbox" name="download_form_email" value="sim" checked="checked" disabled/> Email <br/>
                        <input type="checkbox" name="download_form_telefone" value="sim" <?php if(isset($campos_preenchidos['download_form_telefone'])){ echo 'checked="checked"'; } ?> /> Telefone <br/>
                        <input type="checkbox" name="download_form_cidade" value="sim" <?php if(isset($campos_preenchidos['download_form_cidade'])){ echo 'checked="checked"'; } ?> /> Cidade <br/>
                        <input type="checkbox" name="download_form_mensagem" value="sim" <?php if(isset($campos_preenchidos['download_form_mensagem'])){ echo 'checked="checked"'; } ?> /> Mensagem <br/>

                        <br/>
                      </div>
                    </div>

                  </div>



                  <div class="form-group">
                    <input type="hidden" name="novidade_editar" value="true" />
                    <button type="submit" class="btn btn-info">Salvar</button>
                  </div>
                </form>
              </div>

            </div>
          </div>
        </section>






      </div>
    </div>
    <!-- page end-->
  </section>
</section>

<script type="text/javascript" src="<?php echo getJs('tiny_mce/tiny_mce'); ?>"></script>
<script type="text/javascript">
  $(document).ready(function(){
    // Datepicker
    $('.default-date-picker').datepicker({
      format: 'dd/mm/yyyy'
    });
  });

  tinyMCE.init({
    mode : 'exact',
    elements : 'conteudo',
    entity_encoding : 'raw',
    theme : 'advanced',
    width : '97.5%',
    height : '350px',
    language : 'br',
    plugins : 'searchreplace,contextmenu,paste,filemanager,imagemanager, youtubeIframe',
    paste_auto_cleanup_on_paste : true,
    theme_advanced_buttons1 : 'bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,pasteword,removeformat,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,link,unlink,anchor,|,insertimage,insertfile, youtubeIframe',
    theme_advanced_buttons2 : '',
    theme_advanced_buttons3 : '',
    theme_advanced_toolbar_location : 'top',
    theme_advanced_toolbar_align : 'center',
    theme_advanced_statusbar_location : 'none',
    theme_advanced_resizing : false,
    relative_urls : false
    // remove_script_host : false
  });


  $('#select_publicacao').prop('selectedIndex',0);
  $('#download_cadastro').prop('selectedIndex',0);

  $('#select_publicacao').change(function(){
    if($('#select_publicacao').val()=='1'){
      $('#form_download').slideDown();
    }else{
      $('#form_download').slideUp();
    }
  });

  $('#select_download').change(function(){
    if($('#select_download').val()=='1'){
      $('#form_cadastro').slideDown();
    }else{
      $('#form_cadastro').slideUp();
    }
  });

  <?php if(isset($campos_preenchidos['publicacao']) AND $campos_preenchidos['publicacao']==1): ?>
  $('#select_publicacao option[value="1"]').attr('selected', 'selected');
  <?php endif; ?>
  <?php if(isset($campos_preenchidos['publicacao']) AND $campos_preenchidos['publicacao']==0): ?>
  $('#select_publicacao option[value="0"]').attr('selected', 'selected');
  <?php endif; ?>

  <?php if(isset($campos_preenchidos['download_cadastro']) AND $campos_preenchidos['download_cadastro']==1): ?>
  $('#select_download option[value="1"]').attr('selected', 'selected');
  <?php endif; ?>
  <?php if(isset($campos_preenchidos['download_cadastro']) AND $campos_preenchidos['download_cadastro']==0): ?>
  $('#select_download option[value="0"]').attr('selected', 'selected');
  <?php endif; ?>

</script>