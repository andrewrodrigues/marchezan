<?php //debugVarAndDie($galerias); ?>

<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-12">

                <ul class="breadcrumb">
                    <li><a href="<?php getLink(); ?>"><i class="fa fa-home"></i>Home</a></li>
                    <li><a href="<?php getLink('smartphone'); ?>">Gerenciar Novidades</a></li>
                    <li class="active">Lista de Cadastros para Download</li>
                </ul>

                <section class="panel">
                    <header class="panel-heading">
                        Visualização de Cadastros para Download
                        <span class="tools pull-right">
                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                            <a href="javascript:;" class="fa fa-times"></a>
                         </span>
                    </header>
                    <div class="panel-body">
                        <div class="panel-body">
                            <?php
                                $downloads_efetuados = 0;
                                foreach($downloads as $download){
                                   if($download->download_status==1){
                                       $downloads_efetuados++;
                                   }
                                }
                                $downloads_naoefetuados = sizeof($downloads)-$downloads_efetuados;

                                $percent_1 = floatval(sizeof($downloads)/100);
                                $percent_yes = round($downloads_efetuados/$percent_1);
                                $percent_no = round($downloads_naoefetuados/$percent_1);

                            ?>


                            <p>Downloads Efetuados: <span class="label label-success"><?php echo $downloads_efetuados; ?></span> | Downloads Pendentes: <span class="label label-danger"><?php echo $downloads_naoefetuados; ?></span></p>
                            <p>
                              Progresso em porcentagem:
                            </p>
                            <div class="progress progress-sm">
                                <div class="progress-bar progress-bar-success" style="width: <?php echo $percent_yes; ?>%">
                                    <span class="sr-only"><?php echo $percent_yes; ?>% Efetuaram downloads</span>
                                </div>
                                <div class="progress-bar progress-bar-danger" style="width: <?php echo $percent_no; ?>%">
                                    <span class="sr-only"><?php echo $percent_no; ?>% Não Efetuaram downloads</span>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                        <div class="adv-table">
                        <table  class="display table table-bordered table-striped" id="dynamic-table">
                        <thead>
                        <tr>
                            <th width="15%">Email</th>
                            <th width="">Conteudo do Email</th>
                            <th width="100px">Arquivo</th>
                            <th width="18%">Data de Cadastro:</th>
                            <th width="14%">URL Novidade</th>
                            <th width="14%">Status Download</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($downloads as $item){ ?>
                          <tr>
                            <td>
                              <?php echo $item->email; ?>
                            </td>
                            <td>
                              <?php echo $item->conteudo_email; ?>
                            </td>
                            <td>
                              <?php echo $item->arquivo_download; ?>
                            </td>
                            <td>
                                <?php echo date("d/m/Y - H:s",strtotime($item->creation))?>
                            </td>
                            <td>
                                <a href="<?php echo BASE_URL_PROD.'publicacao/'.$item->publicacao_url; ?>">Ver Publicação</a>
                            </td>


                            <td>
                              <?php if($item->download_status==1){ ?>
                                <span class="label label-success">Efetuado</span>
                              <?php }else{ ?>
                                <span class="label label-danger">Não Efetuado</span>
                              <?php } ?>
                            </td>
                          </tr>
                        <?php } ?>
                        </tbody>
<!--                        <tfoot>
                        <tr>
                            <th>Rendering engine</th>
                            <th>Browser</th>
                            <th>Platform(s)</th>
                            <th class="hidden-phone">Engine version</th>
                            <th class="hidden-phone">CSS grade</th>
                        </tr>
                        </tfoot>-->
                        </table>
                        </div>
                        </div>

                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>

<script type="text/javascript">
    function showDeleteConfirmation(id,name){
        $('#deleteConfirmation').modal('show');
        $('#deleteConfirmation').find('[rel="name"]').text(name);
        $('#deleteConfirmation').find('.btn-warning').attr('onclick','javascript:deleteConfirmation('+id+');');
    }

    function deleteConfirmation(id){
        location.href='<?php getLink('novidades/excluir/');?>'+id;
    }


</script>