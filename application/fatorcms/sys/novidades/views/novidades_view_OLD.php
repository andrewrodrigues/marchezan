<?php foreach($novidades as $item){ ?>
  <tr>
    <td>
      <?php if($item->imagem!=''){ ?>
        <img width="100" src="<?php echo getUploadedFile('imagens/'.$item->imagem); ?>" />
      <?php }else{ echo 'Sem imagem'; } ?>
    </td>

    <td>
      <?php echo '<strong>'.$item->titulo.'</strong><br />'; ?>
      <?php if($item->resumo!=''){ echo '- '.$item->resumo.'<br />'; } ?>
    </td>

    <td>
      <?php
      if($item->publicacao==1){
        echo "Publicação";
      }else{
        echo "Novidade";
      }
      ?>
    </td>

    <td>
      <?php if($item->status==1){ ?>
        <span class="label label-success">Publicado</span>
      <?php }else{ ?>
        <span class="label label-danger">Oculto</span>
      <?php } ?>
    </td>

    <td><?php echo date("d/m/Y - H:s",strtotime($item->creation))?></td>
    <td>
      <button class="btn btn-warning btn-xs" onclick="location.href='<?php getLink('novidades/editar/'.$item->id); ?>'" type="button">Editar</button>
      <button class="btn btn-danger btn-xs" onclick="javascript:showDeleteConfirmation('<?php echo $item->id;?>','<?php echo $item->titulo;?>');" type="button">Excluir</button>
    </td>
  </tr>
<?php } ?>