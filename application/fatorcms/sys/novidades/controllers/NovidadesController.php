<?php

class NovidadesController extends FD_Controller {

  public function __construct() {
    $this->setMenu('default_menu');
    parent::__construct();
    if(!$this->isLoggedIn()){ redirect(base_url().'fatorcms');}
  }

  public function index(){
    /* Dynamic Tables Block */
    $this->loadCSSVendor(vendor_dynamic_tables_css());
    $this->loadJSVendorBottom(vendor_dynamic_tables_js());
    $this->loadJSBottom(dynamic_tables_js());
    /* End - Dynamic Tables Block */

    $data = array();
    $this->load->model('novidades_model');
    //$data['novidades'] = $this->novidades_model->getAllNovidades();

    //debugVarAndDie($data['novidades']);

    $this->load->view('novidades_view',$data);
  }

  public function ajax_getAllNovidades(){
    $this->layout="";
    $this->load->model('novidades_model');
    $novidades = $this->novidades_model->getAllNovidades();
    $html = "";
    foreach($novidades as $item){
      $html.= "<tr>";

      $html.="<td>";
      $html.= convertUStoBRdate($item->data);
      $html.="</td>";

      $html.="<td>";
      if($item->imagem!=''){
        $html.= "<img width=\"100\" src=\"".base_url().'files/uploads/imagens/'.$item->imagem."\" />";
      }else{ $html.= "Sem imagem"; }
      $html.="</td>";

      $html.="<td>";
      $html.="<strong>".$item->titulo."</strong><br />";
      if($item->resumo!=''){ $html.= $item->resumo; }
      $html.="</td>";

      $html.="<td>";
      if($item->publicacao==1){
        $html.= "Publicação";
      }else{
        $html.= "Novidade";
      }
      $html.="</td>";

      $html.="<td>";
      if($item->status==1){
        $html.="<span class=\"label label-success\">Publicado</span>";
      }else{
        $html.="<span class=\"label label-danger\">Oculto</span>";
      }
      $html.="</td>";

      $html.="<td>";
      $html.="<button class=\"btn btn-warning btn-xs\" onclick=\"location.href='".base_url().'fatorcms/novidades/editar/'.$item->id."'\" type=\"button\">Editar</button> ";
      $html.="<button class=\"btn btn-danger btn-xs\" onclick=\"javascript:showDeleteConfirmation('".$item->id."','".$item->titulo."');\" type=\"button\">Excluir</button>";
      $html.="</td>";
      $html.="</tr>";
    }
    echo $html;
  }

  public function adicionar(){
    if($this->input->post('novidade_adicionar')==="true"){

      $sessao['campos_preenchidos'] = array();
      $sessao['campos_preenchidos']['titulo'] = $this->input->post('titulo');
      $sessao['campos_preenchidos']['resumo'] = $this->input->post('resumo');
      $sessao['campos_preenchidos']['conteudo'] = $this->input->post('conteudo');
      $sessao['campos_preenchidos']['categoria'] = $this->input->post('categoria');
      $sessao['campos_preenchidos']['publicacao'] = $this->input->post('publicacao');
      $sessao['campos_preenchidos']['status'] = $this->input->post('status');
      $sessao['campos_preenchidos']['url_download'] = $this->input->post('url_download');
      $sessao['campos_preenchidos']['data'] = $this->input->post('data');
      $sessao['campos_preenchidos']['cor'] = $this->input->post('cor');

      $erro_envio = false;

      if($_FILES['imagem']['name']!=''){
        $file_name = explode(".",$_FILES['imagem']['name']);
        $file_name = strtolower(removeAccents($file_name[0]))."_".date("h_m_i-_d-m-Y",time()).".".$file_name[1];

        $config['file_name'] = $file_name;
        $config['overwrite'] = TRUE;
        $config['allowed_types'] = 'jpg|jpeg|gif|png';
        $config['max_size'] = 2000;
        $config['upload_path'] = "./files/uploads/imagens";

        $this->load->library('upload', $config);
        if(!$this->upload->do_upload('imagem')){
          $data['upload_error_msg'] = true;
          $erro_envio = true;
        }
        $config['image_library'] = 'gd2';
        $config['source_image'] = $config['upload_path'].'/'.$config['file_name'];
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width']  = 858;
        $config['height'] = 490;
        $config['master_dim'] = 'width';
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();
      }
      if(!isset($file_name)){
        $file_name = null;
      }

      if($this->input->post('imagem_hidden')){
        $sessao['campos_preenchidos']['imagem'] = $this->input->post('imagem_hidden');
      }else{
        $sessao['campos_preenchidos']['imagem'] = $file_name;
      }

      $data_insert = array(
        'titulo' => $this->input->post('titulo'),
        'tag' => strtolower(removeAccents($this->input->post('titulo'))),
        'resumo' => $this->input->post('resumo'),
        'conteudo' => $this->input->post('conteudo'),
        'categoria_id' => $this->input->post('categoria'),
        'publicacao' => $this->input->post('publicacao'),
        'status' => $this->input->post('status'),
        'imagem' => $sessao['campos_preenchidos']['imagem'],
        'url_download' => $this->input->post('url_download'),
        'data' => convertBRtoUSdate($this->input->post('data')),
        'cor' => $sessao['campos_preenchidos']['cor']
      );

      // Em caso publicacao seja ativada
      if($this->input->post('publicacao')==1){
        if($_FILES['arquivo']['name']!=''){
          $file_name = explode(".",$_FILES['arquivo']['name']);
          $file_name = strtolower(removeAccents($file_name[0]))."_".date("h_m_-_d-m-Y",time()).".".$file_name[1];

          $config['file_name'] = $file_name;
          $config['overwrite'] = TRUE;
          $config['allowed_types'] = 'jpg|jpeg|gif|png|doc|zip|rar|pdf|txt';
          $config['max_size'] = 10000;
          $config['upload_path'] = "./files/uploads/arquivos";

          $this->load->library('upload', $config);
          $this->upload->initialize($config);

          $config['image_library'] = 'gd2';
          $config['source_image'] = $config['upload_path'].'/'.$config['file_name'];
          $config['create_thumb'] = TRUE;
          $config['maintain_ratio'] = TRUE;
          $config['width']  = 858;
          $config['height'] = 490;
          $config['master_dim'] = 'width';
          $this->load->library('image_lib', $config);
          $this->image_lib->resize();
          if(!$this->upload->do_upload('arquivo')){
            $data['upload_arquivo_error_msg'] = true;
            $erro_envio = true;
          }else{
            $data_insert['arquivo_download'] = $file_name;
          }
        }

        if($this->input->post('arquivo_hidden')){
          $sessao['campos_preenchidos']['arquivo'] = $this->input->post('arquivo_hidden');
        }else{
          $sessao['campos_preenchidos']['arquivo'] = $file_name;
        }

        $data_insert['cadastro_download'] = $this->input->post('download_cadastro');
        $sessao['campos_preenchidos']['download_cadastro'] = $this->input->post('download_cadastro');

        // Em caso do formulário ser ativado
        if($this->input->post('download_cadastro')==1){
          $fields = '';
          if($this->input->post('download_form_nome')=="sim"){
            $fields.= '{Nome}';
          }
          if($this->input->post('download_form_email')=="sim"){
            $fields.= '{E-mail}';
          }
          if($this->input->post('download_form_telefone')=="sim"){
            $fields.= '{Telefone}';
            $sessao['campos_preenchidos']['download_form_telefone'] = $this->input->post('download_form_telefone');
          }
          if($this->input->post('download_form_cidade')=="sim"){
            $fields.= '{Cidade}';
            $sessao['campos_preenchidos']['download_form_cidade'] = $this->input->post('download_form_cidade');
          }
          if($this->input->post('download_form_mensagem')=="sim"){
            $fields.= '{Mensagem}';
            $sessao['campos_preenchidos']['download_form_mensagem'] = $this->input->post('download_form_mensagem');
          }
          $data_insert['formulario_download'] = $fields;
        }
      }

      if($erro_envio){
        $data['campos_preenchidos'] = $sessao['campos_preenchidos'];
      }else{
        unset($data['upload_arquivo_error_msg']);
        unset($data['upload_arquivo_error_msg']);
      }

      $this->load->model('novidades_model');
      if($this->novidades_model->insertNovidade($data_insert) AND $erro_envio==false){
        $this->session->set_flashdata('success_msg',true);
        $sessao['campos_preenchidos'] = array();
        redirect(base_url().'fatorcms/novidades');
      }
    }

    /* DatePicker */
    $this->loadCSSVendor('bootstrap-datepicker/css/datepicker');
    $this->loadJSVendorBottom('bootstrap-datepicker/js/bootstrap-datepicker_pt-br');

    $this->load->model('categorias_model');
    $data['categorias'] = $this->categorias_model->getAllCategorias();
    $this->load->view('novidades_adicionar_view',$data);
  }

  public function editar($id){

    if($this->input->post('novidade_editar')==="true"){

      $sessao['campos_preenchidos'] = array();
      $sessao['campos_preenchidos']['titulo'] = $this->input->post('titulo');
      $sessao['campos_preenchidos']['resumo'] = $this->input->post('resumo');
      $sessao['campos_preenchidos']['conteudo'] = $this->input->post('conteudo');
      $sessao['campos_preenchidos']['categoria'] = $this->input->post('categoria');
      $sessao['campos_preenchidos']['publicacao'] = $this->input->post('publicacao');
      $sessao['campos_preenchidos']['status'] = $this->input->post('status');
      $sessao['campos_preenchidos']['url_download'] = $this->input->post('url_download');
      $sessao['campos_preenchidos']['data'] = $this->input->post('data');
      $sessao['campos_preenchidos']['cor'] = $this->input->post('cor');

      $erro_envio = false;

      //debugVarAndDie($_FILES);

      if($_FILES['imagem']['name']){

        $file_name = explode(".",$_FILES['imagem']['name']);
        $file_name = strtolower(removeAccents($file_name[0]))."_".date("h_m_-_d-m-Y",time()).".".$file_name[1];

        $config['file_name'] = $file_name;
        $config['overwrite'] = TRUE;
        $config['allowed_types'] = 'jpg|jpeg|gif|png';
        $config['max_size'] = 2000;
        $config['upload_path'] = "./files/uploads/imagens";

        $this->load->library('upload', $config);
        if(!$this->upload->do_upload('imagem')){
          $data['upload_error_msg'] = true;
          $erro_envio = true;
        }

        $config['image_library'] = 'gd2';
        $config['source_image'] = $config['upload_path'].'/'.$config['file_name'];
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width']  = 858;
        $config['height'] = 490;
        $config['master_dim'] = 'width';
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();
      }
      if(!isset($file_name)){
        $file_name = null;
      }

      if($this->input->post('imagem_hidden') AND $file_name==null){
        $sessao['campos_preenchidos']['imagem'] = $this->input->post('imagem_hidden');
      }else{
        $sessao['campos_preenchidos']['imagem'] = $file_name;
      }

      $data_insert = array(
        'titulo' => $this->input->post('titulo'),
        'tag' => strtolower(removeAccents($this->input->post('titulo'))),
        'resumo' => $this->input->post('resumo'),
        'conteudo' => $this->input->post('conteudo'),
        'categoria_id' => $this->input->post('categoria'),
        'publicacao' => $this->input->post('publicacao'),
        'status' => $this->input->post('status'),
        'imagem' => $sessao['campos_preenchidos']['imagem'],
        'modified' => date("Y-m-d H:i:s"),
        'url_download' => $this->input->post('url_download'),
        'data' => convertBRtoUSdate($this->input->post('data')),
        'cor' => $sessao['campos_preenchidos']['cor']
      );

      // Em caso publicacao seja ativada
      if($this->input->post('publicacao')==1){
        if($_FILES['arquivo']['name']!=''){
          $file_name = explode(".",$_FILES['arquivo']['name']);
          $file_name = strtolower(removeAccents($file_name[0]))."_".date("h_m_-_d-m-Y",time()).".".$file_name[1];

          $config['file_name'] = $file_name;
          $config['overwrite'] = TRUE;
          $config['allowed_types'] = 'jpg|jpeg|gif|png|doc|zip|rar|pdf|txt';
          $config['max_size'] = 12000;
          $config['upload_path'] = "./files/uploads/arquivos";

          $this->load->library('upload', $config);
          $this->upload->initialize($config);
          if(!$this->upload->do_upload('arquivo')){
            $data['upload_arquivo_error_msg'] = true;
            $erro_envio = true;
          }else{
            $data_insert['arquivo_download'] = $file_name;
          }
        }

        if($this->input->post('arquivo_hidden')){
          $sessao['campos_preenchidos']['arquivo'] = $this->input->post('arquivo_hidden');
        }else{
          $sessao['campos_preenchidos']['arquivo'] = $file_name;
        }

        $data_insert['cadastro_download'] = $this->input->post('download_cadastro');
        $sessao['campos_preenchidos']['download_cadastro'] = $this->input->post('download_cadastro');

        // Em caso do formulário ser ativado
        if($this->input->post('download_cadastro')==1){
          $fields = '';
          if($this->input->post('download_form_nome')=="sim"){
            $fields.= '{Nome}';
          }
          if($this->input->post('download_form_email')=="sim"){
            $fields.= '{E-mail}';
          }
          if($this->input->post('download_form_telefone')=="sim"){
            $fields.= '{Telefone}';
            $sessao['campos_preenchidos']['download_form_telefone'] = $this->input->post('download_form_telefone');
          }
          if($this->input->post('download_form_cidade')=="sim"){
            $fields.= '{Cidade}';
            $sessao['campos_preenchidos']['download_form_cidade'] = $this->input->post('download_form_cidade');
          }
          if($this->input->post('download_form_mensagem')=="sim"){
            $fields.= '{Mensagem}';
            $sessao['campos_preenchidos']['download_form_mensagem'] = $this->input->post('download_form_mensagem');
          }
          $data_insert['formulario_download'] = $fields;
        }
      }

      if($erro_envio){
        //$this->session->set_userdata('campos_preenchidos',$sessao['campos_preenchidos']);
        //redirect(base_url().'fatorcms/novidades/editar/'.$id);
        $data['campos_preenchidos'] = $sessao['campos_preenchidos'];
      }else{
        unset($data['upload_arquivo_error_msg']);
        unset($data['upload_arquivo_error_msg']);
      }

      $this->load->model('novidades_model');
      if($this->novidades_model->updateNovidade($id,$data_insert) AND $erro_envio==false){
        $this->session->set_flashdata('update_msg',true);
        $sessao['campos_preenchidos'] = array();
        redirect(base_url().'fatorcms/novidades');
      }
    }else{
        $this->load->model('novidades_model');
        $novidade = $this->novidades_model->getNovidade($id);

        $sessao['campos_preenchidos'] = array();
        $sessao['campos_preenchidos']['titulo'] = $novidade->titulo;
        $sessao['campos_preenchidos']['resumo'] = $novidade->resumo;
        $sessao['campos_preenchidos']['conteudo'] = $novidade->conteudo;
        $sessao['campos_preenchidos']['categoria'] = $novidade->categoria_id;
        $sessao['campos_preenchidos']['publicacao'] = $novidade->publicacao;
        $sessao['campos_preenchidos']['status'] = $novidade->status;
        $sessao['campos_preenchidos']['data'] = convertUStoBRdate($novidade->data);
        $sessao['campos_preenchidos']['cor'] = $novidade->cor;

        if($novidade->imagem!=null){
            $sessao['campos_preenchidos']['imagem'] = $novidade->imagem;
        }
        if($novidade->arquivo_download!=null){
            $sessao['campos_preenchidos']['arquivo_download'] = $novidade->arquivo_download;
        }
        $sessao['campos_preenchidos']['download_cadastro'] = $novidade->cadastro_download;

      $sessao['url_download'] = $novidade->url_download;

        $campos = explode("{",$novidade->formulario_download);
        if(in_array("Mensagem}",$campos)){ $sessao['campos_preenchidos']['download_form_mensagem'] = true; }
        if(in_array("Telefone}",$campos)){ $sessao['campos_preenchidos']['download_form_telefone'] = true; }
        if(in_array("Cidade}",$campos)){ $sessao['campos_preenchidos']['download_form_cidade'] = true; }

        $data['campos_preenchidos'] = $sessao['campos_preenchidos'];
    }

    /* DatePicker */
    $this->loadCSSVendor('bootstrap-datepicker/css/datepicker');
    $this->loadJSVendorBottom('bootstrap-datepicker/js/bootstrap-datepicker_pt-br');

    $this->load->model('categorias_model');
    $data['categorias'] = $this->categorias_model->getAllCategorias();
    $this->load->view('novidades_editar_view',$data);
  }

  public function excluir($id){
    $this->isAjax();
    $this->load->model('novidades_model');
    $item = $this->novidades_model->getNovidade($id);
    if(file_exists('./files/uploads/imagens/'.$item->imagem)){
      unlink('./files/uploads/imagens/'.$item->imagem);
    }
    if(file_exists('./files/uploads/arquivos/'.$item->arquivo_download)){
      unlink('./files/uploads/arquivos/'.$item->arquivo_download);
    }
    if($this->novidades_model->deleteNovidade($id)){
      $this->session->set_flashdata('delete_success',true);
    }else{
      $this->session->set_flashdata('error_msg',true);
    }
    redirect(base_url().'fatorcms/novidades');
  }

  public function downloads(){
      /* Dynamic Tables Block */
    $this->loadCSSVendor(vendor_dynamic_tables_css());
    $this->loadJSVendorBottom(vendor_dynamic_tables_js());
    $this->loadJSBottom(dynamic_tables_js());
    /* End - Dynamic Tables Block */

    $data = array();
    $this->load->model('novidades_downloads_model');
    $data['downloads'] = $this->novidades_downloads_model->getAllNovidadesDownloads();

    $this->load->view('novidades_downloads_view',$data);
  }

  public function importar_blog(){

    $this->load->model('novidades_model');
    $posts = $this->novidades_model->getBlogPosts();

    // 2 Livros, 3 Artigos, 4 Notícias, 5 Depoimentos, 6 Prêmios
    //$html = new DOMDocument();

    $import_success = 0;
    $import_erros = array();

    function download_remote_file($file_url, $save_to) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_POST, 0);
      curl_setopt($ch, CURLOPT_URL, $file_url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $file_content = curl_exec($ch);
      curl_close($ch);
      $downloaded_file = fopen($save_to, 'w');
      if($downloaded_file){
        fwrite($downloaded_file, $file_content);
        fclose($downloaded_file);
      }
    }


    foreach($posts as $post){
      if($post->post_status=='publish'){

      $data_insert = array(
        'titulo' => $post->post_title,
        'tag' => $post->post_name,
        'conteudo' => $post->post_content,
        'creation' => $post->post_date_gmt,
        'modified' => $post->post_modified_gmt,
        'categoria_id' => 2,
        'status' => 1
      );

      // Gera o Resumo
      if(trim(strip_tags($post->post_content))!=""){
        if(strlen(trim(strip_tags(trim(strip_tags($post->post_content)))))>265){
          $resumo = substr(trim(strip_tags($post->post_content)), 0, 265);
          $resumo = explode(' ',$resumo);
          array_pop($resumo);
          $resumo = implode(' ',$resumo);
          $resumo = trim($resumo);
        }else{
          $resumo = trim(strip_tags($post->post_content));
        }
        $data_insert['resumo'] = $resumo."...";

        $html = new DOMDocument();
        $html->loadHTML((string)$post->post_content);
        $search = $html->getElementsByTagName('img');

        $html_img = "";
        foreach($search as $item){
          $html_img = $item->getAttribute('src');
        }

        if(trim($html_img)!=""){
          $link = explode("/",$html_img);
          $link = array_reverse($link);
          $new_img = $link[2]."_".$link[1]."_".$link[0];
          $new_img = explode(".",$new_img);
          $new_img = strtolower(removeAccents($new_img[0])).".".$new_img[1];

          echo $new_img."<br />";

          download_remote_file($html_img, "./files/uploads/imagens/".$new_img);
          $data_insert['imagem'] = $new_img;
        }

      }





        // "./files/uploads/arquivos"




      if($this->novidades_model->insertNovidade($data_insert)){
        $import_success++;
      }else{
        $import_erros[] = $post->ID;
      }




      /* Script de tratamento de src dos imgs
      $html->loadHTML((string)$post->post_content);
      $search = $html->getElementsByTagName('img');

      $html_img = array();
      $c = 0;
      foreach($search as $item){
        $html_img[$c]['old'] = $item->getAttribute('src');
        $link = explode("/",$item->getAttribute('src'));
        $link = array_reverse($link);
        $link = "old_site/".$link[2]."/".$link[1]."/".$link[0];
        $html_img[$c]['new'] = $link;
        $c++;
      }
      foreach($html_img as $img_src){
        $post->post_content = str_replace($img_src['old'],$img_src['new'],$post->post_content);
      }
      debugVarAndDie($post->post_content);
      */

      }
    }

    /*
    echo "Importados: ".$import_success;
    echo '<br />';
    echo "Erros: ".count($import_erros);
    debugVarAndDie($import_erros);
    */

    die;
  }

}