<?php

class novidades_model extends FD_Model {

  function __construct() {
    parent::__construct();
  }

  public function getAllNovidades(){
    //$this->db->select('fd_marchezan_novidades.*');
    //$this->db->join('fd_marchezan_categorias','fd_marchezan_novidades.categoria_id=fd_marchezan_categorias.id','inner');
    $this->db->order_by('fd_marchezan_novidades.data','DESC');
    return $this->db->get('fd_marchezan_novidades')->result();
  }

  public function getNovidade($id){
    $this->db->where('fd_marchezan_novidades.id',$id);
    return $this->db->get('fd_marchezan_novidades')->row();
  }

  public function insertNovidade($data){
    if($this->db->insert('fd_marchezan_novidades',$data)){
      return true;
    }else{
      return false;
    }
  }

  public function updateNovidade($id,$data){
    $this->db->where('id',$id);
    if($this->db->update('fd_marchezan_novidades',$data)){
      return true;
    }else{
      return false;
    }
  }

  public function deleteNovidade($id){
    if($this->db->delete('fd_marchezan_novidades', array('id' => $id))){
      return true;
    }
    return false;
  }

  public function getBlogPosts(){
    $db2 = $this->load->database('blog', TRUE);
    $query = "SELECT p.*, t.term_id, t.name as category_name, t.slug as category_tag
FROM wp_posts p
LEFT JOIN wp_term_relationships rel ON rel.object_id = p.ID
LEFT JOIN wp_term_taxonomy tax ON tax.term_taxonomy_id = rel.term_taxonomy_id
LEFT JOIN wp_terms t ON t.term_id = tax.term_id";
    return $db2->query($query)->result();
    //return $db2->get('wp_posts')->result();
  }


} 