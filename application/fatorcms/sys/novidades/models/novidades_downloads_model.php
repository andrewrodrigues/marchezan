<?php

class novidades_downloads_model extends FD_Model {

    function __construct() {
        parent::__construct();
    }

    public function getAllNovidadesDownloads(){
        return $this->db->get('fd_marchezan_novidades_downloads')->result();
    }


} 