<aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->
        <div class="leftside-navigation">
        <ul class="sidebar-menu" id="nav-accordion">
            <li>
                <a href="<?php echo getLink('dashboard'); ?>" <?=$this->uri->segment(2)=='dashboard' ? 'class="active"':''; ?>>
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="sub-menu">
                <a href="javascript:;" <?php if($this->uri->segment(2)=='novidades' and $this->uri->segment(3)!='downloads'){ echo 'class="active"'; } ?>>
                    <i class="fa fa-edit"></i>
                    <span>Novidades</span>
                </a>
                <ul class="sub">
                    <li <?=$this->uri->uri_string()=='fatorcms/novidades' ? 'class="active"':''; ?>><a href="<?php getLink('novidades') ?>">Listar Novidades</a></li>
                    <li <?=$this->uri->uri_string()=='fatorcms/novidades/adicionar' ? 'class="active"':''; ?>><a href="<?php getLink('novidades/adicionar') ?>">Cadastrar Novidades</a></li>
                    <!-- <li <?=$this->uri->uri_string()=='fatorcms/categorias' ? 'class="active"':''; ?>><a href="<?php getLink('categorias') ?>">Listar Categorias</a></li>
                    <li <?=$this->uri->uri_string()=='fatorcms/categorias/adicionar' ? 'class="active"':''; ?>><a href="<?php getLink('categorias/adicionar') ?>">Cadastrar Categorias</a></li> -->
                </ul>
            </li>
            <li>
                <a href="<?php echo getLink('newsletter') ?>"<?php echo $this->uri->segment(2)=='newsletter' ? ' class="active"':''; ?>>
                    <i class="fa fa-list-alt"></i>
                    <span>Newsletter</span>
                </a>
            </li>
               <!--  <li class="sub-menu">
                 <a href="javascript:;" <?=$this->uri->segment(2)=='galeria' ? 'class="active"':''; ?>>
                   <i class="fa fa-th"></i>
                   <span>Galeria de Imagens</span>
                 </a>
                 <ul class="sub">
                   <li <?=$this->uri->uri_string()=='fatorcms/galeria' ? 'class="active"':''; ?>><a href="<?php getLink('galeria') ?>">Gerenciar Galeria</a></li>
                 </ul>
             </li> -->
                <!-- <li class="sub-menu">
                  <a href="javascript:;" <?php if($this->uri->segment(2)=='novidades' and $this->uri->segment(3)=='downloads'){ echo 'class="active"'; } ?>>
                    <i class="fa fa-list-alt"></i>
                    <span>Captação de Formulários</span>
                  </a>
                  <ul class="sub">
                    <li <?=$this->uri->uri_string()=='fatorcms/novidades/downloads' ? 'class="active"':''; ?>><a href="<?php getLink('novidades/downloads'); ?>">Listar Formulários Enviados</a></li>
                  </ul>
              </li> -->
              <li>
                <a href="javascript:;" <?=$this->uri->segment(2)=='usuarios' ? 'class="active"':''; ?>>
                    <i class="fa fa-users"></i>
                    <span>Usuários</span>
                </a>
                <ul class="sub">
                  <li <?=$this->uri->uri_string()=='fatorcms/usuarios' ? 'class="active"':''; ?>><a href="<?php getLink('usuarios') ?>">Listar Usuários</a></li>
                  <li <?=$this->uri->uri_string()=='fatorcms/usuarios/adicionar' ? 'class="active"':''; ?>><a href="<?php getLink('usuarios/adicionar') ?>">Cadastrar Usuários</a></li>
              </ul>
          </li>
          <li>
            <a href="<?php getLink('logout'); ?>">
                <i class="fa fa-key"></i>
                <span>Logout</span>
            </a>
        </li>
    </ul></div>
    <!-- sidebar menu end-->
</div>
</aside>