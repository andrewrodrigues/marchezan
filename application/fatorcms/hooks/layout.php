<?php
/**
 * MK CodeIgniter
 * fb.com/mkethur
 *
 * layout.php
 *
 * Version: 1.0.0
 *
 * Date: 27 / 02 / 2014
 */

if (!defined('BASEPATH'))exit('No direct script access allowed');

class Layout {

    private $js;
    private $css;
    private $js_bottom;
    public $base_url;

    public function init() {
        // Instancia do CI.
        $CI = & get_instance();

        // Se layout estiver definido e a regexp nao bater.
        if (isset($CI->layout) && !preg_match('/(.+).php$/', $CI->layout)) {
            $CI->layout .= '.php';
        } else {
            $CI->layout = 'default_layout.php';
        }

        // Definindo caminho completo do layout.
        $layout = LAYOUTPATH . $CI->layout;

        // Se o layout for diferente do default, e o arquivo nao existir.
        if ($CI->layout !== 'default_layout.php' && !file_exists($layout)) {
            // Exibe a mensagem, se o layout for diferente de '.php'.
            if ($CI->layout != '.php') {
                show_error("You have specified a invalid layout: " . $CI->layout);
            }
        }
        
        $this->js = JSPATH;
        $this->css = CSSPATH;
        $this->css_vendor = VENDORPATH;
        $this->js_vendor = VENDORPATH;
        
        // Definindo o base_url.
        $this->base_url = $CI->config->slash_item('base_url');
        // Pegando a saida que o CI gera normalmente.
        $output = $CI->output->get_output();
        // Pegando o valor de title, se definido no controller.
        $title = (isset($CI->title)) ? $CI->title : '';
        // Links CSS definidos no controlador.
        $css = (isset($CI->css) || isset($CI->css_vendor)) ? $this->createCSSLinks($CI->css,$CI->css_vendor) : '';
        // Links JS definidos no controlador.
        $js = (isset($CI->js) || isset($CI->js_vendor)) ? $this->createJSLinks($CI->js,$CI->js_vendor) : '';
        // Links JS localizados no bottom da pagina definidos pelo controlador
        $js_bottom = (isset($CI->js_bottom) || isset($this->js_vendor_bottom)) ? $this->createJSLinks($CI->js_bottom,$CI->js_vendor_bottom) : '';

        if($CI->session->userdata('user_name')!=''){
            $user_name = $CI->session->userdata('user_name');
        }else{ $user_name = ''; }

        // Se o arquivo layout existir.
        if (file_exists($layout)) {
            $layout = $CI->load->file($layout, true);
            $view = str_replace('{content_for_layout}', $output, $layout);
            $view = str_replace('{title_for_layout}', $title, $view);            
            $view = str_replace('{user_name}', $user_name, $view);
            $view = str_replace('{css_for_layout}', $css, $view);
            $view = str_replace('{js_for_layout}', $js, $view);            
            $view = str_replace('{js_for_layout_bottom}', $js_bottom, $view);
            if(isset ($CI->extra) && is_array($CI->extra)){
                foreach($CI->extra as $key => $content){
                    $view = str_replace($key, $content, $view);
                }
            }
        } else {
            $view = $output;
        }

        echo $view;
    }

    private function createCSSLinks($links,$links_vendor) {
        $html = "";

        foreach ($links as $link) {
            $type = substr($link, 0, 4);
            if($type == 'http'){
                $html .= "<link rel='stylesheet' type='text/css' href='" . $link . "' media='screen' />\n\t";
            } else {
                $html .= "<link rel='stylesheet' type='text/css' href='" . $this->base_url . $this->css . $link . ".css' media='screen' />\n\t";
            }
        }

        if(isset($links_vendor)){
            foreach ($links_vendor as $link) {
                $type = substr($link, 0, 4);
                if($type == 'http'){
                    $html .= "<link rel='stylesheet' type='text/css' href='" . $link . "' media='screen' />\n\t";
                } else {
                    $html .= "<link rel='stylesheet' type='text/css' href='" . $this->base_url . $this->css_vendor . $link . ".css' media='screen' />\n\t";
                }
            }
        }

        return $html;
    }

    private function createJSLinks($links, $links_vendor) {
        $html = "";

        if(isset($links_vendor)){
            foreach ($links_vendor as $link) {
                $type = substr($link, 0, 4);
                if($type == 'http'){
                    $html .= "<script type='text/javascript' src='" . $link . "'></script>\n\t";
                } else {
                    $html .= "<script type='text/javascript' src='" . $this->base_url . $this->js_vendor . $link . ".js'></script>\n\t";
                }
            }
        }
        foreach ($links as $link) {
            $type = substr($link, 0, 4);
            if($type == 'http'){
                $html .= "<script type='text/javascript' src='" . $link . "'></script>\n\t";
            } else {
                $html .= "<script type='text/javascript' src='" . $this->base_url . $this->js . $link . ".js'></script>\n\t";
            }
        }

        return $html;
    }

    private function createMetaTags($links) {
        $html = "";

        foreach ($links as $name => $content) {
            $type = substr($name, 0, 2);
            if($type == 'og'){
                $html .= "<meta property='" . $name . "' content='" . $content . "' />\n\t";
            } else {
                $html .= "<meta name='" . $name . "' content='" . $content . "' />\n\t";
            }
        }
        return $html;
    }
    
    
}