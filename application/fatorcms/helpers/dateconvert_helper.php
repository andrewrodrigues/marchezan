<?php


function convertUStoBRdate($date){
  $date = trim($date);
  $date = implode("/",array_reverse(explode("-",$date)));
  return $date;
}

function convertBRtoUSdate($date){
  $date = trim($date);
  $date = implode("-",array_reverse(explode("/",$date)));
  return $date;
}