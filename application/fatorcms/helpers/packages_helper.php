<?php
/**
 * MK CodeIgniter
 * fb.com/mkethur
 *
 * packages_helper.php
 *
 * Version: 1.0.0
 *
 * Date: 27 / 02 / 2014
 */

/**
 * vendor_dynamic_tables_css
 *
 * @return array List of vendor css needs of Dynamic Tables
 */
function vendor_dynamic_tables_css(){
    return array(
        'advanced-datatable/media/css/demo_page',
        'advanced-datatable/media/css/demo_table',
        'data-tables/DT_bootstrap'
    );
}

/**
 * vendor_dynamic_tables_js
 *
 * @return array List of vendor js needs of Dynamic Tables
 */
function vendor_dynamic_tables_js(){
    return array(
        'advanced-datatable/media/js/jquery.dataTables',
        'data-tables/DT_bootstrap'
    );
}

/**
 * dynamic_tables_js
 *
 * @return array List of js needs of Dynamic Tables
 */
function dynamic_tables_js(){
    return array(
        'dynamic_table/dynamic_table_init'
    );
}

