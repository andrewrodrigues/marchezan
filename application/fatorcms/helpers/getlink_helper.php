<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * MK CodeIgniter
 * fb.com/mkethur
 *
 * getlink_helper.php
 *
 * Version: 1.0.1
 *
 * Date: 28 / 02 / 2014
 */


/**
 * getLink
 * Retorna echo da URL completa /{parametro}
 *
 * @param $string String
 */
function getLink($string='') {
  echo base_url().'fatorcms/'.$string;
}

/**
 * getIimg
 * Retorna echo da URL completa da imagem na pasta /files/{modulo}/img
 *
 * @param $img String
 */
function getImg($img){
  echo base_url().IMGPATH.$img;
}

/**
 * getUplodadedFile
 * Retorna echo da URL completa do arquivo na pasta /files/uploads
 *
 * @param $file String
 */
function getUploadedFile($file){
  echo base_url().UPLOADSPATH.$file;
}

/**
 * getCss
 * Retorna echo da URL completa do arquivo stylesheet de /files/{modulo}/css/{parametro}.css
 *
 * @param $css String
 */
function getCss($css){
    echo base_url().CSSPATH.$css.'.css';
}

/**
 * getJs
 * Retorna echo da URL completa do arquivo javascript de /files/{modulo}/js/{parametro}.js
 *
 * @param $js String
 */
function getJs($js){
    echo base_url().JSPATH.$js.'.js';
}

?>
