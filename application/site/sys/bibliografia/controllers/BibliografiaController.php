<?php

class BibliografiaController extends FD_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('novidades_model');
	}

	public function index()
	{
		$head['title'] = 'Deputado Marchezan';
	    $head['css'] = array('bootstrap','style','metrojs');
	    $this->setHead('default_head',$head);

	    $header['classe'] = 'body-bibliografia';
	    $this->setHeader('default_header', $header);

	    $this->load->library('fb_connect');
	    $face = $this->fb_connect->getPost();

	    $retn_face = array();
	    foreach($face['data'] as $k => $fc){
	      if($fc['type'] != "status"){
	        $retn_face[] = $fc;
	        break;
	      }
	    }

	    $data['retn_face'] = $retn_face;

	    $data['novidades'] = $this->novidades_model->getAllNovidades();
	    $this->setContent('index',$data);

	    $footer['js'] = array('jquery','index','jquery-ui','metrojs','main','plugins');
	    $this->setFooter('default_footer', $footer);
	}

}