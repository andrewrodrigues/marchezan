<?php

class IndexController extends FD_Controller {
  
  public function __construct() {
    parent::__construct();
    $this->load->model('novidades_model');
  }
  
  public function index(){

    $this->load->library('fb_connect');
    $face = $this->fb_connect->getPost();

    $retn_face = array();
    foreach($face['data'] as $k => $fc){
      if($fc['type'] != "status"){
        $retn_face[] = $fc;
        break;
      }
    }

    $data['retn_face'] = $retn_face;

    $head['title'] = 'Deputado Marchezan';
    $head['css'] = array('bootstrap','style','metrojs');
    $this->setHead('default_head',$head);

    $this->setHeader('default_header');

    $data['banner'] = $this->novidades_model->getAllNovidades(3);

    $data['novidades'] = $this->novidades_model->getAllNovidades();
    $this->setContent('home',$data);

    $footer['js'] = array('jquery','index','jquery-ui','metrojs','plugins','projetos','biografia','novidades','main');

    $this->setFooter('default_footer', $footer);
  }
}
