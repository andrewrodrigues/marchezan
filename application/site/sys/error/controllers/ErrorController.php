<?php
/**
 * Created by PhpStorm.
 * User: markus.ethur
 * Date: 19/02/14
 * Time: 16:02
 */

class ErrorController extends FD_Controller {

    public function __construct() {
        $this->setMenu('internas_menu');
        $this->setFooter('default_footer');
        parent::__construct();
    }

    public function error_404(){
        $this->setTitle('Erro 404 - Página não encontrada');
        $this->load->view('error_404');
    }

}