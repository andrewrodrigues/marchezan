<?php

class ContatoController extends FD_Controller {

    private $emailReceiver = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->library('email');
		$this->load->helper('url');
		$this->load->model('newsletter_model');
        if (ENVIRONMENT == 'production')
            $this->emailReceiver = 'contato@marchezan.com.br';
        else {
            $this->emailReceiver = 'andrew.rodrigues@fatordigital.com.br';
        }
	}

	public function index()
	{
		$this->email->initialize();

		if ($this->input->post())
		{
			$this->email->from($this->input->post('email'), $this->input->post('nome'));
			$this->email->to($this->emailReceiver);
			$this->email->subject('Contato pelo site');

			$mensagem = 'Nome: ' . $this->input->post('nome') . "\r\n";
			$mensagem .= 'E-mail: ' . $this->input->post('email') . "\r\n";
			$mensagem .= 'Telefone: ' . $this->input->post('telefone') . "\r\n";
			$mensagem .= 'Mensagem: ' . $this->input->post('mensagem');

			$this->email->message($mensagem);

			if ($this->email->send()) {
				echo 'true';
			} else {
				echo 'false';
			}
		} else {
			redirect(base_url());
		}
	}

	public function newsletter()
	{
		if ($this->input->post())
		{
			if ($this->newsletter_model->insert($this->input->post('email')))
			{
				echo 'true';
			} else {
				echo 'false';
			}
		} else {
			redirect(base_url());
		}
	}

}