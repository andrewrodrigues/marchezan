<?php

class newsletter_model extends FD_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function insert($email)
	{
		if ($this->existEmail($email) == 0)
		{
			$this->db->insert('fd_marchezan_newsletter', array('email' => $email));
			return true;
		} else {
			return false;
		}
	}

	public function existEmail($email)
	{
		$this->db->where('email',$email);
		$this->db->from('fd_marchezan_newsletter');
		return $this->db->count_all_results();
	}

}