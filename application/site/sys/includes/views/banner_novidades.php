<?php if(!empty($banner)): ?>
<section class="banner-novidades">
	<div class="container">
		<div id="banner-conceitual" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 slider-novidades carousel slide carousel-fade" data-ride="carousel" data-pause="false">
			<ul class="ui-tabs-nav novidades carousel-indicators">
				<li class="ui-tabs-nav-item" data-target="#banner-conceitual" data-slide-to="0" style="display:none"></li>
			<?php foreach($banner as $i => $novidade): ?>
			<?php $i = $i + 1; ?>
				<li class="ui-tabs-nav-item" data-target="#banner-conceitual" data-slide-to="<?php echo $i ?>">
					<a class="abre-novidade" href="#novidade" id="<?php echo $novidade->id ?>" data-url="<?php echo getLink('novidade/'.$novidade->id.'/'.$novidade->tag) ?>" title="<?php echo $novidade->titulo ?>" OnMouseOver="$('#banner-conceitual').carousel(<?php echo $i ?>)" OnMouseOut="$('#banner-conceitual').carousel()"><?php echo $novidade->titulo ?></a>
				</li>
			<?php endforeach; ?>
			</ul>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 carousel-inner" role="listbox">
                <div class="item active">
                    <img alt="" src="<?php echo getImg('chamada.jpg') ?>" class="img-responsive">
                </div>
				<?php foreach($banner as $i => $novidade): ?>
				<?php $novidade->imagem = explode('.', $novidade->imagem); ?>
				<?php $novidade->imagem = $novidade->imagem[0].'_thumb.'.$novidade->imagem[1]; ?>
				<?php $i = $i + 1; ?>
				<div class="item">
					<a class="abre-novidade" id="<?php echo $novidade->id ?>" href="#novidade" data-url="<?php echo getLink('novidade/'.$novidade->id.'/'.$novidade->tag) ?>" title="<?php echo $novidade->titulo ?>">
						<img class="img-responsive" src="<?php echo getUploadedFile('imagens/'.$novidade->imagem) ?>" alt="<?php echo $novidade->titulo ?>">
					</a>
				</div>
				<?php endforeach; ?>
            </div>
		</div>
	</div>
</section>
<?php else: ?>
<section class="banner" style="height: 645px;">
    <img src="<?php echo getImg('banner-mobile.png') ?>" alt="Marchezan - 4545" class="img-responsive fullsize-images hidden-lg hidden-md hidden-sm">
    <img src="<?php echo getImg('banner-ipad.gif') ?>" alt="Marchezan - 4545" class="img-responsive fullsize-images hidden-lg hidden-md hidden-xs">
    <div class="banner hidden-sm hidden-xs" style="position: absolute; width: 1920px; left: 50%; top: 0; margin: 0 0 0 -960px; height: 645px;">
        <div id="swiffycontainer" style="width: 1920px; height: 645px">
        </div>
    </div>
</section>
<?php endif; ?>