<section class="download">
	<div class="container">
		<h2 class="padrao text-center text-left-xs">Multimídia</h2>
		<h3 class="padrao text-center text-left-xs">Mostre que você apoia a <span>política sem medo da verdade.</span><br />Baixe <span>gratuitamente</span> os materiais de campanha e <span>espalhe</span> pela rede.</h3>
		<div class="boxes-download">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container-download">
				<!-- <div class="col-lg-2 col-lg-offset-3 col-md-4 col-sm-6 col-xs-12">
					<div class="box-download">
						<a href="#" title="Papéis de parede" class="abre-download" data-id="1">PAPÉIS DE<br />PAREDE</a>
						<a href="#" title="Papéis de parede" class="abre-download" data-id="1">
							<img src="<?php echo getImg('wallpaper-ico.jpg') ?>" alt="Papéis de parede" class="img-responsive">
						</a>
						<a href="#" class="fixed" title="Papéis de parede" class="abre-download" data-id="1"></a>
					</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
					<div class="box-download">
						<a href="#" title="Ringtones" class="abre-download" data-id="2">RINGTONES</a>
						<a href="#" title="Ringtones" class="abre-download" data-id="2">
							<img src="<?php echo getImg('ringtone-ico.jpg') ?>" alt="Ringtones" class="img-responsive">
						</a>
						<a href="#" class="fixed" title="Ringtones" class="abre-download" data-id="2"></a>
					</div>
				</div> -->
				<div class="col-lg-2 col-lg-offset-5 col-md-4 col-sm-6 col-xs-12">
					<div class="box-download">
						<a href="#" title="Jingle" class="abre-download" data-id="3">JINGLES</a>
						<a href="#" title="Jingle" class="abre-download" data-id="3">
							<img src="<?php echo getImg('jingle-ico.jpg') ?>" alt="Jingle" class="img-responsive">
						</a>
						<a href="#" class="fixed" title="Jingle" class="abre-download" data-id="3"></a>
					</div>
				</div>
				<!-- <div class="col-lg-2 col-lg-offset-3 col-md-4 col-sm-6 col-xs-12">
					<div class="box-download">
						<a href="#" title="Fotos" class="abre-download" data-id="4">FOTOS</a>
						<a href="#" title="Fotos" class="abre-download" data-id="4">
							<img src="<?php echo getImg('fotos-ico.jpg') ?>" alt="Fotos" class="img-responsive">
						</a>
						<a href="#" class="fixed" title="Fotos" class="abre-download" data-id="4"></a>
					</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
					<div class="box-download">
						<a href="#" title="Peças gráficas" class="abre-download" data-id="5">PEÇAS<br />GRÁFICAS</a>
						<a href="#" title="Peças gráficas" class="abre-download" data-id="5">
							<img src="<?php echo getImg('pecas-graficas-ico.jpg') ?>" alt="Ringtones" class="img-responsive">
						</a>
						<a href="#" class="fixed" title="Peças gráficas" class="abre-download" data-id="5"></a>
					</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
					<div class="box-download">
						<a href="#" title="Emoticons e avatares" class="abre-download" data-id="6">EMOTICONS<br />E AVATARES</a>
						<a href="#" title="Emoticons e avatares" class="abre-download" data-id="6">
							<img src="<?php echo getImg('emoticon-ico.jpg') ?>" alt="Emoticons e avatares" class="img-responsive">
						</a>
						<a href="#" class="fixed" title="Emoticons e avatares" class="abre-download" data-id="6"></a>
					</div>
				</div> -->
				<div class="download-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="download-1">
					<a href="#" class="fechar-download pull-right" id="fechar-download-1">Fechar Download</a>
					<h3 class="sem-linha">Papéis de parede</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium non autem deleniti at labore quia recusandae sunt earum, omnis molestias porro, corporis doloremque temporibus voluptatem. Qui rerum eum iure optio.</p>
					<p>Magni perferendis quas obcaecati. Impedit ut cum quos rem omnis consequatur sit, delectus veniam distinctio repudiandae itaque eum neque tenetur beatae, iste dicta labore consequuntur sapiente deleniti earum! Delectus, dolorem.</p>
					<p>Similique alias vitae fuga ullam, eius quos sequi molestias, tempora mollitia assumenda quaerat maiores quis. Dolore fuga, quis voluptas magni error sequi voluptatibus minus praesentium? Ex sequi deserunt, molestias maiores!</p>
					<p>Doloremque, facere, sint fuga cupiditate eum vero at, accusantium voluptates ut officiis eius. Dolorum atque nisi, iste? Veniam harum doloribus eveniet vel omnis. Velit voluptates illum rem libero obcaecati vitae?</p>
					<p>Deleniti, minima blanditiis repellendus error rerum amet commodi corporis voluptatem non temporibus vero ut, delectus libero cumque excepturi veniam soluta nisi quia facere ab et tempora labore assumenda. Reiciendis, cum.</p>
				</div>
				<div class="download-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="download-2">
					<a href="#" class="fechar-download pull-right" id="fechar-download-2">Fechar Download</a>
					<h3 class="sem-linha">Ringtones</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium non autem deleniti at labore quia recusandae sunt earum, omnis molestias porro, corporis doloremque temporibus voluptatem. Qui rerum eum iure optio.</p>
					<p>Magni perferendis quas obcaecati. Impedit ut cum quos rem omnis consequatur sit, delectus veniam distinctio repudiandae itaque eum neque tenetur beatae, iste dicta labore consequuntur sapiente deleniti earum! Delectus, dolorem.</p>
					<p>Similique alias vitae fuga ullam, eius quos sequi molestias, tempora mollitia assumenda quaerat maiores quis. Dolore fuga, quis voluptas magni error sequi voluptatibus minus praesentium? Ex sequi deserunt, molestias maiores!</p>
					<p>Doloremque, facere, sint fuga cupiditate eum vero at, accusantium voluptates ut officiis eius. Dolorum atque nisi, iste? Veniam harum doloribus eveniet vel omnis. Velit voluptates illum rem libero obcaecati vitae?</p>
					<p>Deleniti, minima blanditiis repellendus error rerum amet commodi corporis voluptatem non temporibus vero ut, delectus libero cumque excepturi veniam soluta nisi quia facere ab et tempora labore assumenda. Reiciendis, cum.</p>
				</div>
				<div class="download-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="download-3">
					<a href="#" class="fechar-download pull-right" id="fechar-download-3">Fechar Download</a>
					<h3 class="sem-linha">Jingle</h3>
					<p><a href="<?php echo base_url() . 'files/site/MARCHEZAN_Jingle_III.mp3' ?>" target="_blank">Download do jingle</a></p>
					
				</div>
				<div class="download-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="download-4">
					<a href="#" class="fechar-download pull-right" id="fechar-download-4">Fechar Download</a>
					<h3 class="sem-linha">Fotos</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium non autem deleniti at labore quia recusandae sunt earum, omnis molestias porro, corporis doloremque temporibus voluptatem. Qui rerum eum iure optio.</p>
					<p>Magni perferendis quas obcaecati. Impedit ut cum quos rem omnis consequatur sit, delectus veniam distinctio repudiandae itaque eum neque tenetur beatae, iste dicta labore consequuntur sapiente deleniti earum! Delectus, dolorem.</p>
					<p>Similique alias vitae fuga ullam, eius quos sequi molestias, tempora mollitia assumenda quaerat maiores quis. Dolore fuga, quis voluptas magni error sequi voluptatibus minus praesentium? Ex sequi deserunt, molestias maiores!</p>
					<p>Doloremque, facere, sint fuga cupiditate eum vero at, accusantium voluptates ut officiis eius. Dolorum atque nisi, iste? Veniam harum doloribus eveniet vel omnis. Velit voluptates illum rem libero obcaecati vitae?</p>
					<p>Deleniti, minima blanditiis repellendus error rerum amet commodi corporis voluptatem non temporibus vero ut, delectus libero cumque excepturi veniam soluta nisi quia facere ab et tempora labore assumenda. Reiciendis, cum.</p>
				</div>
				<div class="download-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="download-5">
					<a href="#" class="fechar-download pull-right" id="fechar-download-5">Fechar Download</a>
					<h3 class="sem-linha">Peças Gráficas</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium non autem deleniti at labore quia recusandae sunt earum, omnis molestias porro, corporis doloremque temporibus voluptatem. Qui rerum eum iure optio.</p>
					<p>Magni perferendis quas obcaecati. Impedit ut cum quos rem omnis consequatur sit, delectus veniam distinctio repudiandae itaque eum neque tenetur beatae, iste dicta labore consequuntur sapiente deleniti earum! Delectus, dolorem.</p>
					<p>Similique alias vitae fuga ullam, eius quos sequi molestias, tempora mollitia assumenda quaerat maiores quis. Dolore fuga, quis voluptas magni error sequi voluptatibus minus praesentium? Ex sequi deserunt, molestias maiores!</p>
					<p>Doloremque, facere, sint fuga cupiditate eum vero at, accusantium voluptates ut officiis eius. Dolorum atque nisi, iste? Veniam harum doloribus eveniet vel omnis. Velit voluptates illum rem libero obcaecati vitae?</p>
					<p>Deleniti, minima blanditiis repellendus error rerum amet commodi corporis voluptatem non temporibus vero ut, delectus libero cumque excepturi veniam soluta nisi quia facere ab et tempora labore assumenda. Reiciendis, cum.</p>
				</div>
				<div class="download-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="download-6">
					<a href="#" class="fechar-download pull-right" id="fechar-download-6">Fechar Download</a>
					<h3 class="sem-linha">Emoticons e Avatares</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium non autem deleniti at labore quia recusandae sunt earum, omnis molestias porro, corporis doloremque temporibus voluptatem. Qui rerum eum iure optio.</p>
					<p>Magni perferendis quas obcaecati. Impedit ut cum quos rem omnis consequatur sit, delectus veniam distinctio repudiandae itaque eum neque tenetur beatae, iste dicta labore consequuntur sapiente deleniti earum! Delectus, dolorem.</p>
					<p>Similique alias vitae fuga ullam, eius quos sequi molestias, tempora mollitia assumenda quaerat maiores quis. Dolore fuga, quis voluptas magni error sequi voluptatibus minus praesentium? Ex sequi deserunt, molestias maiores!</p>
					<p>Doloremque, facere, sint fuga cupiditate eum vero at, accusantium voluptates ut officiis eius. Dolorum atque nisi, iste? Veniam harum doloribus eveniet vel omnis. Velit voluptates illum rem libero obcaecati vitae?</p>
					<p>Deleniti, minima blanditiis repellendus error rerum amet commodi corporis voluptatem non temporibus vero ut, delectus libero cumque excepturi veniam soluta nisi quia facere ab et tempora labore assumenda. Reiciendis, cum.</p>
				</div>
			</div>
		</div>
	</div>
	<a name="contato"></a>
</section>