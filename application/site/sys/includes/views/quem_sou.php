<a href="#biografia" class="go-bio"></a>
<a name="biografia"></a>
<section class="conteudos-dinamicos exibe-biografia">
	<div class="container">
		<div class="biografia">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
	    		<h2 class="padrao">Biografia</h2>
	    		<hr>
	    		<figure>
	    			<img src="<?php echo getImg('marchezan-biografia.jpg') ?>" alt="Deputador Nelson Marchezan Jr." class="fullsize-image img-responsive">
	    			<figcaption>
	    				Marchezan durante discurso na Assembléia Legislativa do RS - 2006
	    			</figcaption>
	    		</figure>
	    		<h3>Sobre Nelson Marchezan Júnior</h3>
	    		<p><strong>RAÍZES</strong><br>Marchezan é filho do ex-deputado federal Nelson Marchezan e da professora Maria Helena Bolsson Marchezan. É pai de Nelson Marchezan Neto. Passou parte da infância em Pantano Grande e Rio Pardo. Acompanhando a atividade do pai, morou em Porto Alegre, onde nasceu, e Brasília.</p>
	    		<p><strong>FORMAÇÃO PROFISSIONAL</strong><br>Marchezan é advogado, formado pela Unisinos (São Leopoldo-RS). Antes de se formar em Direito, estudou Educação Física (UFRGS) e Comunicação (Famecos-PUCRS). É pós-graduado em Gestão Empresarial pela Fundação Getúlio Vargas (FGV) e cursou a Escola da Ajuris (Associação dos Juízes do Rio Grande do Sul).<br><br>Exerceu a advocacia por oito anos, quando decidiu interromper a atividade por entender ser incompatível, sob ponto de vista ético, com a sua atuação parlamentar.<br><br>Ainda na área privada, foi diretor de Desenvolvimento, Agronegócios e Governos do Banco do Estado do Rio Grande do Sul (Banrisul). Nessa função, foi responsável por financiamentos para o setor produtivo e de fomento e pelas relações com o setor público, especialmente prefeituras.<br><br>Na sua gestão, trabalhou pela democratização da distribuição de recursos para a agricultura; auxiliou na implantação de gestão junto a prefeituras; implantou o Projeto Pescar, que formou já 199 jovens em Serviços Administrativos, possibilitando o primeiro emprego para 95% dos alunos; possibilitou o financiamento de todas as feiras agropecuárias do Estado, fomentando pequenas e médias empresas em todas as regiões; fundou o Departamento de Tradições Gaúchas do Centro Social e de Treinamento do Banrisul, conhecido como DTG Marca Gaúcha; e reestruturou a área de recursos para o desenvolvimento do banco.</p>
	    		<p><strong>NA ASSEMBLEIA GAÚCHA</strong><br>Em 2006, Marchezan foi eleito o deputado estadual mais votado da bancada do PSDB do Rio Grande do Sul. Em seu primeiro mandato na Assembleia gaúcha, foi eleito para quatro anos como presidente da Comissão de Finanças, Planejamento, Fiscalização e Controle e também como titular da Comissão de Constituição e Justiça.<br><br>Sua atuação teve destaque pelo combate a atos administrativos ilegais no Poder Judiciário, no Ministério Público, no Tribunal de Contas do Estado e na própria Assembleia Legislativa.<br><br>Também deixou sua marca por ser o autor de leis que garantem transparência na aplicação de recursos públicos: a lei da cotação eletrônica e do pregão eletrônico. Estas são as duas ferramentas mais modernas para o combate à corrupção, economia, transparência e agilidade no setor público.</p>
	    		<p><strong>EM BRASÍLIA</strong><br>Único deputado federal do PSDB gaúcho eleito em 2010, é conhecido por uma atuação versátil e marcante. Em seu primeiro mandato, foi apontado por dois anos consecutivos como um parlamentar em ascensão.<br><br>Atua em quatro comissões permanentes: Comissão de Finanças e Tributação; Comissão de Constituição e Justiça e de Cidadania; Comissão de Ciência e Tecnologia, Comunicação e Informática; e Comissão de Defesa do Consumidor.<br><br>Faz parte de diversas comissões especiais, como as que debatem a perda automática do mandado parlamentar, a primeira infância, o Plano Nacional de Educação, o Código de Processo Civil, a exploração sexual de crianças e adolescentes, o desarmamento e a identificação, registro e licenciamento de máquinas agrícolas.</p>
	    		<p><strong>APERFEIÇOAMENTO</strong><br>Marchezan busca permanente aperfeiçoamento em suas áreas de atuação. Participou do Fórum de Líderes de Governo das Américas, em Miami, nos Estados Unidos, promovido pela Microsoft para debater e compartilhar experiências com representantes dos poderes públicos e líderes educacionais. Visitou as empresas líderes mundiais Google, Cisco e Telsinc, na Califórnia (Estados Unidos), com o objetivo de conhecer softwares para as áreas da saúde, educação e segurança.<br><br>Cursou o Programa de Visita e Diálogo para Deputados, promovido pelo Parlamento alemão, com o objetivo de promover diálogo político entre deputados do Brasil e da Alemanha e de conhecer o sistema legislativo alemão. Também participou do Programa de Liderança Executiva em Desenvolvimento da Primeira Infância, na Universidade de Harvard, nos Estados Unidos. Esse programa resultou no desenvolvimento de projeto para o fortalecimento de políticas públicas na primeira infância.<br><br>E, recentemente, representou a Câmara dos Deputados em missão oficial a Portugal, onde realizou visitas técnicas e encontros com especialistas para conhecer o processo de reciclagem de resíduos sólidos realizado na Comunidade Europeia.</p>
	    		<p><strong>ATUAÇÃO RECONHECIDA</strong><br>Por sua atuação destacada, Marchezan recebeu o reconhecimento da sociedade. Foi agraciado com o Prêmio Springer Carrier – ARI, da Associação Riograndense de Imprensa, na área de economia, e o Prêmio Mérito Lojista, categoria Personalidade Política, da Federação das Câmaras de Dirigentes Lojistas do Rio Grande do Sul (FCDL-RS).<br><br>Recebeu o Prêmio Líderes & Vencedores, promovido pela Federasul e Assembleia Legislativa, na categoria mérito político. Também foi homenageado na categoria personalidade política pela Associação das Empresas Brasileiras de Tecnologia da Informação e Internet , Regional Rio Grande do Sul (Assespro-RS).</p>
	    	</div>
	    	<div class="col-lg-4 col-md-4 hidden-sm hidden-xs sidebar text-right">
	    		<a href="" id="fechar-biografia">
	    			Fechar biografia
	    		</a>
				<a href="#projetos" class="goto" title="Projetos">
					<img src="<?php echo getImg('projetos-ico.jpg') ?>" alt="Projetos">
				</a>
				<!-- <a href="#propostas" class="goto" title="Propostas">
					<img src="<?php echo getImg('propostas-ico.jpg') ?>" alt="Propostas">
				</a>
				<a href="#novidades" class="goto" title="Novidades">
					<img src="<?php echo getImg('novidades-ico.jpg') ?>" alt="Novidades">
				</a> -->
			</div>
	    </div>
	</div>
</section>