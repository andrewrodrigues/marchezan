<section class="biografia exibe-biografia">
	<div class="container">
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-right text-left-sm text-left-xs">
			<h2>Nelson Marchezan Jr.</h2>
			<p>Nelson Marchezan Júnior tem 40 anos, é natural de Porto Alegre e filho do falecido ex-deputado federal Nelson Marchezan e da professora Maria Helena Bolsson Marchezan. É pai de Nelson Marchezan Neto, com Nadine Dubal. Formado em Direito, exerceu a profissão por oito anos antes de ingressar na vida pública. Atualmente, cursa pós-graduação em Gestão Empresarial na Fundação Getúlio Vargas.</p>
			<a title="Leia a biografia completa" href="<?php echo getLink('bibliografia') ?>" class="esconde-section">Leia a <span class="azul">biografia</span> completa <img src="<?php echo getImg('ver-mais-ico.png') ?>" alt="Leia a biografia completa"></a>
		</div>
	</div>
</section>
<section class="conteudos-dinamicos bib">
	<div class="container">
		<div class="biografia">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
	    		<h2 class="padrao">Biografia</h2>
	    		<hr>
	    		<figure>
	    			<img src="<?php echo getImg('marchezan-biografia.jpg') ?>" alt="Deputador Nelson Marchezan Jr." class="fullsize-image img-responsive">
	    			<figcaption>
	    				Marchezan durante discurso na Assembléia Legislativa do RS - 2006
	    			</figcaption>
	    		</figure>
	    		<h3>Sobre Nelson Marchezan Júnior</h3>
	    		<p>Nelson Marchezan Júnior tem 40 anos, é natural de Porto Alegre e filho do falecido ex-deputado federal Nelson Marchezan e da professora Maria Helena Bolsson Marchezan. É pai de Nelson Marchezan Neto, com Nadine Dubal. Formado em Direito, exerceu a profissão por oito anos antes de ingressar na vida pública. Atualmente, cursa pós-graduação em Gestão Empresarial na Fundação Getúlio Vargas.<br />
				Em 2003, Marchezan assumiu o cargo de diretor de Desenvolvimento, Agronegócios e Governos do Banrisul. Em 2006, elegeu-se deputado estadual com 45.604 votos, o mais votado da bancada do PSDB na Assembleia Legislativa. Foi eleito para quatro anos como presidente da Comissão de Finanças, Planejamento, Fiscalização e Controle. É também titular da Comissão de Constituição e Justiça da AL.<br />
				Nas eleições de 2010, conquistou nova vitória nas urnas, elegendo-se deputado federal com 92.394 votos.</p>
				<h3>Atuação parlamentar</h3>
				<p>Marchezan é o autor do projeto, já transformado em lei, que vedou qualquer compra e contratação por dispensa de licitação em todos os poderes do Estado. É a cotação eletrônica. Agora, todos os preços são cotados pela internet. Por outro projeto, também já transformado em lei, todas as compras e contratações de qualquer órgão público estadual se darão por um leilão ao contrário, na internet, onde vence quem oferecer o menor preço. É o pregão eletrônico. São as duas ferramentas mais modernas para o combate à corrupção, economia, transparência e agilidade no setor público.<br />
				Em 2010, Marchezan conquistou avanços importantes na transparência em relação aos gastos públicos. Graças ao seu projeto, já transformado em lei, o setor público terá que publicar a cada bimestre um quantitativo dos cargos públicos, informando quantos são e quanto custa cada servidor público. Trata-se da lei da publicação da remuneração de todos os servidores públicos estaduais na internet. A finalidade é dar ao cidadão oportunidade de saber quanto paga para os seus servidores.<br />
				Marchezan continua defendendo a aplicação do teto salarial, determinado pela Constituição em todos os poderes, por justiça salarial e respeito ao contribuinte, que vai gerar uma economia superior a R$ 15 milhões.<br />
				Marchezan também é o autor da Lei de Incentivo à Saúde, que está tramitando na AL, e que vai permitir às empresas destinar parte de seus impostos para projetos na área da saúde em seu município ou região. O caminho do recurso é mais curto, com menos chances de desvio. A empresa financia a saúde de seu município e o Conselho Municipal de Saúde aprova e fiscaliza o projeto de interesse da comunidade.<br />
				Marchezan obteve outra vitória importante em setembro de 2010, quando conseguiu que os deputados aprovassem seu projeto pedindo a instalação da Comissão Permanente de Segurança Pública na AL. Depois de um ano de articulações junto a entidades da segurança pública, Marchezan conquistou o apoio necessário para a aprovação do projeto. Agora, a Assembleia vai discutir de forma técnica e regimental, todas as semanas, este assunto que é tão importante para os gaúchos. Esse foi mais um compromisso de campanha cumprido por Marchezan. Conforme o deputado, “não era mais possível que o Parlamento gaúcho se abstesse de debater este tema”.<br />
				Por sua atuação destacada no Parlamento, Marchezan recebeu o reconhecimento da sociedade. Em 2009, foi agraciado com o Prêmio Springer Carrier – ARI, da Associação Riograndense de Imprensa, na área de economia e o Prêmio Mérito Lojista, categoria Personalidade Política, da Federação das Câmaras de Dirigentes Lojistas do Rio Grande do Sul – FCDL/RS. Este ano, recebeu o Prêmio Líderes & Vencedores 2010, promovido pela Federasul e Assembleia Legislativa, na categoria mérito político. Também foi homenageado na categoria personalidade política pela Associação das Empresas Brasileiras de Tecnologia da Informação, Software e Internet, Regional Rio Grande do Sul (Assespro-RS), em comemoração aos 30 anos da entidade.</p>
				<h3>Comissões que Marchezan participa:</h3>
				<p><strong>COMISSÕES PERMANENTES:</strong><br />
				Comissão de Ciência e Tecnologia, Comunicação e Informática (CCTCI), titular, 06/03/13 – 31/01/14;<br />
				Comissão de Defesa do Consumidor (CDC),suplente, 06/03/13 – 31/01/2014; Comissão de Constituição e Justiça e de Cidadania (CCJC), suplente, 06/03/13 – 31/01/2014;<br />
				Comissão de Finanças e Tributação (CFT),suplente, 06/03/13 – 31/01/2014;<br />
				<strong>COMISSÕES EXTERNAS E ESPECIAIS:</strong><br />
				CEXSANTA, titular, destinada a acompanhar a apuração dos fatos relacionados à tragédia que vitimou centenas de jovens em um incêndio no município de Santa Maria, Rio Grande do SUL, e oferecer sugestão de aperfeiçoamento da legislação sobre o tema ,29/01/2013-;<br />
				CPICRIAN, titular, Comissão Parlamentar de Inquérito destinada a apurar denúncias de turismo sexual e exploração sexual de crianças e adolescentes, conforme diversas matérias publicadas pela imprensa, 09/02/2012-;<br />
				CEDOCSIG, titular,Comissão Especial destinada a apreciar as solicitações de acesso a informações sigilosas produzidas ou recebidas pela Câmara dos Deputados no exercício de suas funções parlamentares e administrativas, assim como sobre o cancelamento ou redução de prazos de sigilo e outras atribuições, 30/03/2011-;<br />
				PL803510, titular,Comissão Especial destinada a proferir parecer ao Projeto de Lei nº 8035, de 2010, do Poder Executivo, que “aprova o Plano Nacional de Educação para o decênio 2011-2020 e dá outras providências”, 22/03/2011-;<br />
				CPITRAPE, suplente, Comissão Parlamentar de Inquérito destinada a investigar o tráfico de pessoas no Brasil, suas causas, conseqüências e responsáveis no período de 2003 a 2011, compreendido na vigência da Convenção de Palermo, 09/02/2012-;<br />
				SUBSETEL, suplente, Subcomissão Especial para Acompanhamento, Fiscalização e Controle dos Serviços de Telecomunicações, 08/08/2012;<br />
				PL157211, suplente, Comissão Especial destinada a proferir parecer ao Projeto de Lei nº 1572, de 2011, do Sr. Vicente Cândido, que “institui o Código Comercial”, 23/03/2012-;<br />
				PL742006, suplente, comissão Especial destinada a proferir parecer ao Projeto de Lei nº 7420, de 2006, da Sra. Professora Rachel Teixeira, que “Dispõe sobre a qualidade da educação básica e a responsabilidade dos gestores públicos na sua promoção,20/10/2011-;<br />
				PL602505, suplente, Comissão Especial destinada a proferir parecer ao Projeto de Lei nº 6025, de 2015, ao Projeto de Lei nº 8046, de 2010, ambos do Senado Federal, e outros, que tratem do “Código de Processo Civil” (revogam a Lei nº5.869, de 1973), 22/12/2010.</p>
	    	</div>
	    	<div class="col-lg-4 col-md-4 hidden-sm hidden-xs sidebar text-right">
	    		<a href="" id="fechar-biografia">
	    			Fechar biografia
	    		</a>
				<a href="#projetos" class="goto" title="Projetos">
					<img src="<?php echo getImg('projetos-ico.jpg') ?>" alt="Projetos">
				</a>
				<a href="#propostas" class="goto" title="Propostas">
					<img src="<?php echo getImg('propostas-ico.jpg') ?>" alt="Propostas">
				</a>
				<a href="#novidades" class="goto" title="Novidades">
					<img src="<?php echo getImg('novidades-ico.jpg') ?>" alt="Novidades">
				</a>
			</div>
	    </div>
	</div>
</section>