<section class="doacoes">
			<div class="container">
				<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
					<h2 class="padrao">Faça a sua doação</h2>
					<p>A eleição está na reta final e sua participação é cada vez mais importante.<br>
					Faça sua doação para campanha Marchezan 4545 - Deputado Federal.</p>
					<p><strong>Dados para depósito</strong><br>
					Eleição 2014 – Nelson Marchezan Júnior Deputado Federal<br>
					CNPJ: 20.567.696/0001-40<br>
					Endereço: Rua Botafogo, 237<br>
					Bairro Praia de Belas – POA/RS<br>
					CEP: 90.110-000<br>
					Fone: (51) 9914-5451<br>
					Conta Corrente:<br>
					Banco do Estado do Rio Grande do Sul – Banrisul<br>
					Banco: 041<br>
					Agência: 0835<br>
					Conta Corrente: 06.202.756.0-5<br>
					Contamos com seu apoio.</p>
					<p style="font-size:10px"><strong>TERMOS DE DOAÇÃO</strong></p>
					<p style="font-size:10px">A lei brasileira regula as doações para as campanhas eleitorais. A fim de evitar problemas para quem quiser colaborar com a campanha, seguem abaixo dicas e regras. Caso surjam novas dúvidas, não deixe de entrar em contato conosco via e-mail: contato@marchezan.com.br</p>
					<p style="font-size:10px">O CPF do doador deve estar regular. Caso haja alguma pendência na Receita Federal (como declaração de Imposto de Renda atrasada), ela deve ser solucionada antes de feita a doação.</p>
					<p style="font-size:10px">As doações são limitadas a 10% (dez por cento) do rendimento declarado pelo doador no Imposto de Renda relativo ao ano de 2013. Esse limite não se refere a cada doação, mas à soma de todas as doações feitas por uma única pessoa nas eleições.</p>
					<p style="font-size:10px">Se pessoa jurídica, as doações são limitadas a 2% (dois por cento) do rendimento declarado pelo doador no Imposto de Renda relativo ao ano de 2013.</p>
					<p style="font-size:10px">Na sua próxima declaração de Imposto de Renda, não se esqueça de informar sua doação. As doações deverão ser apresentadas em campo próprio do programa que a Receita Federal distribuirá para a declaração do IR relativa ao ano anterior. O partido declarará todas as doações recebidas. É imprescindível, assim, que os doadores também declarem suas doações para evitar conflitos no cruzamento de dados</p>
					<!-- <div class="row">
						<div class="col-lg-6 hidden-md hidden-sm hidden-xs">
							<img src="<?php echo getImg('pagseguro-logo.jpg') ?>" alt="PagSeguro">
						</div>
						<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
							<a href="#" title="Faça sua doação e ajude a campanha">
								<div class="texto">Faça sua <span class="azul">doação</span> e ajude na <span class="azul">campanha</span></div>
								<img src="<?php echo getImg('ver-mais-ico.png') ?>" alt="Faça sua doação e ajude na campanha">
							</a>
						</div>
					</div> -->
				</div>
			</div>
			<a class="novidades-scroll"></a>
		</section>