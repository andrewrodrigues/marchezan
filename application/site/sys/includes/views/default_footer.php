<footer>
	<section class="contato">
		<div class="container">
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<a name="contato"></a>
				<h2 class="padrao">Contato</h2>
				<form action="<?php echo getLink('contato') ?>" method="post" id="contato">
					<input type="text" name="nome" placeholder="Nome">
					<input type="text" name="email" placeholder="E-mail">
					<input type="text" name="telefone" placeholder="Telefone" id="telefone">
					<textarea name="mensagem" placeholder="mensagem"></textarea>
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<p class="errors"></p>
						</div>
					</div>
					<input type="submit" value="">
				</form>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h3>Brasília</h3>
					<p>Praça dos Três Poderes - Câmara dos Deputados</p>
					<p>Gabinete: 250 - Anexo: IV</p>
					<p>CEP: 70160-900 - Brasília - DF</p>
					<p>Telefone: (61) 3215-5250</p>
					<p>Fax: (61) 3215-2250</p>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h3>Porto Alegre</h3>
					<p>Rua General Andrade Neves, 14 – 10º andar</p>
					<p>CEP: 90010-210 – Centro – Porto Alegre – RS</p>
					<p>Telefones:</p>
					<p>(51) 3330-4545 / (51) 9505-4545</p>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h3>E-mails:</h3>
					<p><a href="mailto:contato@marchezan.com.br" title="Envie um e-mail para contato@marchezan.com.br">contato@marchezan.com.br</a></p>
					<p><a href="mailto:agenda@marchezan.com.br" title="Envie um e-mail para agenda@marchezan.com.br">agenda@marchezan.com.br</a></p>
					<p><a href="mailto:imprensa@marchezan.com.br" title="Envie um e-mail para imprensa@marchezan.com.br">imprensa@marchezan.com.br</a></p>
				</div>
			</div>
		</div>
	</section>
	<section class="newsletter">
		<div class="container">
			<div class="col-lg-6 col-md-4 hidden-sm hidden-xs text-right pull-right">
				<img src="<?php echo getImg('psdb-logo.png') ?>" alt="PSDB - Social Democracia">
			</div>
			<div class="hidden-lg hidden-md col-sm-12 col-xs-12 text-center">
				<img src="<?php echo getImg('psdb-logo.png') ?>" alt="PSDB - Social Democracia">
			</div>
		</div>
	</section>
</footer>
<?php if(isset($js)){ loadJs($js); } ?>
</body>

</html>
<script>
  var stage = new swiffy.Stage(document.getElementById('swiffycontainer'),
      swiffyobject, {  });
  
  stage.start();
</script>