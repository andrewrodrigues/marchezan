<section class="comites">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin-bottom:30px">
			<h2 class="padrao">Comitês</h2>
			<h3 class="padrao">Encontre um comitê mais próximo de você.</h3>
		</div>
		<div class="claerfix"></div>
		<div class="col-lg-offset-2 col-lg-10 col-md-12 col-sm-12 col-xs-12">
				<div class="lista-comites">
					<div class="info-comite">
						<p><strong>Porto Alegre</strong></p>
						<p class="info">Avenida Botafogo, 237</p>
						<p class="info">Bairro Menino Deus</p>
						<p class="info">Diariamente das 8hs às 21hs</p>
						<p class="info">Fone: 51 3392.0002</p>
					</div>
					<div class="info-comite">
						<p><strong>Canoas</strong></p>
						<p class="info">Avenida Getúlio Vargas, 5191</p>
						<p class="info">Diariamente das 8hs às 21hs</p>
						<p class="info">Fone: 51 9898.1617</p>
					</div>
					<div class="info-comite">
						<p><strong>Canoas 2</strong></p>
						<p class="info">Rua Júlio de Castilho, 459</p>
						<p class="info">Bairro Niteroi</p>
						<p class="info">Diariamente das 9hs às 19hs</p>
					</div>
					<div class="info-comite">
						<p><strong>Canoas 3</strong></p>
						<p class="info">Rua Bage, 80</p>
						<p class="info">Bairro Estação Niteroi</p>
						<p class="info">Diariamente das 9hs às 19hs</p>
					</div>
					<div class="info-comite">
						<p><strong>Santa Maria</strong></p>
						<p class="info">Avenida Presidente Vargas, 1569</p>
						<p class="info">Bairro Centro</p>
						<p class="info">Diariamente das 8:30 às 19hs</p>
						<p class="info">Fone: 55 9971.2572</p>
					</div>
					<div class="info-comite">
						<p><strong>Viamão</strong></p>
						<p class="info">Avenida Liberdade em frente Praça Sta Isabel</p>
						<p class="info">Diariamente das 9h às 19hs</p>
					</div>
					<div class="info-comite">
						<p><strong>Carazinho</strong></p>
						<p class="info">Contato vereador Orion Albuquerque -54.3330.2322</p>
						<p class="info">Avenida Pátria, 49</p>
						<p class="info">Centro</p>
						<p class="info">Das 13h30 às 18h</p>
					</div>
					<div class="info-comite">
						<p><strong>Dom Pedrito</strong></p>
						<p class="info">Edilaine</p>
						<p class="info">Fone: 53.9123.4517</p>
					</div>
					<div class="info-comite">
						<p><strong>Canguçu</strong></p>
						<p class="info">Eber Hoffmann:</p>
						<p class="info">Comercial - (53) 3252-1528</p>
						<p class="info">Celular - (53) 9122-4552</p>
						<p class="info">Comercial - (51) 3210-1445</p>
						<p class="info">Celular - (51) 9971-3749</p>
						<p class="info">eber.hoffmann2@hotmail.com</p>
					</div>
					<div class="info-comite">
						<p><strong>São Lourenço do Sul</strong></p>
						<p class="info">Contato é Emílio Lessa - 50.99</p>
						<p class="info">Rua Marechal Floriano, 1677</p>
						<p class="info">8h30min às 18h30min<br>(fecha das 12h às 13h30min)</p>
					</div>
					<div class="info-comite">
						<p><strong>Santa Maria</strong></p>
						<p class="info">Presidente Vargas, 1569</p>
						<p class="info">Centro</p>
						<p class="info">Das 8h30 às 19h</p>
					</div>
				</div>
		</div>
	</div>
</section>