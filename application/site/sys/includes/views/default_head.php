<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php if(isset($title)){ echo $title; } ?></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <base href="<?php echo base_url(); ?>">

  <!-- Tags OG para compartilhamento em redes sociais -->

  <meta property="og:locale" content="pt_BR">
  <meta property="og:url" content="http://www.meusite.com.br/ola-mundo">
  <meta property="og:title" content="Título da página ou artigo">
  <meta property="og:site_name" content="Nome do meu site">
  <meta property="og:description" content="Minha boa descrição para intrigar os usuários.">
  <meta property="og:image" content="www.meusite.com.br/imagem.jpg">
  <meta property="og:image:type" content="image/jpeg">
  <meta property="og:type" content="website">


  <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

  <?php if(isset($css)){ loadCss($css); } ?>

  <?php if(isset($js)){ loadJs($js); } ?>

  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-58233367-1', 'auto');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

  </script>

  <script>
  var baseURL = window.location.origin;
  </script>

  <link rel="shortcut icon" href="<?php echo getImg('favicon.ico'); ?>" />
  <link href='http://fonts.googleapis.com/css?family=Oswald:400,300' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
  <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
  <script src="https://www.gstatic.com/swiffy/v7.0/runtime.js"></script>
  <script src="<?php echo getJs('banner') ?>"></script>

  <!--[if lt IE 9]>
  <script src="//raw.github.com/mylovecompany/ie9-js/master/ie9.min.js">IE7_PNG_SUFFIX=".png";</script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.min.js"></script>
  <![endif]-->
</head>