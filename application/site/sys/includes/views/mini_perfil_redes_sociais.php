<section class="perfil-redes-sociais">
	<div class="container">
		<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 perfil">
			<a name="quem-sou"></a>
			<h2>Perfil</h2>
			<p>Nelson Marchezan Júnior, é natural de Porto Alegre e filho do ex-deputado federal Nelson Marchezan e da professora Maria Helena Bolsson Marchezan. É pai de Nelson Marchezan Neto, de seis anos.<br><br>
Formado em Direito pela Unisinos e pós-graduado em Gestão Empresarial na Fundação Getúlio Vargas, exerceu a advocacia por oito anos antes de ingressar na vida pública...</p>
			<div class="bg-btn-perfil">
				<a title="Clique e leia a bibliografia completa" href="<?php echo getLink('biografia') ?>" class="abrir-biografia"> <img src="<?php echo getImg('btn-perfil.png') ?>" class="img-responsive" alt=""></a>
			</div>
		</div>
		<div class="col-lg-9 col-md-4 col-sm-12 col-xs-12 redes-sociais">
			<h2>Me acompanhe nas redes sociais</h2>
			<div class="row">
				<div class="hidden-lg hidden-md hidden-sm col-xs-6 text-center">
					<a href="https://twitter.com/marchezan_" target="_blank">
						<img src="<?php echo getImg('twitter-mobile.png') ?>" alt="Clique aqui para acessar e seguir no Twitter"><br>
						Clique aqui para acessar e seguir no Twitter
					</a>
				</div>
				<div class="hidden-lg hidden-md hidden-sm col-xs-6 text-center">
					<a href="https://www.facebook.com/nelsonmarchezan" target="_blank">
						<img src="<?php echo getImg('facebook-mobile.png') ?>" alt="Clique aqui para acessar e curtir a página!"><br>
						Clique aqui para acessar e curtir a página!
					</a>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 hidden-xs twitter">
					<a href="https://twitter.com/marchezan_" target="_blank" class="compartilhar">Clique aqui para acessar e seguir no Twitter</a>
					<a class="twitter-timeline"
					  href="https://twitter.com/marchezan_"
					  data-widget-id="522393631127117824"
					  data-screen-name="marchezan_"
					  data-show-replies="false"
					  data-tweet-limit="3"
					  width="362">
					Tweets by @marchezan_
					</a>
				</div>
				<div class="col-lg-5 col-md-5 col-sm-12 hidden-xs facebook">
					<a href="https://www.facebook.com/nelsonmarchezan" target="_blank" class="compartilhar">Clique aqui para acessar e curtir a página!</a>
					<div id="fb-root"></div>
					<script>(function(d, s, id) {
					  var js, fjs = d.getElementsByTagName(s)[0];
					  if (d.getElementById(id)) return;
					  js = d.createElement(s); js.id = id;
					  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.0";
					  fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));</script>
					<div class="fb-like-box" data-href="https://www.facebook.com/nelsonmarchezan" data-width="390" data-height="340" data-colorscheme="light" data-show-faces="false" data-header="false" data-stream="true" data-show-border="false"></div>
				</div>
			</div>
		</div>
	</div>
</section>