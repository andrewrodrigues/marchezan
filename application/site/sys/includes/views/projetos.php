<a href="#trabalho-educacao" class="trabalho-educacao"></a>
<a name="trabalho-educacao"></a>
<section class="educacao">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h2>Educação</h2>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="projeto azul-escuro text-center unico no-padding espacamento-mobile">
					<img src="<?php echo getImg('educacao-chamada.jpg') ?>" class="img img-responsive" alt="">
					<p class="quote text-chamada-azul-claro bg-azul">“Assegurar o desenvolvimento integral e completo das crianças é também garantir o desenvolvimento das novas gerações. Para atingir essa meta, é preciso valorizar e qualificar os professores, com remuneração justa e com o reconhecimento de quem trabalha pela melhoria da educação brasileira.”</p>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="projeto no-padding text-center abre-projeto margin-bottom relative espacamento-mobile" data-categoria="educacao" data-id="1">
							<img src="<?php echo getimg('educacao-1.jpg') ?>" class="img-responsive" alt="ATUAÇÃO DE DESTAQUE NO PLANO NACIONAL DE EDUCAÇÃO">
							<p class="quote maior text-chamada-azul-claro bg-azul-opacity">ATUAÇÃO DE DESTAQUE NO PLANO NACIONAL DE EDUCAÇÃO</p>
						</div>
						<div class="projeto no-padding text-center abre-projeto relative espacamento-mobile" data-categoria="educacao" data-id="2">
							<img src="<?php echo getimg('educacao-2.jpg') ?>" style="width: 100%; min-width: 100%;" alt="PISO NACIONAL DO MAGISTÉRIO PARA TODOS">
							<p class="quote maior text-chamada-azul-claro bg-azul-opacity">PISO NACIONAL DO MAGISTÉRIO PARA TODOS</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="projeto azul-claro text-center unico abre-projeto no-padding espacamento-mobile" data-categoria="educacao" data-id="3">
					<img src="<?php echo getimg('educacao-3.jpg') ?>" style="width: 100%; min-width: 100%;" alt="MERITOCRACIA DEVE NORTEAR O ENSINO PÚBLICO">
					<h3 class="bg-azul">Meritocracia deve nortear o ensino público</h3>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="projeto azul-claro text-center no-padding unico abre-projeto espacamento-mobile" data-categoria="educacao" data-id="4">
							<h3 class="bg-azul">Marco legal da primeira infância</h3>
							<img src="<?php echo getimg('educacao-4.jpg') ?>" alt="Marco legal da primeira infância" class="img-responsive">
						</div>
					</div>
				</div>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="educacao-1">
				<a href="#" class="fechar-projeto pull-right" id="fechar-educacao-1">Fechar Projeto</a>
				<h3 class="sem-linha">Atuação de destaque no Plano Nacional de Educação</h3>
				<img src="<?php echo getImg('educacao-1.jpg') ?>" alt="Atuação de destaque no Plano Nacional de Educação" align="left" class="img-responsive">
				<p>Vice-presidente da Comissão Especial do Plano Nacional de Educação (PNE), que estabelece metas a serem atingidas pelo Brasil nos próximos anos, Marchezan apresentou pelo menos 40 emendas. Entre os destaques, estão:</p>
				<p>- universalização do atendimento escolar da população de 4 e 5 anos, até 2016, e ampliação da oferta de Educação Infantil em creches de forma a atender, no mínimo, 50% das crianças até 3 anos</p>
				<p>- universalização do Ensino Fundamental de 9 anos para a população entre 6 e 14 anos e garantir que pelo menos 95% dos alunos concluam essa etapa na idade recomendada</p>
				<p>- implantação de uma política nacional de formação e valorização dos professores e demais profissionais da educação</p>
				<p>- garantir plano de carreira aos professores e aos demais profissionais da educação básica pública, com salários iniciais atrativos, promoção por mérito e remuneração variável por resultados educacionais</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="educacao-2">
				<a href="#" class="fechar-projeto pull-right" id="fechar-educacao-2">Fechar Projeto</a>
				<h3 class="sem-linha">Piso Nacional do Magistério para todos</h3>
				<img src="<?php echo getImg('educacao-2.jpg') ?>" alt="Piso Nacional do Magistério para todos" align="left" class="img-responsive">
				<p>Marchezan defende o piso nacional do magistério por meio do Projeto de Lei 3.020/2011, de sua autoria. Estados e municípios não beneficiados pelo Fundo de Manutenção e Desenvolvimento da Educação Básica e de Valorização dos Profissionais da Educação (Fundeb) poderão receber complementação da União para o pagamento do piso nacional. A proposta de Marchezan prevê que isso ocorra nos casos em que o município comprovar não ter condições financeiras para cumprir o pagamento do piso e leva em conta aspectos como a relação médica do número de alunos por professor, diferenciando-a para zona urbana e rural. O repasse ocorrerá durante a vigência do Fundeb e só poderá ser usado para essa finalidade.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="educacao-3">
				<a href="#" class="fechar-projeto pull-right" id="fechar-educacao-3">Fechar Projeto</a>
				<h3 class="sem-linha">Meritocracia deve nortear o ensino público</h3>
				<img src="<?php echo getImg('educacao-3.jpg') ?>" alt="Meritocracia deve nortear o ensino público" align="left" class="img-responsive">
				<p>Relator na Comissão de Constituição e Justiça, Marchezan defende a PEC 82/2011, que prevê o reconhecimento do esforço dos professores para criar um ambiente de estímulo à produtividade. Segundo a proposta, a noção do mérito é requisito básico para o ensino público de qualidade, a permanência dos servidores e o aprimoramento contínuo das pessoas que têm em suas mãos o futuro dos estudantes.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="educacao-4">
				<a href="#" class="fechar-projeto pull-right" id="fechar-educacao-4">Fechar Projeto</a>
				<h3 class="sem-linha">Marco legal da primeira infância</h3>
				<img src="<?php echo getImg('educacao-4.jpg') ?>" alt="Marco legal da primeira infância" align="left" class="img-responsive">
				<p>Para garantir a estruturação do setor público com ações que assegurem o desenvolvimento completo nos primeiros anos de vida de uma criança, Marchezan foi o subescritor do Marco Legal na Primeira Infância e do aperfeiçoamento do Estatuto da Criança e do Adolescente (ECA), que asseguram, por exemplo, a criação e a ampliação de espaços que atendam ao setor e o acesso a serviços, como creches ou atendimento da família por agentes públicos.</p>
			</div>
		</div>
	</div>
</section>

<a href="#trabalho-saude" class="trabalho-saude"></a>
<a name="trabalho-saude"></a>
<section class="saude">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h2>Saúde</h2>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="projeto verde text-center unico no-padding espacamento-mobile">
					<img src="<?php echo getImg('saude-chamada.jpg') ?>" alt="Saúde" class="img-responsive">
					<p class="quote text-chamada-verde bg-verde" style="font-size: 15px !important;">“Marchezan apresentou projetos que têm por finalidade melhorar os serviços e ampliar a aplicação de recursos para a saúde no país, tornando a distribuição mais justa e aumentando o acesso da população ao atendimento.”</p>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="projeto verde text-center duo-img abre-projeto no-padding relative margin-bottom espacamento-mobile" data-categoria="saude" data-id="1">
							<img src="<?php echo getImg('saude-1.jpg') ?>" alt="Melhores Salários para Agentes Comunitários" class="img-responsive">
							<p class="quote maior text-chamada-verde bg-verde-opacity">Mais Recursos Federais para a Área</p>
						</div>
						<div class="projeto verde text-center duo-img abre-projeto no-padding relative espacamento-mobile" data-categoria="saude" data-id="2">
							<img src="<?php echo getImg('saude-2.jpg') ?>" alt="Lei de Incentivo à Saúde" class="img-responsive">
							<p class="quote maior text-chamada-verde bg-verde-opacity">Lei de Incentivo à Saúde</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="projeto verde text-center unico abre-projeto no-padding espacamento-mobile" data-categoria="saude" data-id="3">
					<img src="<?php echo getImg('saude-3.jpg') ?>" alt="Melhores Salários para Agentes Comunitários" class="img-responsive">
					<h3 class="titulo-projeto-grande bg-verde box" style="font-size: 24px;line-height: 180%;">Melhores Salários Para Agentes Comunitários</h3>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="projeto verde text-center no-padding unico abre-projeto espacamento-mobile" data-categoria="saude" data-id="4">
							<h3 class="titulo-projeto-grande bg-verde">Por Melhores e Novos Investimentos</h3>
							<img src="<?php echo getImg('saude-4.jpg') ?>" alt="Por Melhores e Novos Investimentos" class="img-responsive">
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="projeto verde text-center unico abre-projeto no-padding margin-top espacamento-mobile" data-categoria="saude" data-id="5">
					<img src="<?php echo getImg('saude-5.jpg') ?>" alt="Por Melhores e Novos Investimentos" class="img-responsive">
					<h3 class="titulo-projeto-grande bg-verde box" style="font-size: 34px;line-height: 140%;">Mais Saúde na Cesta Básica</h3>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="projeto verde text-center unico abre-projeto espacamento-mobile" data-categoria="saude" data-id="6">
					<h3 class="titulo-projeto-grande bg-verde margin-top">SUS Para Quem Realmente Precisa</h3>
					<img src="<?php echo getImg('saude-6.jpg') ?>" alt="SUS Para Quem Realmente Precisa" class="img-responsive">
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="projeto verde text-center duo-img abre-projeto no-padding margin-top margin-bottom relative espacamento-mobile" data-categoria="saude" data-id="7">
							<img src="<?php echo getImg('saude-7.jpg') ?>" alt="Informações Sobre Lactose nas Embalagens" class="img-responsive">
							<div class="transparent">
								<p class="quote maior text-chamada-verde bg-verde-opacity">Informações Sobre Lactose nas Embalagens</p>
							</div>
						</div>
						<div class="projeto verde text-center duo-img abre-projeto no-padding relative espacamento-mobile" data-categoria="saude" data-id="8">
							<img src="<?php echo getImg('saude-8.jpg') ?>" alt="Primeira Infância Fez Parte de Estudos" class="img-responsive">
							<p class="quote maior text-chamada-verde bg-verde-opacity">Primeira Infância Fez Parte de Estudos</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="projeto verde text-center unico abre-projeto margin-top" data-categoria="saude" data-id="9">
					<h3 class="titulo-projeto-grande bg-verde box" style="font-size: 24px;line-height: 150%;">Andadores Infantis Trazem Riscos à Saúde das Crianças</h3>
					<img src="<?php echo getImg('saude-9.jpg') ?>" alt="Andadores Infantis Trazem Riscos à Saúde das Crianças" class="img-responsive">
				</div>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="saude-1">
				<a href="#" class="fechar-projeto pull-right" id="fechar-saude-1">Fechar Projeto</a>
				<h3 class="sem-linha">Mais recursos federais para a área</h3>
				<img src="<?php echo getImg('saude-1.jpg') ?>" alt="Mais recursos federais para a área" align="left" class="img-responsive">
				<p>A Lei de Incentivo à Saúde busca enfrentar o baixo investimento do governo federal na saúde pública. Como alternativa para esse problema, as empresas poderão investir na qualificação da saúde, através de Projeto de Lei de autoria de Marchezan. A proposta é semelhante à Lei de Incentivo à Cultura, e prevê a dedução da Contribuição para Financiamento da Seguridade Social (Cofins) devida. Será possível descontar integralmente, em até 12 meses, o valor despendido em apoio a projetos na área da saúde, até o máximo de 6% do valor do tributo devido ao ano. </p>
				<p>Ainda quando deputado estadual, Marchezan apresentou proposta semelhante na Assembleia gaúcha. O Projeto de Lei, chamado Lei de Incentivo à Saúde, possibilita às empresas transferirem recursos de impostos para projetos na área. Quem ganha são as pessoas, pois a distribuição da verba se dará de forma proporcional à população de cada cidade. Os deputados estaduais do PSDB gaúcho reapresentaram a proposta recentemente.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="saude-2">
				<a href="#" class="fechar-projeto pull-right" id="fechar-saude-2">Fechar Projeto</a>
				<h3 class="sem-linha">Mais recursos federais para a área</h3>
				<img src="<?php echo getImg('saude-2.jpg') ?>" alt="Mais recursos federais para a área" align="left" class="img-responsive">
				<p>O parlamentar elaborou um Projeto de Lei do parlamentar que prevê o investimento de 10% da receita corrente bruta da União em ações de saúde para qualificar o atendimento. Atualmente, os Estados devem investir 12% da arrecadação, e os municípios, 15%, enquanto a União repassa ao setor um valor corrigido anualmente pela variação nominal do Produto Interno Bruto (PIB).</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="saude-3">
				<a href="#" class="fechar-projeto pull-right" id="fechar-saude-3">Fechar Projeto</a>
				<h3 class="sem-linha">Melhores salários para agentes comunitários</h3>
				<img src="<?php echo getImg('saude-3.jpg') ?>" alt="Melhores salários para agentes comunitários" align="left" class="img-responsive">
				<p>Outra luta de Marchezan, os agentes comunitários de saúde e de combate a endemias passaram a ter, neste ano, piso nacional – no valor de R$ 1.014,00 – e plano de carreira. Antes, não existia piso. A União transferia aos agentes um valor semelhante ao piso aprovado. Muitas vezes, os municípios repassavam à categoria um salário mínimo e usavam a diferença para cobrir encargos trabalhistas. A lei foi sancionada pela presidente Dilma, que vetou os pontos que tratavam do reajuste do valor, da organização das carreiras e do incentivo da União para as áreas de atuação dos agentes de saúde.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="saude-4">
				<a href="#" class="fechar-projeto pull-right" id="fechar-saude-4">Fechar Projeto</a>
				<h3 class="sem-linha">Por melhorias e novos investimentos</h3>
				<img src="<?php echo getImg('saude-4.jpg') ?>" alt="Por melhorias e novos investimentos" align="left" class="img-responsive">
				<p>Defensor da qualidade da saúde e atento às demandas do setor hospitalar, Marchezan participa anualmente da Feira Hospitalar, em São Paulo. Dessa forma, ele também busca dar andamento na Câmara aos principais problemas expostos pelo segmento.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="saude-5">
				<a href="#" class="fechar-projeto pull-right" id="fechar-saude-5">Fechar Projeto</a>
				<h3 class="sem-linha">Mais saúde na cesta básica</h3>
				<img src="<?php echo getImg('saude-5.jpg') ?>" alt="Mais saúde na cesta básica" align="left" class="img-responsive">
				<p>Marchezan defende que óculos de sol, escova de dente, filtro e bloqueador solar tenham alíquota zero, tornando esses produtos mais acessíveis aos brasileiros. Essa medida  é importante porque ajuda a prevenir o câncer de pele – o tipo mais comum da doença no Brasil; aumentar a proteção contra os raios ultravioletas, protegendo a visão de lesões oculares; e a melhorar a saúde bucal.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="saude-6">
				<a href="#" class="fechar-projeto pull-right" id="fechar-saude-6">Fechar Projeto</a>
				<h3 class="sem-linha">SUS para quem realmente precisa</h3>
				<img src="<?php echo getImg('saude-6.jpg') ?>" alt="SUS para quem realmente precisa" align="left" class="img-responsive">
				<p>Marchezan defende que o SUS não pode sustentar os planos privados de saúde. O SUS é para quem precisa. Melhorar o atendimento no SUS exige uma série de medidas. Entre elas, está a que se o usuário de plano de saúde privado acessar o SUS, a operadora deve fazer o reembolso de valores.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="saude-7">
				<a href="#" class="fechar-projeto pull-right" id="fechar-saude-7">Fechar Projeto</a>
				<h3 class="sem-linha">Informação sobre lactose nas embalagens</h3>
				<img src="<?php echo getImg('saude-7.jpg') ?>" alt="Informação sobre lactose nas embalagens" align="left" class="img-responsive">
				<p>Tornar obrigatória a informação sobre a presença de lactose nas embalagens ou rótulos de alimentos, bebidas e medicamentos, melhorando a vida de milhões de brasileiros, foi também alvo de atenção do mandato de Marchezan.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="saude-8">
				<a href="#" class="fechar-projeto pull-right" id="fechar-saude-8">Fechar Projeto</a>
				<h3 class="sem-linha">Primeira Infância fez parte de estudos</h3>
				<img src="<?php echo getImg('saude-8.jpg') ?>" alt="Primeira Infância fez parte de estudos" align="left" class="img-responsive">
				<p>Marchezan participou, em março de 2012, do Programa de Liderança Executiva em Desenvolvimento da Primeira Infância (DPI), na Universidade de Harvard, nos Estados Unidos. Cada participante desenvolveu uma proposta de projeto de base científica para fortalecer as políticas ou programas de Primeira Infância em sua região. De acordo com representantes  do Fundo Nas Nações Unidas para a Infância (Unicef), os seis primeiros anos de vida são cruciais para o desenvolvimento pleno das crianças, no que diz respeito aos direitos à amamentação, à vacinação e à merenda escolar.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="saude-9">
				<a href="#" class="fechar-projeto pull-right" id="fechar-saude-9">Fechar Projeto</a>
				<h3 class="sem-linha">Andadores infantis trazem risco à saúde das crianças</h3>
				<img src="<?php echo getImg('saude-9.jpg') ?>" alt="Andadores infantis trazem risco à saúde das crianças" align="left" class="img-responsive">
				<p>Estudos mostram que o uso de andadores infantis atrasa o desenvolvimento das crianças. Além disso, representa risco de acidentes, especialmente com lesões na cabeça. Por isso, o deputado Marchezan apresentou proposta que proíbe a comercialização, a doação ou a distribuição de andador infantil.</p>
			</div>
		</div>
	</div>
</section>

<a href="#trabalho-menos-impostos" class="trabalho-menos-impostos"></a>
<a name="trabalho-menos-impostos"></a>
<section class="menos-impostos">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h2>Menos Impostos</h2>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="projeto amarelo text-center unico no-padding espacamento-mobile">
					<img src="<?php echo getImg('menos-impostos-chamada.jpg') ?>" alt="Menos Impostos" class="img-responsive">
					<p class="quote text-chamada-amarelo bg-amarelo relative">“Ciente de que a alta carga tributária paga pelos brasileiros não se converte em melhoria de serviços, Marchezan defende que é preciso aumentar a eficiência da gestão, elevar a competitividade do país e cortar gastos públicos excessivos para que o cidadão veja os resultados dos valores pagos em impostos.”</p>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="projeto amarelo text-center duo-img duo-img-2 abre-projeto no-padding margin-bottom relative espacamento-mobile" data-categoria="menos-impostos" data-id="1">
							<img src="<?php echo getImg('menos-impostos-1.jpg') ?>" alt="MICRO E PEQUENAS EMPRESAS GANHAM COM A UNIVERSALIZAÇÃO DO SIMPLES" class="img-responsive">
							<p class="quote text-chamada-verde bg-amarelo-opacity">MICRO E PEQUENAS EMPRESAS GANHAM COM A UNIVERSALIZAÇÃO DO SIMPLES</p>
						</div>
						<div class="projeto amarelo text-center duo-img duo-img-2 abre-projeto no-padding margin-bottom relative espacamento-mobile" data-categoria="menos-impostos" data-id="2">
							<img src="<?php echo getImg('menos-impostos-2.jpg') ?>" alt="REFIS: OPORTUNIDADE PARA QUITAR DÍVIDA" class="img-responsive">
							<p class="quote text-chamada-verde bg-amarelo-opacity">REFIS:<br>OPORTUNIDADE PARA QUITAR DÍVIDA</p>
						</div>
						<div class="projeto amarelo text-center duo-img duo-img-2 abre-projeto no-padding relative espacamento-mobile" data-categoria="menos-impostos" data-id="3">
							<img src="<?php echo getImg('menos-impostos-3.jpg') ?>" alt="FIM ADICIONAL DE MULTA RESCISÓRIA" class="img-responsive">
							<p class="quote text-chamada-verde bg-amarelo-opacity">FIM ADICIONAL DE MULTA RESCISÓRIA</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="projeto amarelo text-center duo-img abre-projeto no-padding margin-bottom relative espacamento-mobile" data-categoria="menos-impostos" data-id="4">
							<img src="<?php echo getImg('menos-impostos-4.jpg') ?>" alt="RENEGOCIAÇÃO DA DÍVIDA DO RS" class="img-responsive">
							<p class="quote text-chamada-verde bg-amarelo-opacity">RENEGOCIAÇÃO DA DÍVIDA DO RS</p>
						</div>
						<div class="projeto amarelo text-center duo-img abre-projeto no-padding relative espacamento-mobile" data-categoria="menos-impostos" data-id="5">
							<img src="<?php echo getImg('menos-impostos-5.jpg') ?>" alt="Telefonia: Tarifa Alta, Muito Lucro, Baixa Qualidade" class="img-responsive">
							<p class="quote text-chamada-verde bg-amarelo-opacity">TELEFONIA: TARIFA ALTA, MUITO LUCRO, BAIXA QUALIDADE</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="projeto amarelo text-center duo-img abre-projeto no-padding margin-bottom relative espacamento-mobile" data-categoria="menos-impostos" data-id="6">
							<img src="<?php echo getImg('menos-impostos-6.jpg') ?>" alt="RENEGOCIAÇÃO DA DÍVIDA DO RS" class="img-responsive">
								<p class="quote text-chamada-verde bg-amarelo-opacity">CICLOVIAS E FAIXAS PARA AS PEQUENAS CIDADES</p>
						</div>
						<div class="projeto amarelo duo-img text-center abre-projeto no-padding relative" data-categoria="menos-impostos" data-id="7">
							<img src="<?php echo getImg('menos-impostos-7.jpg') ?>" alt="IPI ZERO PARA BICICLETAS" class="img-responsive">
								<p class="quote text-chamada-verde bg-amarelo-opacity">IPI ZERO PARA BICICLETAS</p>
						</div>
					</div>
				</div>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="menos-impostos-1">
				<a href="#" class="fechar-projeto pull-right" id="fechar-menos-impostos-1">Fechar Projeto</a>
				<h3 class="sem-linha">Micro e pequenas empresas ganham com a universalização do Simples</h3>
				<img src="<?php echo getImg('menos-impostos-1.jpg') ?>" alt="Micro e pequenas empresas ganham com a universalização do Simples" align="left" class="img-responsive">
				<p>Entre as vitórias do parlamentar na área está a aprovação do projeto de lei que altera o regime de tributação das micro e pequenas empresas e universaliza o acesso do setor de serviços ao Simples Nacional. Segundo Marchezan, há sete anos se lutava para incluir as categorias de serviço, advogados, consultores, entre outros, no regime especial.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="menos-impostos-2">
				<a href="#" class="fechar-projeto pull-right" id="fechar-menos-impostos-2">Fechar Projeto</a>
				<h3 class="sem-linha">Refis: oportunidade para quitar dívida</h3>
				<img src="<?php echo getImg('menos-impostos-2.jpg') ?>" alt="Refis: oportunidade para quitar dívida" align="left" class="img-responsive">
				<p>Marchezan também trabalhou pela renegociação das dívidas do Refis, defendendo a possibilidade de as empresas que não conseguiram cumprir todas as etapas para aderir ao regime especial tivessem uma nova oportunidade. Diversos trechos da regulamentação da lei são considerados confusos, o que teria induzido as empresas ao erro.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="menos-impostos-3">
				<a href="#" class="fechar-projeto pull-right" id="fechar-menos-impostos-3">Fechar Projeto</a>
				<h3 class="sem-linha">Fim de adicional de multa rescisória</h3>
				<img src="<?php echo getImg('menos-impostos-3.jpg') ?>" alt="Fim de adicional de multa rescisória" align="left" class="img-responsive">
				<p>O deputado defende o Projeto de Lei Complementar que extingue a contribuição adicional de 10% do FGTS na multa rescisória devida pelo empregador. A medida visa reduzir a carga tributária das empresas nacionais, que refletem na competitividade de produtos e serviços.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="menos-impostos-4">
				<a href="#" class="fechar-projeto pull-right" id="fechar-menos-impostos-4">Fechar Projeto</a>
				<h3 class="sem-linha">Renegociação da dívida do RS</h3>
				<img src="<?php echo getImg('menos-impostos-4.jpg') ?>" alt="Renegociação da dívida do RS" align="left" class="img-responsive">
				<p>No Congresso, Marchezan trabalhou para a aprovação do projeto que garantirá a troca do indexador que corrige as dívidas dos Estados e municípios com a União, substituindo o IGP-DI mais 6% a 9% ao ano para o IPCA mais 4% ao no ou pela variação da Taxa Selic – o que for menor. Isso permitirá ao RS a retomada de sua capacidade de investimento e do desenvolvimento econômico e social.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="menos-impostos-5">
				<a href="#" class="fechar-projeto pull-right" id="fechar-menos-impostos-5">Fechar Projeto</a>
				<h3 class="sem-linha">Telefonia: tarifa alta, muito lucro, baixa qualidade</h3>
				<img src="<?php echo getImg('menos-impostos-5.jpg') ?>" alt="Telefonia: tarifa alta, muito lucro, baixa qualidade" align="left" class="img-responsive">
				<p>Marchezan solicitou, em 2012, a realização de audiência pública para discutir a qualidade dos serviços de telefonia, o que resultou na proibição da venda de chips. Ele também solicitou ao Tribunal de Contas da União (TCU) a realização de auditoria sobre os procedimentos da Anatel na fiscalização dos serviços de telefonia móvel, telefonia fica, banda larga e TV por assinatura.</p>
				<p>Entre os problemas do setor está a cobrança indevida de serviços nas faturas dos consumidores, além da dificuldade de os clientes saberem pelo o que estão pagando. Para Marchezan, a Anatel é partidarizada e não atende aos interesses dos usuários dos serviços de telefonia.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="menos-impostos-6">
				<a href="#" class="fechar-projeto pull-right" id="fechar-menos-impostos-6">Fechar Projeto</a>
				<h3 class="sem-linha">Ciclovias e faixas para as pequenas cidades</h3>
				<img src="<?php echo getImg('menos-impostos-6.jpg') ?>" alt="Ciclovias e faixas para as pequenas cidades" align="left" class="img-responsive">
				<p>Municípios com 20 mil habitantes ou mais terão de planejar a construção de ciclovias e faixas exclusivas para motos. E esse planejamento deverá constar no Plano Diretor. A proposta vai melhorar o deslocamento nas cidades, e reduzir a emissão de poluentes.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="menos-impostos-7">
				<a href="#" class="fechar-projeto pull-right" id="fechar--menosimpostos-7">Fechar Projeto</a>
				<h3 class="sem-linha">IPI zero para bicicletas</h3>
				<img src="<?php echo getImg('menos-impostos-7.jpg') ?>" alt="IPI zero para bicicletas" align="left" class="img-responsive">
				<p>Melhorar as condições de deslocamento nas cidades e reduzir a emissão de poluentes são alguns dos aspectos favoráveis para incentivar o uso de bicicletas. Marchezan apoia a proposta que prevê IPI zero para as bicicletas.</p>
			</div>
		</div>
	</div>
</section>

<a href="#trabalho-transparencia" class="trabalho-transparencia"></a>
<a name="trabalho-transparencia"></a>
<section class="transparencia">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h2>Transparência</h2>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="projeto azul-escuro text-center unico no-padding espacamento-mobile">
					<img src="<?php echo getImg('transparencia-chamada.jpg') ?>" alt="Transparência" class="img-responsive">
					<p class="quote text-chamada-azul-escuro bg-azul" style="font-size: 11px !important">“Para Marchezan, o poder público deve divulgar o quanto gasta e em quais projetos investe o dinheiro dos impostos que o cidadão paga. Por isso, acredita que a transparência é a principal reforma a ser feita nesse sentido e a única forma de combater a corrupção e melhorar serviços essenciais como saúde, educação e segurança. O setor público precisa ser realmente público. ”</p>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="projeto azul-escuro text-center duo-img abre-projeto no-padding margin-bottom relative espacamento-mobile" data-categoria="transparencia" data-id="1">
							<img src="<?php echo getImg('transparencia-1.jpg') ?>" alt="DIVULGAÇÃO DE SALÁRIOS DE SERVIDORES" class="img-responsive">
							<div class="transparent">
								<p class="quote text-chamada-verde bg-azul-opacity">DIVULGAÇÃO DE SALÁRIOS DE SERVIDORES</p>
							</div>
						</div>
						<div class="projeto azul-escuro text-center duo-img abre-projeto no-padding relative espacamento-mobile" data-categoria="transparencia" data-id="2">
							<img src="<?php echo getImg('transparencia-2.jpg') ?>" alt="Detalhamento dos Gastos Públicos" class="img-responsive">
							<p class="quote text-chamada-verde bg-azul-opacity">DETALHAMENTO DOS GASTOS PÚBLICOS</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="projeto azul-escuro text-center duo-img abre-projeto no-padding margin-bottom relative espacamento-mobile" data-categoria="transparencia" data-id="3">
							<img src="<?php echo getImg('transparencia-3.jpg') ?>" alt="MAIS TRANSPARÊNCIA NOS RECURSOS PARA ONGs" class="img-responsive">
							<p class="quote text-chamada-verde bg-azul-opacity">MAIS TRANSPARÊNCIA NOS RECURSOS PARA ONGs</p>
						</div>
						<div class="projeto azul-escuro text-center duo-img abre-projeto no-padding relative espacamento-mobile" data-categoria="transparencia" data-id="4">
							<img src="<?php echo getImg('transparencia-4.jpg') ?>" alt="PENSÃO ALIMENTÍCIA DEVE BENEFICIAR O DEPENDENTE" class="img-responsive">
							<p class="quote text-chamada-verde bg-azul-opacity">PENSÃO ALIMENTÍCIA DEVE BENEFICIAR O DEPENDENTE</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="projeto azul-escuro duo-img text-center abre-projeto no-padding margin-bottom relative espacamento-mobile" data-categoria="transparencia" data-id="5">
							<img src="<?php echo getImg('transparencia-5.jpg') ?>" alt="PELO VOTO ABERTO" class="img-responsive">
								<p class="quote text-chamada-verde bg-azul-opacity">PELO VOTO ABERTO</p>
						</div>
						<div class="projeto azul-escuro text-center duo-img abre-projeto no-padding relative espacamento-mobile" data-categoria="transparencia" data-id="6">
							<img src="<?php echo getImg('transparencia-6.jpg') ?>" alt="QUEM NÃO PAGA PENSÃO PODE ENTRAR PARA A LISTA DE INADIMPLENTES" class="img-responsive">
								<p class="quote text-chamada-verde bg-azul-opacity">QUEM NÃO PAGA PENSÃO PODE ENTRAR PARA A LISTA DE INADIMPLENTES</p>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="projeto azul-escuro text-center unico abre-projeto margin-top espacamento-mobile" data-categoria="transparencia" data-id="7">
					<h3 class="titulo-projeto-grande bg-azul box">DE OLHO NOS SUPERSALÁRIOS</h3>
					<img src="<?php echo getImg('transparencia-7.jpg') ?>" alt="DE OLHO NOS SUPERSALÁRIOS" class="img-responsive">
				</div>
			</div>
			<div class="coil-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="projeto azul-escuro text-center unico abre-projeto no-padding margin-top espacamento-mobile" data-categoria="transparencia" data-id="8">
					<img src="<?php echo getImg('transparencia-8.jpg') ?>" alt="Fiscalização nos Reajustes das Tarifas de Energia" class="img-responsive">
					<h3 class="titulo-projeto-grande bg-azul box" style="font-size: 25px">FISCALIZAÇÃO NOS REAJUSTES DAS TARIFAS DE ENERGIA</h3>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="projeto azul-escuro text-center duo-img abre-projeto no-padding margin-top margin-bottom relative espacamento-mobile" data-categoria="transparencia" data-id="9">
							<img src="<?php echo getImg('transparencia-9.jpg') ?>" alt="PEC 190 CRIA MAIS PRIVILÉGIOS" class="img-responsive">
							<div class="transparent">
								<p class="quote text-chamada-verde bg-azul-opacity">
								PEC 190 CRIA MAIS PRIVILÉGIOS
								</p>
							</div>
						</div>
						<div class="projeto azul-escuro text-center DUO-IMG abre-projeto no-padding relative espacamento-mobile" data-categoria="transparencia" data-id="10">
							<img src="<?php echo getImg('transparencia-10.jpg') ?>" alt="FIM DO “FURA-FILA” NA JUSTIÇA" class="img-responsive">
							<p class="quote text-chamada-verde bg-azul-opacity">FIM DO “FURA-FILA” NA JUSTIÇA</p>
						</div>
					</div>
				</div>
			</div>
			<div class="coil-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="projeto azul-escuro text-center unico abre-projeto no-padding margin-top espacamento-mobile" data-categoria="transparencia" data-id="11">
					<h3 class="titulo-projeto-grande bg-azul box" style="font-size:27px">REINCIDÊNCIA EM CRIMES ACABA COM CONDICIONAL</h3>
					<img src="<?php echo getImg('transparencia-11.jpg') ?>" alt="REINCIDÊNCIA EM CRIMES ACABA COM CONDICIONAL" class="img-responsive">
				</div>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="transparencia-1">
				<a href="#" class="fechar-projeto pull-right" id="fechar-transparencia-1">Fechar Projeto</a>
				<h3 class="sem-linha">Divulgação de salários de servidores</h3>
				<img src="<?php echo getImg('transparencia-1.jpg') ?>" alt="Divulgação de salários de servidores" align="left" class="img-responsive">
				<p>Como deputado estadual, Marchezan foi o autor da lei que determina a publicação individualizada dos salários pagos a cada cargo ocupado no setor público estadual. O Tribunal de Justiça, no entanto, não cumpria essa norma. Por essa razão, o parlamentar recorreu ao Conselho Nacional de Justiça, que publicou decisão favorável ao pedido, reafirmando a importância da medida criada pelo deputado.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="transparencia-2">img-responsiveo pull-right" id="fechar-transparencia-2">Fechar Projeto</a>
				<h3 class="sem-linha">Detalhamento dos gastos públicos</h3>
				<img src="<?php echo getImg('transparencia-2.jpg') ?>" alt="Detalhamento dos gastos públicos" align="left" class="img-responsive">
				<p>Uma das armas para enfrentar a corrupção está o investimento nos avanços da tecnologia da informação, que permitem conhecer, com rapidez quase instantânea, como estão sendo aplicados os recursos públicos. O Projeto de Lei (PLP) 116, de sua autoria, prevê revelar os gastos do governo e reforçar o princípio da transparência. </p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="transparencia-3">
				<a href="#" class="fechar-projeto pull-right" id="fechar-transparencia-3">Fechar Projeto</a>
				<h3 class="sem-linha">Mais transparência nos recursos para ONGs</h3>
				<img src="<?php echo getImg('transparencia-3.jpg') ?>" alt="Mais transparência nos recursos para ONGs" align="left" class="img-responsive">
				<p>Para garantir transparência na parceria entre organizações não-governamentais (ONGs) e governos , Marchezan defende o projeto que estabelece normas, como chamamento público obrigatório para acesso aos recursos, controle e alcance de resultados. Isso impede a parceria com organizações não-governamentais (ONGs) e dirigentes que tenham praticado crimes ou outros atos de violação.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="transparencia-4">
				<a href="#" class="fechar-projeto pull-right" id="fechar-transparencia-4">Fechar Projeto</a>
				<h3 class="sem-linha">Pensão alimentícia deve beneficiar o dependente</h3>
				<img src="<?php echo getImg('transparencia-4.jpg') ?>" alt="Pensão alimentícia deve beneficiar o dependente" align="left" class="img-responsive">
				<p>O deputado é autor do Projeto de Lei que prevê a obrigatoriedade de prestação de contas mensal sobre as despesas do dependente à pessoa que tem obrigação legal de pagar pensão alimentícia. A proposta garante o benefício ao principal interessado e dá mais segurança a todos os envolvidos.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="transparencia-5">
				<a href="#" class="fechar-projeto pull-right" id="fechar-transparencia-5">Fechar Projeto</a>
				<h3 class="sem-linha">Pelo voto aberto</h3>
				<img src="<?php echo getImg('transparencia-5.jpg') ?>" alt="Pelo voto aberto" align="left" class="img-responsive">
				<p>Para Marchezan, o voto aberto no Congresso é fundamental. Ele acredita que a transparência é um grande remédio contra a corrupção, a incompetência e os privilégios que imperam no país.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="transparencia-6">
				<a href="#" class="fechar-projeto pull-right" id="fechar-transparencia-6">Fechar Projeto</a>
				<h3 class="sem-linha">Quem não paga pensão pode entrar para lista de inadimplentes</h3>
				<img src="<?php echo getImg('transparencia-6.jpg') ?>" alt="Quem não paga pensão pode entrar para lista de inadimplentes" align="left" class="img-responsive">
				<p>Marchezan é favorável ao projeto de lei que determina que os inadimplentes de pensão alimentícia tenham seus nomes inscritos nos serviços de proteção ao crédito, como SPC e Serasa. Ele propõe que a inclusão do nome do devedor somente seja feita após a citação para pagamento das prestações vencidas, como estabelece o Código de Processo Civil.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="transparencia-7">
				<a href="#" class="fechar-projeto pull-right" id="fechar-transparencia-7">Fechar Projeto</a>
				<h3 class="sem-linha">De olho nos supersalários</h3>
				<img src="<?php echo getImg('transparencia-7.jpg') ?>" alt="De olho nos supersalários" align="left" class="img-responsive">
				<p>A Constituição Federal determina que nenhum servidor público tenha salário maior do que os ministros do Supremo Tribunal Federal, os quais, hoje, recebem R$ 29.462,25. No entanto, isso é descumprido, e os órgãos que deveriam julgar e fiscalizar ignoram o que está na lei. Marchezan solicitou auditoria do Tribunal de Contas da União, na qual foram identificados 3.390 servidores da administração federal recebendo acima do limite.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="transparencia-8">
				<a href="#" class="fechar-projeto pull-right" id="fechar-transparencia-8">Fechar Projeto</a>
				<h3 class="sem-linha">Fiscalização nos reajustes das tarifas de energia</h3>
				<img src="<?php echo getImg('transparencia-8.jpg') ?>" alt="Fiscalização nos reajustes das tarifas de energia" align="left" class="img-responsive">
				<p>Depois de a presidente Dilma Rousseff ter anunciado corte de 18% nas contas de luz de residências e de até 32% para a indústria, os brasileiros viram o reajuste chegar a até 30%. Marchezan solicitou a realização de audiência pública na Câmara para cobrar esclarecimento sobre os reajustes.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="transparencia-9">
				<a href="#" class="fechar-projeto pull-right" id="fechar-transparencia-9">Fechar Projeto</a>
				<h3 class="sem-linha">PEC 190 cria mais privilégios</h3>
				<img src="<?php echo getImg('transparencia-9.jpg') ?>" alt="PEC 190 cria mais privilégios" align="left" class="img-responsive">
				<p>Se aprovada, a PEC 190 – que autoriza a criação do estatuto dos servidores do Judiciário e propõe uma carreira única para funcionários federais e estaduais desse Poder – poderá criar uma casta na qual os servidores da Justiça serão elevados a um patamar exclusivo no serviço público.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="transparencia-10">
				<a href="#" class="fechar-projeto pull-right" id="fechar-transparencia-10">Fechar Projeto</a>
				<h3 class="sem-linha">Fim do “fura-fila” na Justiça</h3>
				<img src="<?php echo getImg('transparencia-10.jpg') ?>" alt="Fim do “fura-fila” na Justiça" align="left" class="img-responsive">
				<p>Marchezan é autor da emenda que impede “furar a fila” de processos, dando prioridade para a análise dos casos mais antigos. A mudança já faz parte do novo Código de Processo Civil (CPC). Agora as varas judiciais têm que dar andamento aos atos processuais na mesma ordem cronológica dos despachos judiciais e protocolos. Além disso, a ordem dos atos processuais deve ser pública. O deputado apresentou 25 emendas ao novo CPC. Dessas, 17 foram aprovadas pelo relator.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="transparencia-11">
				<a href="#" class="fechar-projeto pull-right" id="fechar-transparencia-11">Fechar Projeto</a>
				<h3 class="sem-linha">Reincidência em crimes acaba com condicional</h3>
				<img src="<?php echo getImg('transparencia-11.jpg') ?>" alt="Reincidência em crimes acaba com condicional" align="left" class="img-responsive">
				<p>No Brasil, sete em cada 10 presos que deixam o sistema penitenciário volta ao crime, uma das maiores taxas de reincidência do mundo, segundo o CNJ. Marchezan é relator de projeto para reverter esse quadro e estuda formas legais para garantir a possibilidade de detenção de reincidentes.</p>
			</div>
		</div>
	</div>
</section>

<a href="#trabalho-combate-a-corrupcao" class="trabalho-combate-a-corrupcao"></a>
<a name="trabalho-combate-a-corrupcao"></a>
<section class="corrupcao">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h2>Combate à Corrupção</h2>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="projeto marrom text-center unico no-padding espacamento-mobile">
					<img src="<?php echo getImg('combate-corrupcao-chamada.jpg') ?>" alt="Combate à Corrupção" class="img-responsive">
					<p class="quote text-chamada-marrom bg-marrom" style="font-size: 13px !important">“Marchezan atua intensivamente no combate à corrupção. Faz parte da Organização Mundial de Parlamentares contra a Corrupção, a qual reúne deputados de todo o mundo interessados em novos meios de estimular a transparência e a integridade no ambiente político.”</p>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="projeto marrom text-center unico abre-projeto no-padding margin-bottom espacamento-mobile" data-categoria="combate-a-corrupcao" data-id="1">
					<h3 class="titulo-projeto-grande bg-marrom">CARTÓRIOS:<br>A LEGALIZAÇÃO DO ABSURDO</h3>
					<img src="<?php echo getImg('combate-corrupcao-1.jpg') ?>" alt="Cartórios: A Legalização do Absurdo" class="img-responsive">
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="projeto marrom text-center duo-img abre-projeto no-padding margin-bottom relative espacamento-mobile" data-categoria="combate-a-corrupcao" data-id="2">
							<img src="<?php echo getImg('combate-corrupcao-2.jpg') ?>" alt="MAIS INTEGRIDADE NA POLÍTICA" class="img-responsive">
							<p class="quote text-chamada-verde bg-marrom-opacity">MAIS INTEGRIDADE NA POLÍTICA</p>
						</div>
						<div class="projeto marrom text-center duo-img abre-projeto no-padding relative espacamento-mobile" data-categoria="combate-a-corrupcao" data-id="3">
							<img src="<?php echo getImg('combate-corrupcao-3.jpg') ?>" alt="SUSPENSÃO DO PAGAMENTO DA URV" class="img-responsive">
							<p class="quote text-chamada-verde bg-marrom-opacity">SUSPENSÃO DO PAGAMENTO DA URV</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="projeto marrom text-center duo-img abre-projeto no-padding margin-bottom relative espacamento-mobile" data-categoria="combate-a-corrupcao" data-id="4">
							<img src="<?php echo getImg('combate-corrupcao-4.jpg') ?>" alt="CONTA O AUXÍLIO MORADIA PARA JUDICIÁRIO" class="img-responsive">
							<p class="quote text-chamada-verde bg-marrom-opacity">CONTA O AUXÍLIO MORADIA PARA JUDICIÁRIO</p>
						</div>
						<div class="projeto marrom text-center duo-img abre-projeto no-padding relative" data-categoria="combate-a-corrupcao" data-id="5">
							<img src="<?php echo getImg('combate-corrupcao-5.jpg') ?>" alt="SUBSÍDIOS RETROATIVOS DO MP: ENRIQUECIMENTO ILÍCITO" class="img-responsive">
							<p class="quote text-chamada-verde bg-marrom-opacity">SUBSÍDIOS RETROATIVOS DO MP: ENRIQUECIMENTO ILÍCITO</p>
						</div>
					</div>
				</div>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="combate-a-corrupcao-1">
				<a href="#" class="fechar-projeto pull-right" id="fechar-combate-a-corrupcao-1">Fechar Projeto</a>
				<h3 class="sem-linha">Cartórios: a legalização do absurdo</h3>
				<img src="<?php echo getImg('combate-corrupcao-1.jpg') ?>" alt="Cartórios: a legalização do absurdo" align="left" class="img-responsive">
				<p>Além disso, o parlamentar acredita que a chamada PEC dos Cartórios (PEC 471) é um absurdo, pois contraria a regra de ouro da Constituição de que, para ser perene no serviço público, tem de passar por concurso. A proposta propõe efetivar os titulares de serviços notariais e de registro que trabalham interinamente, ou seja, sem concurso público.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="combate-a-corrupcao-2">
				<a href="#" class="fechar-projeto pull-right" id="fechar-combate-a-corrupcao-2">Fechar Projeto</a>
				<h3 class="sem-linha">Mais integridade na política</h3>
				<img src="<?php echo getImg('combate-corrupcao-2.jpg') ?>" alt="Mais integridade na política" align="left" class="img-responsive">
				<p>Marchezan integra a Organização Mundial de Parlamentares Contra a Corrupção, a qual reúne deputados de todo o mundo interessados em novos meios de estímulo à transparência e à integridade no ambiente político. </p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="combate-a-corrupcao-3">
				<a href="#" class="fechar-projeto pull-right" id="fechar-combate-a-corrupcao-3">Fechar Projeto</a>
				<h3 class="sem-linha">Suspensão do pagamento da URV</h3>
				<img src="<?php echo getImg('combate-corrupcao-3.jpg') ?>" alt="Suspensão do pagamento da URV" align="left" class="img-responsive">
				<p>O deputado defende a devolução dos valores pagos indevidamente da correção da URV pelo Poder Judiciário. Em 2007, uma reportagem denunciou a “farra da URV” no RS. A Lei 8.880/94 determinou a conversão dos salários a partir de março de 1994 pela conversão da URV correspondente ao dia do efetivo pagamento pela média aritmética dos salários dos meses de novembro de 1993 a fevereiro de 1994. Um erro no cálculo da conversão gerou a majoração no vencimento básico de todos os juízes em 10,62% e dos servidores em 4,43%.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="combate-a-corrupcao-4">
				<a href="#" class="fechar-projeto pull-right" id="fechar-combate-a-corrupcao-4">Fechar Projeto</a>
				<h3 class="sem-linha">Contra o auxílio-moradia para Judiciário</h3>
				<img src="<?php echo getImg('combate-corrupcao-4.jpg') ?>" alt="Contra o auxílio-moradia para Judiciário" align="left" class="img-responsive">
				<p>O deputado federal protocolou representação no Conselho Nacional de Justiça (CNJ) contra o pagamento do auxílio-moradia para os membros do judiciário do Rio Grande do Sul. Na prática, Marchezan questiona a ilegalidade dos pagamentos, os juros que incidem sobre os valores retroativos pagos e o valor acima do teto constitucional.</p>
				<p>Em 2010, uma decisão administrativa do Tribunal de Justiça autorizou 900 juízes e desembargadores gaúchos a receberem auxílio-moradia. O valor é de R$ 7 mil por mês, em média. O ato administrativo do TJ alega igualdade de direitos entre o Legislativo e o Judiciário. De acordo com uma auditoria do Tribunal de Contas do Estado benefício aos juízes custariam R$ 600 milhões (valores respectivos a 2010) aos cofres públicos.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="combate-a-corrupcao-5">
				<a href="#" class="fechar-projeto pull-right" id="fechar-combate-a-corrupcao-5">Fechar Projeto</a>
				<h3 class="sem-linha">Subsídios retroativos do MP: enriquecimento ilícito</h3>
				<img src="<?php echo getImg('combate-corrupcao-5.jpg') ?>" alt="Subsídios retroativos do MP: enriquecimento ilícito" align="left" class="img-responsive">
				<p>Marchezan é contra a decisão do Conselho Nacional do Ministério Público (CNMP), que determinou que o Estado pague os valores da diferença salarial retroativa de janeiro de 2005 a março de 2009 a promotores e procuradores gaúchos. Conforme o parlamentar, se trata de uma “afronta à divisão de poderes” para “enriquecimento ilícito dos promotores gaúchos”.</p>
				<p>A partir de março de 2009, por decisão da AL-RS, promotores e procuradores do Ministério Público gaúcho passaram a receber remuneração pelo sistema de subsídio, que substitui o pagamento por vencimentos e institui faixas salariais fixas. A medida significou uma alta expressiva no contracheque da categoria e representou uma despesa de R$ 217 milhões ao MP-RS. À época, boa parte dos membros teve ganhos de até 40%.</p>
			</div>
		</div>
	</div>
</section>

<a href="#trabalho-seguranca" class="trabalho-seguranca"></a>
<a name="trabalho-seguranca"></a>
<section class="seguranca">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h2>Segurança</h2>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="projeto verde text-center unico no-padding espacamento-mobile">
					<img src="<?php echo getImg('seguranca-chamada.jpg') ?>" alt="Segurança" class="img-responsive">
					<p class="quote text-chamada-verde bg-verde box" style="font-size: 16px !important">“Investir na melhoria dos quadros da segurança e estipular o piso nacional são ações fundamentais defendidas por Marchezan para proporcionar à população mais segurança.”</p>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="projeto verde text-center duo-img abre-projeto no-padding margin-bottom relative espacamento-mobile" data-categoria="seguranca" data-id="1">
							<img src="<?php echo getImg('seguranca-1.jpg') ?>" alt="PISO SALARIAL PARA POLICIAIS E BOMBEIROS" class="img-responsive">
							<p class="quote text-chamada-verde bg-verde-opacity">PISO SALARIAL PARA POLICIAIS E BOMBEIROS</p>
						</div>
						<div class="projeto verde text-center duo-img abre-projeto no-padding relative espacamento-mobile" data-categoria="seguranca" data-id="2">
							<img src="<?php echo getImg('seguranca-2.jpg') ?>" alt="NOVAS REGRAS PARA COMBATER CRIMES NA INTERNET" class="img-responsive">
							<p class="quote text-chamada-verde bg-verde-opacity">NOVAS REGRAS PARA COMBATER CRIMES NA INTERNET</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="projeto verde text-center unico abre-projeto no-padding espacamento-mobile" data-categoria="seguranca" data-id="3">
					<h3 class="titulo-projeto-grande bg-verde box" style="font-size: 25px;line-height: 140%;">IDENTIFICAÇÃO PELAS DIGITAIS É GARANTIA DE SEGURANÇA</h3>
					<img src="<?php echo getImg('seguranca-3.jpg') ?>" alt="Identificação Pelas Digitais é Garantia de Segurança" class="img-responsive">
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="projeto verde text-center unico abre-projeto no-padding" data-categoria="seguranca" data-id="4">
					<img src="<?php echo getImg('seguranca-4.jpg') ?>" alt="Desermamento em Debate" class="img-responsive">
					<h3 class="titulo-projeto-grande bg-verde box">DESARMAMENTO EM DEBATE</h3>
				</div>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="seguranca-1">
				<a href="#" class="fechar-projeto pull-right" id="fechar-seguranca-1">Fechar Projeto</a>
				<h3 class="sem-linha">Piso salarial para policiais e bombeiros</h3>
				<img src="<?php echo getImg('seguranca-1.jpg') ?>" alt="Piso salarial para policiais e bombeiros" align="left" class="img-responsive">
				<p>Investir na melhoria dos quadros da segurança - policiais e bombeiros - e estipular o piso nacional são ações fundamentais para a valorização das categorias. Marchezan afirma que o governo federal tem se omitido diante do tema, não adotando medidas efetivas para a implantação do piso. A remuneração justa à categoria é prevista pela PEC 300, que busca a equiparação dos salários dos policiais militares e bombeiros de todos os Estados ao que recebem os PMs do Distrito Federal, os mais bem pagos do país.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="seguranca-2">
				<a href="#" class="fechar-projeto pull-right" id="fechar-seguranca-2">Fechar Projeto</a>
				<h3 class="sem-linha">Novas regras para combater crimes na internet</h3>
				<img src="<?php echo getImg('seguranca-2.jpg') ?>" alt="Novas regras para combater crimes na internet" align="left" class="img-responsive">
				<p>Atento à difusão dos crimes virtuais, Marchezan vai apresentar projeto de lei para tornar ilícito o uso de perfis falsos na internet que tenham o objetivo de prejudicar, intimidar, ameaçar, obter vantagem ou causar dano a terceiros. Para ele, é necessária uma alteração no Código Penal Brasileiro, especificamente no artigo 307, o qual prevê crime de falsa identidade.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="seguranca-3">
				<a href="#" class="fechar-projeto pull-right" id="fechar-seguranca-3">Fechar Projeto</a>
				<h3 class="sem-linha">Identificação pelas digitais é garantia de segurança</h3>
				<img src="<?php echo getImg('seguranca-3.jpg') ?>" alt="Identificação pelas digitais é garantia de segurança" align="left" class="img-responsive">
				<p>O parlamentar defende a criação de cadastro único e a obrigatoriedade da identificação biométrica – a leitura das digitais – para a emissão de documento de identidade. Medida que contribuiria no combate ao tráfico de pessoas, particularmente mulheres e crianças, além de facilitar a identificação em diversos documentos.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="seguranca-4">
				<a href="#" class="fechar-projeto pull-right" id="fechar-seguranca-4">Fechar Projeto</a>
				<h3 class="sem-linha">Desarmamento em debate</h3>
				<img src="<?php echo getImg('seguranca-4.jpg') ?>" alt="Desarmamento em debate" align="left" class="img-responsive">
				<p>Membro da Comissão Especial que analisa o Projeto de Lei que revoga o estatuto do desarmamento, Marchezan trabalha para promover o debate do assunto entre os gaúchos. “O governo dificultou o acesso às armas para os cidadãos de bem. No entanto, afrouxou a fiscalização das nossas fronteiras, permitindo que os criminosos aumentassem seu poder de fogo e que os índices de criminalidade disparassem no Brasil”, analisa o parlamentar.</p>
			</div>
		</div>
	</div>
</section>

<a href="#trabalho-agricultura" class="trabalho-agricultura"></a>
<a name="trabalho-agricultura"></a>
<section class="agricultura">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h2>Agricultura</h2>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="projeto amarelo text-center unico no-padding espacamento-mobile">
					<img src="<?php echo getImg('agricultura-chamada.jpg') ?>" alt="Agricultura" class="img-responsive">
					<p class="quote text-chamada-verde bg-amarelo box">“A agricultura está entre as prioridades de Marchezan, que atua diretamente na valorização do homem do campo, no campo.”</p>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="projeto amarelo text-center duo-img abre-projeto no-padding margin-bottom relative espacamento-mobile" data-categoria="agricultura" data-id="1">
							<img src="<?php echo getImg('agricultura-1.jpg') ?>" alt="GARANTIAS E DIREITOS PARA MANTER O HOMEM NO CAMPO" class="img-responsive">
							<p class="quote text-chamada-verde bg-amarelo-opacity">GARANTIAS E DIREITOS PARA MANTER O HOMEM NO CAMPO</p>
						</div>
						<div class="projeto amarelo text-center duo-img abre-projeto no-padding relative espacamento-mobile" data-categoria="agricultura" data-id="2">
							<img src="<?php echo getImg('agricultura-2.jpg') ?>" alt="INCENTIVO PARA MODERNIZAR A PRODUÇÃO AGRÍCOLA" class="img-responsive">
							<p class="quote text-chamada-verde bg-amarelo-opacity">INCENTIVO PARA MODERNIZAR A PRODUÇÃO AGRÍCOLA</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="projeto amarelo text-center duo-img abre-projeto no-padding margin-bottom relative espacamento-mobile" data-categoria="agricultura" data-id="3">
							<img src="<?php echo getImg('agricultura-3.jpg') ?>" alt="HOMENAGEM AO CINQUENTENÁRIO DA FETAG-RS" class="img-responsive">
								<p class="quote text-chamada-verde bg-amarelo-opacity">HOMENAGEM AO CINQUENTENÁRIO DA FETAG-RS</p>
						</div>
						<div class="projeto amarelo text-center duo-img abre-projeto no-padding relative espacamento-mobile" data-categoria="agricultura" data-id="4">
							<img src="<?php echo getImg('agricultura-4.jpg') ?>" alt="CÓDIGO FLORESTAL: SUSTENTABILIDADE NO AGRONEGÓCIO" class="img-responsive">
							<p class="quote text-chamada-verde bg-amarelo-opacity">CÓDIGO FLORESTAL: SUSTENTABILIDADE NO AGRONEGÓCIO</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="projeto amarelo text-center unico abre-projeto no-padding" data-categoria="agricultura" data-id="5">
					<h3 class="titulo-projeto-grande bg-amarelo">POLÍTICAS DE RECICLAGEM EUROPÉIAS</h3>
					<img src="<?php echo getImg('agricultura-5.jpg') ?>" alt="Políricas de Reciclagem Européias" class="img-responsive">
				</div>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="agricultura-1">
				<a href="#" class="fechar-projeto pull-right" id="fechar-agricultura-1">Fechar Projeto</a>
				<h3 class="sem-linha">Garantias e direitos para manter o homem no campo</h3>
				<img src="<?php echo getImg('agricultura-1.jpg') ?>" alt="Garantias e direitos para manter o homem no campo" align="left" class="img-responsive">
				<p>O combate à informalidade e a luta pela garantia dos direitos trabalhistas e previdenciários, através do fortalecimento das entidades que congregam os trabalhadores rurais estão entre as suas ações. </p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="agricultura-2">
				<a href="#" class="fechar-projeto pull-right" id="fechar-agricultura-2">Fechar Projeto</a>
				<h3 class="sem-linha">Incentivo para modernizar a produção agrícola</h3>
				<img src="<?php echo getImg('agricultura-2.jpg') ?>" alt="Incentivo para modernizar a produção agrícola" align="left" class="img-responsive">
				<p>Um dos caminhos para aumentar a produção defendido por Marchezan é a modernização do setor primário. Por isso, facilitar a aquisição de máquinas e equipamentos agrícolas é uma das ações sistemáticas do parlamentar. Por meio de emendas parlamentares, o deputado tem possibilitado aos produtores gaúchos o aumento da produtividade das lavouras.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="agricultura-3">
				<a href="#" class="fechar-projeto pull-right" id="fechar-agricultura-3">Fechar Projeto</a>
				<h3 class="sem-linha">Homenagem ao cinquentenário da Fetag-RS</h3>
				<img src="<?php echo getImg('agricultura-3.jpg') ?>" alt="Homenagem ao cinquentenário da Fetag-RS" align="left" class="img-responsive">
				<p>Os 50 anos da fundação da Federação dos Trabalhadores na Agricultura do Rio Grande do Sul (Fetag-RS) foram motivo de sessão solene proposta por Marchezan, em Brasília.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="agricultura-4">
				<a href="#" class="fechar-projeto pull-right" id="fechar-agricultura-4">Fechar Projeto</a>
				<h3 class="sem-linha">Código Florestal: sustentabilidade no agronegócio</h3>
				<img src="<?php echo getImg('agricultura-4.jpg') ?>" alt="Código Florestal: sustentabilidade no agronegócio" align="left" class="img-responsive">
				<p>O deputado atuou na aprovação do novo Código Florestal Brasileiro com o objetivo de impulsionar a agricultura sustentável sem comprometer os recursos naturais. Promovendo o consenso, Marchezan trabalhou na regulamentação das normas que hoje integram agricultura, meio ambiente e sustentabilidade em prol do desenvolvimento da economia no campo.</p>
			</div>
			<div class="projeto-aberto esconde col-lg-12 col-md-12 col-sm-12 col-xs-12" id="agricultura-5">
				<a href="#" class="fechar-projeto pull-right" id="fechar-agricultura-5">Fechar Projeto</a>
				<h3 class="sem-linha">Políticas de reciclagem europeias</h3>
				<img src="<?php echo getImg('agricultura-5.jpg') ?>" alt="Políticas de reciclagem europeias" align="left" class="img-responsive">
				<p>Marchezan representou a Câmara de Deputados em missão oficial a Portugal, onde realizou visitas técnicas e encontros com especialistas para conhecer o processo de reciclagem de resíduos sólidos realizado na Comunidade Europeia, desde o início da reciclagem até o gerenciamento de fluxos de resíduos.</p>
			</div>
		</div>
	</div>
</section>