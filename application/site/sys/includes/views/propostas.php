<section class="propostas hidden-sm hidden-xs">
			<div class="container">
				<h2 class="padrao text-center">Propostas</h2>
				<h3 class="padrao text-center">Um pouco do que pretendo fazer pelo <span>Brasil</span> nos próximos <span>4 anos.</span></h3>
			</div>
			<div class="slide-propostas">
				<div class="container">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div id="controla-tabs" class="flexslider">
							<ul class="slides">
								<li class="text-center">
									<img src="<?php echo getImg('educacao-ico.png') ?>" alt="Educação" class="img-responsive">
									<span>Educação</span>
								</li>
								<li class="text-center">
									<img src="<?php echo getImg('saude-ico.png') ?>" alt="Saúde" class="img-responsive">
									<span>Saúde</span>
								</li>
								<li class="text-center">
									<img src="<?php echo getImg('imposto-ico.png') ?>" alt="Menos impostos" class="img-responsive">
									<span>Menos impostos</span>
								</li>
								<li class="text-center">
									<img src="<?php echo getImg('transparencia-ico.png') ?>" alt="Transparência" class="img-responsive">
									<span>Transparência</span>
								</li>
								<li class="text-center">
									<img src="<?php echo getImg('seguranca-ico.png') ?>" alt="Segurança" class="img-responsive">
									<span>Segurança</span>
								</li>
								<li class="text-center">
									<img src="<?php echo getImg('agricultura-ico.png') ?>" alt="Agricultura" class="img-responsive">
									<span>Agricultura</span>
								</li>
							</ul>
						</div>
						<div id="tabs" class="flexslider">
							<ul class="slides">
								<li>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi lobortis tincidunt elit, tempus porta eros ornare a. Aliquam erat volutpat. Sed fringilla consequat tellus sit amet lacinia. Integer euismod suscipit turpis id facilisis. Ut sit amet placerat libero. Pellentesque ornare magna tincidunt, dictum mauris nec, pulvinar velit. Phasellus malesuada ipsum vitae ipsum faucibus, id dictum elit rhoncus. Cras non massa volutpat, pellentesque sem eu, sagittis magna. Cras ac commodo neque. In vitae tristique leo. Proin viverra enim at sem laoreet, in vehicula leo volutpat. Duis lacus nisi, lacinia eu ultricies non, ornare eget orci. Integer arcu sem, faucibus quis adipiscing viverra, consectetur ac neque. Phasellus euismod nunc felis, ut egestas erat feugiat eu. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi lobortis tincidunt elit, tempus porta eros ornare a. Aliquam erat volutpat. Sed fringilla consequat tellus sit amet lacinia. Integer euismod suscipit turpis id facilisis. Ut sit amet placerat libero. Pellentesque ornare magna tincidunt, dictum mauris nec, pulvinar velit. Phasellus malesuada ipsum vitae ipsum faucibus, id dictum elit rhoncus. </p>
								</li>
								<li>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi lobortis tincidunt elit, tempus porta eros ornare a. Aliquam erat volutpat. Sed fringilla consequat tellus sit amet lacinia. Integer euismod suscipit turpis id facilisis. Ut sit amet placerat libero. Pellentesque ornare magna tincidunt, dictum mauris nec, pulvinar velit. Phasellus malesuada ipsum vitae ipsum faucibus, id dictum elit rhoncus. Cras non massa volutpat, pellentesque sem eu, sagittis magna. Cras ac commodo neque. In vitae tristique leo. Proin viverra enim at sem laoreet, in vehicula leo volutpat. Duis lacus nisi, lacinia eu ultricies non, ornare eget orci. Integer arcu sem, faucibus quis adipiscing viverra, consectetur ac neque. Phasellus euismod nunc felis, ut egestas erat feugiat eu. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi lobortis tincidunt elit, tempus porta eros ornare a. Aliquam erat volutpat. Sed fringilla consequat tellus sit amet lacinia. Integer euismod suscipit turpis id facilisis. Ut sit amet placerat libero. Pellentesque ornare magna tincidunt, dictum mauris nec, pulvinar velit. Phasellus malesuada ipsum vitae ipsum faucibus, id dictum elit rhoncus. </p>
								</li>
								<li>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi lobortis tincidunt elit, tempus porta eros ornare a. Aliquam erat volutpat. Sed fringilla consequat tellus sit amet lacinia. Integer euismod suscipit turpis id facilisis. Ut sit amet placerat libero. Pellentesque ornare magna tincidunt, dictum mauris nec, pulvinar velit. Phasellus malesuada ipsum vitae ipsum faucibus, id dictum elit rhoncus. Cras non massa volutpat, pellentesque sem eu, sagittis magna. Cras ac commodo neque. In vitae tristique leo. Proin viverra enim at sem laoreet, in vehicula leo volutpat. Duis lacus nisi, lacinia eu ultricies non, ornare eget orci. Integer arcu sem, faucibus quis adipiscing viverra, consectetur ac neque. Phasellus euismod nunc felis, ut egestas erat feugiat eu. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi lobortis tincidunt elit, tempus porta eros ornare a. Aliquam erat volutpat. Sed fringilla consequat tellus sit amet lacinia. Integer euismod suscipit turpis id facilisis. Ut sit amet placerat libero. Pellentesque ornare magna tincidunt, dictum mauris nec, pulvinar velit. Phasellus malesuada ipsum vitae ipsum faucibus, id dictum elit rhoncus. </p>
								</li>
								<li>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi lobortis tincidunt elit, tempus porta eros ornare a. Aliquam erat volutpat. Sed fringilla consequat tellus sit amet lacinia. Integer euismod suscipit turpis id facilisis. Ut sit amet placerat libero. Pellentesque ornare magna tincidunt, dictum mauris nec, pulvinar velit. Phasellus malesuada ipsum vitae ipsum faucibus, id dictum elit rhoncus. Cras non massa volutpat, pellentesque sem eu, sagittis magna. Cras ac commodo neque. In vitae tristique leo. Proin viverra enim at sem laoreet, in vehicula leo volutpat. Duis lacus nisi, lacinia eu ultricies non, ornare eget orci. Integer arcu sem, faucibus quis adipiscing viverra, consectetur ac neque. Phasellus euismod nunc felis, ut egestas erat feugiat eu. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi lobortis tincidunt elit, tempus porta eros ornare a. Aliquam erat volutpat. Sed fringilla consequat tellus sit amet lacinia. Integer euismod suscipit turpis id facilisis. Ut sit amet placerat libero. Pellentesque ornare magna tincidunt, dictum mauris nec, pulvinar velit. Phasellus malesuada ipsum vitae ipsum faucibus, id dictum elit rhoncus. </p>
								</li>
								<li>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi lobortis tincidunt elit, tempus porta eros ornare a. Aliquam erat volutpat. Sed fringilla consequat tellus sit amet lacinia. Integer euismod suscipit turpis id facilisis. Ut sit amet placerat libero. Pellentesque ornare magna tincidunt, dictum mauris nec, pulvinar velit. Phasellus malesuada ipsum vitae ipsum faucibus, id dictum elit rhoncus. Cras non massa volutpat, pellentesque sem eu, sagittis magna. Cras ac commodo neque. In vitae tristique leo. Proin viverra enim at sem laoreet, in vehicula leo volutpat. Duis lacus nisi, lacinia eu ultricies non, ornare eget orci. Integer arcu sem, faucibus quis adipiscing viverra, consectetur ac neque. Phasellus euismod nunc felis, ut egestas erat feugiat eu. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi lobortis tincidunt elit, tempus porta eros ornare a. Aliquam erat volutpat. Sed fringilla consequat tellus sit amet lacinia. Integer euismod suscipit turpis id facilisis. Ut sit amet placerat libero. Pellentesque ornare magna tincidunt, dictum mauris nec, pulvinar velit. Phasellus malesuada ipsum vitae ipsum faucibus, id dictum elit rhoncus. </p>
								</li>
								<li>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi lobortis tincidunt elit, tempus porta eros ornare a. Aliquam erat volutpat. Sed fringilla consequat tellus sit amet lacinia. Integer euismod suscipit turpis id facilisis. Ut sit amet placerat libero. Pellentesque ornare magna tincidunt, dictum mauris nec, pulvinar velit. Phasellus malesuada ipsum vitae ipsum faucibus, id dictum elit rhoncus. Cras non massa volutpat, pellentesque sem eu, sagittis magna. Cras ac commodo neque. In vitae tristique leo. Proin viverra enim at sem laoreet, in vehicula leo volutpat. Duis lacus nisi, lacinia eu ultricies non, ornare eget orci. Integer arcu sem, faucibus quis adipiscing viverra, consectetur ac neque. Phasellus euismod nunc felis, ut egestas erat feugiat eu. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi lobortis tincidunt elit, tempus porta eros ornare a. Aliquam erat volutpat. Sed fringilla consequat tellus sit amet lacinia. Integer euismod suscipit turpis id facilisis. Ut sit amet placerat libero. Pellentesque ornare magna tincidunt, dictum mauris nec, pulvinar velit. Phasellus malesuada ipsum vitae ipsum faucibus, id dictum elit rhoncus. </p>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>