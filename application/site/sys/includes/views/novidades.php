<?php $total = count($novidades) ?>

<?php if($total > 3): ?>
<section class="novidades">
	<div class="container">
		<div class="lista">
			<h2 class="padrao text-center">Novidades</h2>
            <div class="lista-novidades">
                <?php
                foreach($novidades as $novidade) {
                    //$novidade->imagem = $novidade->imagem[0].'_thumb.'.$novidade->imagem[1];
                ?>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="novidade normal <?php echo $novidade->cor ?>">
                        <div class="data-postagem text-center">
                            <?php echo dateFormatBox($novidade->data) ?>
                        </div>
                        <a id="<?php echo $novidade->id ?>" href="#novidade" data-url="<?php echo getLink('novidade/'.$novidade->id.'/'.$novidade->tag) ?>" class="abre-novidade" title="<?php echo $novidade->titulo ?>">
                            <div class="fundo <?php echo $novidade->cor ?>"></div>
                            <img src="<?php echo getUploadedFile('imagens/'.$novidade->imagem) ?>" alt="<?php echo $novidade->titulo ?>" class="img-responsive">
                        </a>
                        <a id="<?php echo $novidade->id ?>" href="#novidade" data-url="<?php echo getLink('novidade/'.$novidade->id.'/'.$novidade->tag) ?>" class="abre-novidade titulo" title="<?php echo $novidade->titulo ?>"><?php echo $novidade->titulo ?></a>
                    </div>
                </div>
                <?php } ?>
            </div>
		</div>
	</div>
</section>
<?php endif; ?>