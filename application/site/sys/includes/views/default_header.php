<body class="<?php if(isset($classe)){ echo $classe; } ?>">
	<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=256441067827746&version=v2.0";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>

<header>
	<section class="barra-topo">
		<div class="container">
			<div class="col-lg-2 col-md-2 col-sm-5 col-xs-8">
				<a class="logo-topo" href="<?php echo base_url() ?>" title="Marchezan 4542 - Deputado Federal - Política sem medo da verdade">
					<img src="<?php echo getImg('deputado-marchezan-logo.png') ?>" alt="Marchezan 4542 - Deputado Federal - Política sem medo da verdade" class="img-responsive">
				</a>
			</div>
			<div class="col-lg-10 col-md-10 hidden-sm hidden-xs">
				<nav>
					<ul>
						<li><a href="#quem-sou" title="Perfil">Perfil</a></li>
						<li><a href="#trabalho-educacao">Educação</a></li>
						<li><a href="#trabalho-saude">Saúde</a></li>
						<li><a href="#trabalho-menos-impostos">Menos Impostos</a></li>
						<li><a href="#trabalho-transparencia">Transparência</a></li>
						<li><a href="#trabalho-combate-a-corrupcao">Combate à Corrupção</a></li>
						<li><a href="#trabalho-seguranca">Segurança</a></li>
						<li><a href="#trabalho-agricultura">Agricultura</a></li>
						<li><a href="#contato" title="Contato">Contato</a></li>
					</ul>
				</nav>
			</div>
			<div class="hidden-lg hidden-md col-sm-3 col-xs-4 text-right">
				<img src="<?php echo getImg('menu-mobile-ico.png') ?>" alt="Clique para abrir o menu" class="menu-mobile">
			</div>
			<div class="menu-mobile">
				<ul>
					<li><a href="#quem-sou" title="Perfil">Perfil</a></li>
					<li><a href="#trabalho-educacao">Educação</a></li>
					<li><a href="#trabalho-saude">Saúde</a></li>
					<li><a href="#trabalho-menos-impostos">Menos Impostos</a></li>
					<li><a href="#trabalho-transparencia">Transparência</a></li>
					<li><a href="#trabalho-combate-a-corrupcao">Combate à Corrupção</a></li>
					<li><a href="#trabalho-seguranca">Segurança</a></li>
					<li><a href="#trabalho-agricultura">Agricultura</a></li>
					<li><a href="#contato" title="Contato">Contato</a></li>
				</ul>
			</div>
		</div>
	</section>
</header>