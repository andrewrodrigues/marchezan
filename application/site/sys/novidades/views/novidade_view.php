<?php $this->load->view('banner_novidades'); ?>

<?php /*<style>
    .novidade {
        display: block;
    }
</style>*/ ?>

<a name="novidade"></a>
<div class="novidade novidade-ajax" id="novidade-<?php echo $novidade->id ?>">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <a href="<?php echo base_url() ?>" class="fechar" id="fechar-novidade-<?php echo $novidade->id ?>"><< Fechar</a>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <?php if(!empty($novidade->imagem)): ?>
                    <?php $novidade->imagem = explode('.', $novidade->imagem); ?>
                    <?php $novidade->imagem = $novidade->imagem[0].'_thumb.'.$novidade->imagem[1]; ?>
                    <figure>
                        <img src="<?php echo getUploadedFile('imagens/'.$novidade->imagem) ?>" alt="<?php $novidade->titulo ?>" class="fullsize-image img-responsive">
                    </figure>
                    <?php endif; ?>
                    <h3><?php echo $novidade->titulo ?></h3>
                    <?php echo $novidade->conteudo ?>
                </div>
                <div class="col-lg-4 col-md-4 hidden-sm hidden-xs sidebar text-right">
                    <a href="#projetos" class="goto" title="Projetos">
                        <img src="<?php echo getImg('projetos-ico.jpg') ?>" alt="Projetos">
                    </a>
                    <!-- <a href="#propostas" class="goto" title="Propostas">
                        <img src="<?php echo getImg('propostas-ico.jpg') ?>" alt="Propostas">
                    </a>
                    <a href="#novidades" class="goto" title="Novidades">
                        <img src="<?php echo getImg('novidades-ico.jpg') ?>" alt="Novidades">
                    </a> -->
                </div>
            </div>
        </div>
    </div>
</div>
<?php $novidades_ajax = $novidades ?>
<?php $this->load->view('novidades_ajax'); ?>

<?php $this->load->view('mini_perfil_redes_sociais'); ?>
<?php $this->load->view('quem_sou') ?>

<?php $this->load->view('novidades'); ?>

<a name="projetos"></a>
<?php $this->load->view('projetos'); ?>





