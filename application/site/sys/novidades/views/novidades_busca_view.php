<section class="historico">
  <div class="container">
    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs">
      <a href="<?php echo getLink(); ?>" title="Home">Home</a> » <a href="<?php echo getLink('novidades'); ?>" title="Novidades">Novidades</a> » Busca
    </div>
  </div>
</section>
<section class="conteudo">
  <div class="container">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <?php if(isset($termos_categoria)){ echo "<h2>".$termos_categoria."</h2>"; }else{ ?>
      <h2>Busca</h2>
      <?php } ?>
    </div>
    <div class="col-lg-4 col-md-4 hidden-sm hidden-xs pull-right sidebar">
      <form action="<?php echo getLink('novidades') ?>" method="get" id="pesquisa">
        <input type="submit" value="" class="busca_botao"/>
        <input type="text" placeholder="buscar" name="busca">
      </form>
      <h2>CATEGORIAS</h2>
      <ul>
        <?php foreach($categorias as $categoria){ ?>
        <?php if(!in_array($categoria->tag, array('artigos', 'livros', 'cartilhas'))): ?>
        <li>
          <a href="<?php echo getLink('novidades?categoria='.$categoria->tag); ?>" title="<?php echo $categoria->nome; ?>">» <?php echo $categoria->nome; ?></a>
          <?php if($categoria->tag == 'publicacoes'): ?>
          <ul>
            <?php foreach($subcategorias as $subcategoria): ?>
            <li><a href="<?php echo getLink('novidades?categoria='.$subcategoria->tag); ?>" title="<?php echo $subcategoria->nome; ?>">» <?php echo $subcategoria->nome;?></a></li>
            <?php endforeach; ?>
          </ul>
          <?php endif; ?>
        </li>
        <?php endif; ?>
        <?php } ?>
      </ul>
      <!--
      <h2>PUBLICAÇÕES</h2>
      <div class="row">
        <div class="col-lg-5 col-md-5 col-sm-6 hidden-xs">
          <img src="" alt="{ULTIMA_PUBLICACAO}" class="img-responsive fullsize-image">
        </div>
        <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
          <span class="tipo">Livro</span>
          <a href="<?php echo getLink(); ?>" class="titulo" title="Muitas Cabeças, Muitas Sentenças - Eduardo Pazinato e Aline Kerber (2014)">
            Muitas Cabeças, Muitas Sentenças - Eduardo Pazinato e Aline Kerber (2014)
          </a>
          <p class="autor">Eduardo</p>
          <a href="<?php echo getLink(); ?>" class="saiba-mais pull-right" title="Saiba mais">Saiba mais »</a>
        </div>
      </div>
      -->
    </div>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 busca-termos">
                <?php if(isset($termos_busca)){ echo "Você pesquisou por: <strong>".$termos_busca."</strong>"; } ?>
                <br /><br />
                <?php if(isset($termos_busca) AND empty($novidades)){ ?>
                Não foi encontrado nenhum resultado com os termos utilizados, por favor, tente com outras palavras.
                <?php } ?>
            </div>
        </div>
        <?php if(isset($termos_categoria) AND empty($novidades)){ ?>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 busca-termos">
                    Nenhuma novidade encontrada.
                </div>
            </div>
        <?php }else{ foreach($novidades as $novidade): ?>
          <div class="row busca-item">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                      <?php if(!empty($novidade->imagem) && file_exists('files/uploads/imagens/'.$novidade->imagem)): ?>
                      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <img src="<?php echo getUploadedFile('imagens/'.$novidade->imagem); ?>" alt="<?php echo $novidade->titulo; ?>" class="img-responsive fullsize-image">
                      </div>
                      <?php endif; ?>
                      <div class="<?php echo !empty($novidade->imagem) && file_exists('files/uploads/imagens/'.$novidade->imagem) ? 'col-lg-9 col-md-9 col-sm-9 col-xs-9' : 'col-lg-12 col-md-12 col-sm-12 col-xs-12' ?> busca-titulo-url">
                         <?php if($novidade->publicacao==1){ ?>
                            <a href="<?php echo getLink('publicacao/'.$novidade->id."/".$novidade->tag); ?>"><?php echo $novidade->titulo; ?></a>
                             <p>
                                 <a href="<?php echo getLink('publicacao/'.$novidade->id."/".$novidade->tag); ?>"><?php echo $novidade->resumo; ?></a>
                             </p>
                          <?php }else{ ?>
                            <a href="<?php echo getLink('novidade/'.$novidade->id."/".$novidade->tag); ?>"><?php echo $novidade->titulo; ?></a>
                             <p>
                                 <a href="<?php echo getLink('novidade/'.$novidade->id."/".$novidade->tag); ?>"><?php echo $novidade->resumo; ?></a>
                             </p>
                          <?php } ?>
                      </div>
                  </div>
              </div>
          </div>
        <?php endforeach; } ?>
        <div class="row">


          <center><div id="paginator-test"></div></center>

          <script type="text/javascript" src="<?php echo getJs('bootstrap-paginator'); ?>"></script>

          <?php if(isset($_GET['categoria'])){ $categoria_page = $_GET['categoria']; } ?>
          <script type="text/javascript">

            var container = $('#paginator-test');

            var options = {
              containerClass:"pagination"
              , currentPage:1
              , numberOfPages: 5
              , totalPages: <?php echo $max_pages; ?>
              , pageUrl:function(type,page){
                return "http://fidedigna_cms.dev/novidades?categoria=<?php echo $categoria_page; ?>&pag="+page;
              }
              , onPageClicked:null
              , onPageChanged:null
            };

            container.bootstrapPaginator(options);


          </script>

        </div>
    </div>
  </div>
</section>

