<section class="historico">
  <div class="container">
    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs">
      <a href="<?php echo getLink(); ?>" title="Home">Home</a> » <a href="<?php echo getLink('novidades'); ?>" title="Novidades">Novidades</a> » Case da Indústria de Aços Gerdau
    </div>
  </div>
</section>
<section class="conteudo">
  <div class="container">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <h2><?php echo $publicacao->titulo; ?></h2>
    </div>
    <div class="col-lg-4 col-md-4 col-12 hidden-xs">
      <figure class="text-center">
        <img src="<?php echo getUploadedFile('imagens/'.$publicacao->imagem); ?>" alt="<?php echo $publicacao->titulo; ?>" class="capa-publicacao">
      </figure>

      <?php if($publicacao->cadastro_download==1){ ?>
      <form action="<?php echo getLink('novidades/email-download'); ?>" id="download" method="post" class="form-padrao">
        <h4 class="padrao">Download do artigo</h4>
          <?php if($this->session->flashdata('form_download_publicacao_sucesso')): ?>
              <div class="alert alert-success alert-block fade in">
                  <p>O link para download foi enviado a você, verifique seu e-mail por favor.</p>
              </div>
          <?php endif; ?>

          <?php if($this->session->flashdata('form_download_publicacao_erro')): ?>
              <div class="alert alert-danger alert-block fade in">
                  <p>Ocorreu um erro nas informações preenchidas neste formulário.</p>
              </div>
          <?php endif; ?>
        <label for="nome" class="padrao">Nome:</label>
        <input type="text" class="padrao" name="nome" id="nome" required>
        <label for="email" class="padrao">E-mail:</label>
        <input type="text" class="padrao" name="email" id="email" required>
        <?php if($publicacao->formulario_download!=""){
            $campos = explode("{",$publicacao->formulario_download);
            if(in_array("Telefone}",$campos)){
                echo '<label for="telefone" class="padrao">Telefone</label>';
                echo '<input type="text" class="padrao" name="telefone" id="telefone" required>';
            }
            if(in_array("Cidade}",$campos)){
                echo '<label for="cidade" class="padrao">Cidade</label>';
                echo '<input type="text" class="padrao" name="cidade" id="cidade" required>';
            }
            if(in_array("Mensagem}",$campos)){
                echo '<label for="mensagem" class="padrao">Mensagem</label>';
                echo '<textarea name="mensagem" id="mensagem" class="padrao" required></textarea>';
            }

        } ?>
        <input type="hidden" name="arquivo_download" value="<?php echo $publicacao->arquivo_download; ?>"/>
        <input type="hidden" name="publicacao_id" value="<?php echo $publicacao->id; ?>"/>
        <input type="hidden" name="email_download" value="true"/>
        <input type="hidden" name="publicacao_url" value="<?php echo $publicacao->tag; ?>"/>
        <div style="visibility: hidden;">
            <input type="text" name="fatordigital" value=""/>
        </div>
        <input type="submit" class="btn btn-padrao pull-right" value="Enviar">
      </form>
      <?php }else{ ?>
          <?php if($publicacao->arquivo_download!=""){ ?>
          <p><strong>Arquivo disponível para download:</strong></p>
          <p><a href="<?php echo getLink('novidades/download-agora/'.$publicacao->arquivo_download); ?>">Baixar arquivo <?php echo $publicacao->arquivo_download; ?></a></p>
          <?php } ?>
      <?php } ?>

    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
      <div class="date-time">
        <!-- <span><img src="<?php echo getImg('data-icone.jpg'); ?>" alt="<?php echo novidade_date($publicacao->creation); ?>"> 26 Jun, 2014</span> -->
        <!--<span><img src="<?php echo getImg('usuario-icone.jpg') ?>" alt="Eduardo Pazinato">Eduardo Pazinato</span>-->
      </div>
      <div class="conteudo-novidade">
        <?php echo $publicacao->conteudo; ?>
      </div>
    </div>
    <div class="col-lg-4 col-md-4 hidden-sm hidden-xs pull-right sidebar">
        <form action="<?php echo getLink('novidades') ?>" method="get" id="pesquisa">
            <input type="submit" value="" class="busca_botao"/>
            <input type="text" placeholder="buscar" name="busca">
        </form>
        <h2>CATEGORIAS</h2>
        <ul>
            <?php foreach($categorias as $categoria){ ?>
                <?php if(!in_array($categoria->tag, array('artigos', 'livros', 'cartilhas'))): ?>
                <li>
                    <a href="<?php echo getLink('novidades?categoria='.$categoria->tag); ?>" title="<?php echo $categoria->nome; ?>">» <?php echo $categoria->nome; ?></a>
                    <?php if($categoria->tag == 'publicacoes'): ?>
                    <ul>
                      <?php foreach($subcategorias as $subcategoria): ?>
                      <li><a href="<?php echo getLink('novidades?categoria='.$subcategoria->tag); ?>" title="<?php echo $subcategoria->nome; ?>">» <?php echo $subcategoria->nome;?></a></li>
                      <?php endforeach; ?>
                    </ul>
                    <?php endif; ?>
                </li>
              <?php endif; ?>
            <?php } ?>
        </ul>
        <!--<h2>PUBLICAÇÕES</h2>
      <div class="row">
        <div class="col-lg-5 col-md-5 col-sm-6 hidden-xs">
          <img src="<?php echo getImg('capa-livro-preview.jpg'); ?>" alt="Muitas Cabeças, Muitas Sentenças - Eduardo Pazinato e Aline Kerber (2014)" class="img-responsive fullsize-image">
        </div>
        <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
          <span class="tipo">Livro</span>
          <a href="<?php echo getLink(); ?>" class="titulo" title="Muitas Cabeças, Muitas Sentenças - Eduardo Pazinato e Aline Kerber (2014)">
            Muitas Cabeças, Muitas Sentenças - Eduardo Pazinato e Aline Kerber (2014)
          </a>
          <p class="autor">Eduardo</p>
          <a href="<?php echo getLink(); ?>" class="saiba-mais pull-right" title="Saiba mais">Saiba mais »</a>
        </div>

      </div>-->
    </div>
  </div>
</section>