<h2 class="padrao text-center">A Campanha</h2>
<div class="lista-novidades">
    <?php
    $i = 1;
    foreach($novidades as $novidade) {
        //echo $i . '-';
        if($i === 1 || $i === 10) {
            ?>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="novidade destaque <?php echo $novidade->cor ?>">
                    <div class="data-postagem text-center">
                        <?php echo dateFormatBox($novidade->data) ?>
                    </div>
                    <a id="<?php echo $novidade->id ?>" href="<?php echo getLink('novidade/'.$novidade->id.'/'.$novidade->tag) ?>" class="abre-novidade" title="<?php echo $novidade->titulo ?>">
                        <img src="<?php echo getUploadedFile('imagens/'.$novidade->imagem) ?>" alt="<?php echo $novidade->titulo ?>" class="img-responsive fullsize-images">
                    </a>
                    <a id="<?php echo $novidade->id ?>" href="<?php echo getLink('novidade/'.$novidade->id.'/'.$novidade->tag) ?>" class="abre-novidade titulo" title="<?php echo $novidade->titulo ?>"><?php echo $novidade->titulo ?></a>
                </div>
            </div>
            <?php
        } else {
            if($i === 6) {
                ?>
            <div class="clear"></div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="row">
                <?php
            }
            if($i === 6 || $i === 7 || $i === 8 || $i ===9) {
                ?>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <?php
            } else {
                ?>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <?php
            }
            ?>  <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="novidade normal <?php echo $novidade->cor ?>">
                                <div class="data-postagem text-center">
                                    <?php echo dateFormatBox($novidade->data) ?>
                                </div>
                            </a>
                            <a id="<?php echo $novidade->id ?>" href="<?php echo getLink('novidade/'.$novidade->id.'/'.$novidade->tag) ?>" class="abre-novidade fixed" title="<?php echo $novidade->titulo ?>"></a>
                            <a id="<?php echo $novidade->id ?>" href="<?php echo getLink('novidade/'.$novidade->id.'/'.$novidade->tag) ?>" class="abre-novidade titulo" title="<?php echo $novidade->titulo ?>"><?php echo $novidade->titulo ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            if($i === 9) {
                ?>
                </div></div>
                <?php
            }
        }
        $i++;
    }

    ?>
</div>