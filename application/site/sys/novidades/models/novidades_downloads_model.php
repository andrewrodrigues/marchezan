<?php

class novidades_downloads_model extends FD_Model {

  function __construct() {
    parent::__construct();
  }

  public function insertDownload($data){
      if($this->db->insert('fd_marchezan_novidades_downloads',$data)){
          return true;
      }else{
          return false;
      }
  }

  public function updateDownload($id,$data){
      $this->db->where('id',$id);
      if($this->db->update('fd_marchezan_novidades_downloads',$data)){
          return true;
      }else{
          return false;
      }
  }

  public function getDownloadFile($hash){
      $this->db->select('id,arquivo_download,download_status');
      $this->db->where('fd_marchezan_novidades_downloads.hash_download',$hash);
      return $this->db->get('fd_marchezan_novidades_downloads')->row();
  }



} 