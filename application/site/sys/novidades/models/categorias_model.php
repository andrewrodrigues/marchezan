<?php

class categorias_model extends FD_Model {

  function __construct() {
    parent::__construct();
  }

  public function getAllCategorias(){
    $this->db->order_by('fd_marchezan_categorias.nome','ASC');
    return $this->db->get('fd_marchezan_categorias')->result();
  }

  public function getCategoriaNome($tag){
      $this->db->select('fd_marchezan_categorias.nome');
      $this->db->where('fd_marchezan_categorias.tag',$tag);
      return $this->db->get('fd_categorias')->row();
  }

  public function getSubcategoriaPublicacoes($in){
    $this->db->order_by('fd_marchezan_categorias.nome','ASC');
    $this->db->where_in('id', $in);
    return $this->db->get('fd_marchezan_categorias')->result();
  }

}