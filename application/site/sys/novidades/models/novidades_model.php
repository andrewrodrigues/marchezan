<?php

class novidades_model extends FD_Model {

  function __construct() {
    parent::__construct();
  }

  public function getAllNovidades($limit='12',$tipo='all',$notWhare=null){
    if($tipo=='publicacoes'){ $this->db->where('fd_marchezan_novidades.publicacao','1');}
    if($tipo=='novidades'){ $this->db->where('fd_marchezan_novidades.publicacao','0');}
    if(!empty($notWhare)) $this->db->where('fd_marchezan_novidades.id !=', $notWhare);
    $this->db->order_by('fd_marchezan_novidades.id','DESC');
    $this->db->order_by('fd_marchezan_novidades.data','DESC');
    $this->db->where('fd_marchezan_novidades.status','1');
    $this->db->limit((int)$limit);
    return $this->db->get('fd_marchezan_novidades')->result();
  }

  public function getNovidadeByTag($tag){
    $this->db->where('fd_marchezan_novidades.tag',$tag);
    return $this->db->get('fd_marchezan_novidades')->row();
  }

  public function getNovidadeById($id){
    $this->db->where('fd_marchezan_novidades.id',$id);
    return $this->db->get('fd_marchezan_novidades')->row();
  }

  public function countResultados($termos=null,$categoria=null){
    $query = "SELECT COUNT(fd_marchezan_novidades.id) as total FROM fd_marchezan_novidades";
    if($categoria!=null){
      $query.= " INNER JOIN fd_marchezan_categorias ON fd_marchezan_novidades.categoria_id = fd_marchezan_categorias.id";
      $query.= " WHERE fd_categorias.tag='".$categoria."'";
    }
    if($termos!=null){
      if($categoria==null){ $query.=" WHERE "; }else{ $query.=" AND "; }
      $query.= "fd_marchezan_novidades.titulo LIKE '%".$termos."%' ";
      $query.= "OR fd_marchezan_novidades.conteudo LIKE '%".$termos."%'";
    }
    return $this->db->query($query)->row();
  }

  public function searchNovidade($termos=null,$categoria=null,$limit_start=0,$limit=10){
    $query = "SELECT fd_marchezan_novidades.* FROM fd_novidades";
    if($categoria!=null){
        $query.= " INNER JOIN fd_marchezan_categorias ON fd_marchezan_novidades.categoria_id = fd_marchezan_categorias.id";
        $query.= " WHERE fd_categorias.tag='".$categoria."'";
    }
    if($termos!=null){
        if($categoria==null){ $query.=" WHERE "; }else{ $query.=" AND "; }
        $query.= "fd_marchezan_novidades.titulo LIKE '%".$termos."%' ";
        $query.= "OR fd_marchezan_novidades.conteudo LIKE '%".$termos."%'";
    }
    if($limit!=null ){
      $query.= " LIMIT ".$limit_start.",".$limit;
    }
    return $this->db->query($query)->result();
  }



} 