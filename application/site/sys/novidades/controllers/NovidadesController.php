<?php

class NovidadesController extends FD_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('novidades_model');
        $this->load->model('novidades_downloads_model');
        $this->load->model('categorias_model');
    }

    public function index(){
        $data['novidades'] = $this->novidades_model->getAllNovidades();
        $data['banner'] = $this->novidades_model->getAllNovidades(3);
        $this->setContent('novidades_view', $data);
    }

    public function ajax(){
        $data['novidades'] = $this->novidades_model->getAllNovidades();
        $this->setContent('ajax_view', $data);
    }

    public function novidade_ajax($id,$tag = null){

        $this->load->model('novidades_model');
        $data['novidade'] = $this->novidades_model->getNovidadeById($id);

        if(empty($data['novidade'])){
            redirect(getLink('pagina-nao-encontrada'));
        }

        $this->load->model('categorias_model');
        $data['categorias'] =  $this->categorias_model->getAllCategorias();
        $data['subcategorias'] = $this->categorias_model->getSubcategoriaPublicacoes(array(4, 5, 6));

        $this->setContent('novidade_ajax_view',$data);
    }    

    public function novidade($id,$tag){

        $this->load->model('novidades_model');
        $data['novidade'] = $this->novidades_model->getNovidadeById($id);

        if(empty($data['novidade'])){
            redirect(getLink('pagina-nao-encontrada'));
        }

        $this->load->model('categorias_model');
        $data['categorias'] =  $this->categorias_model->getAllCategorias();
        $data['subcategorias'] = $this->categorias_model->getSubcategoriaPublicacoes(array(4, 5, 6));

        $head['title'] = 'Deputado Marchezan';
        $head['css'] = array('bootstrap','style','metrojs');
        $this->setHead('default_head',$head);

        $header['classe'] = 'body-novidade';
        $this->setHeader('default_header', $header);

        $data['banner'] = $this->novidades_model->getAllNovidades(3);

        $data['novidades'] = $this->novidades_model->getAllNovidades();

        $data['novidades_ajax'] = $this->novidades_model->getAllNovidades(12,'all',$id);

        $this->setContent('novidade_view', $data);

        $footer['js'] = array('jquery','jquery-ui','metrojs','plugins','projetos','biografia','novidades','main',);
        $this->setFooter('default_footer', $footer);

        
    }

    public function publicacao($id,$tag){

        $this->load->model('novidades_model');
        $data['publicacao'] = $this->novidades_model->getNovidadeById($id);

        if(empty($data['publicacao'])){
            redirect(getLink('pagina-nao-encontrada'));
        }

        $this->load->model('categorias_model');
        $data['categorias'] =  $this->categorias_model->getAllCategorias();
        $data['subcategorias'] = $this->categorias_model->getSubcategoriaPublicacoes(array(4, 5, 6));

        $head['title'] = $data['publicacao']->titulo;
        $this->setHead('default_head',$head);

        $header['title_interna'] = '';
        $header['body_class'] = 'novidades';
        $this->setHeader('default_header',$header);

        $this->setContent('publicacao_view',$data);

        $this->setFooter('default_footer');
    }

    public function download($params){
        ini_set('allow_url_fopen',true);
        $this->load->model('novidades_downloads_model');
        $download = $this->novidades_downloads_model->getDownloadFile($params);

        if($download AND $download->download_status==0){
            $this->novidades_downloads_model->updateDownload($download->id,array('download_status'=>1));
        }

        $this->load->helper('download');

        $data = file_get_contents(getUploadedFile('arquivos/'.$download->arquivo_download));
        $name = $download->arquivo_download;

        force_download($name, $data);
        redirect(getLink());
    }

    public function download_agora($params){
        ini_set('allow_url_fopen',true);
        $this->load->helper('download');

        $data = file_get_contents(getUploadedFile('arquivos/'.$params));
        $name = $params;

        force_download($name, $data);
        redirect(getLink());
    }

    public function email_download(){
        if($this->input->post('email_download') && $this->input->post('fatordigital')==""){

            $this->load->model('novidades_downloads_model');

            $conteudo_email = '<strong>Nome:</strong> '.$this->input->post('nome').'<br />';
            $conteudo_email.= '<strong>Email:</strong> '.$this->input->post('email').'<br />';
            if($this->input->post('telefone')){
                $conteudo_email.= '<strong>Telefone:</strong> '.$this->input->post('telefone').'<br />';
            }
            if($this->input->post('cidade')){
                $conteudo_email.= '<strong>Cidade:</strong> '.$this->input->post('cidade').'<br />';
            }
            if($this->input->post('mensagem')){
                $conteudo_email.= '<strong>Mensagem:</strong> '.$this->input->post('mensagem'.'<br /><br />');
            }


            $data_insert = array(
                'email' => $this->input->post('email'),
                'conteudo_email' => $conteudo_email,
                'arquivo_download' => $this->input->post('arquivo_download'),
                'publicacao_id' => $this->input->post('publicacao_id'),
                'publicacao_url' => $this->input->post('publicacao_url'),
                'hash_download' => md5($this->input->post('email')),
                'download_status' => 0
            );

            $this->novidades_downloads_model->insertDownload($data_insert);

            require_once(APPPATH.'/libraries/phpMailer/class.phpmailer.php');
            $mail = new PHPMailer();

            $mail->IsSMTP(); // Define que a mensagem será SMTP
            $mail->Host = "smtp.mandrillapp.com:587"; // Endereço do servidor SMTP
            $mail->SMTPAuth = true; // Usa autenticação SMTP? (opcional)
            $mail->Username = 'mandrill@fatordigital.com.br'; // Usuário do servidor SMTP
            $mail->Password = '_eRTjGQpJMAaixfXBoApZw'; // Senha do servidor SMTP

            $mail->From = "nao-responder@ifidedigna.com.br"; // Seu e-mail
            $mail->FromName = "Instituto Fidedigna"; // Seu nome

            $mail->AddAddress($this->input->post('email'), 'Instituto Fidedigna');

            $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
            $mail->CharSet = 'UTF-8'; // Charset da mensagem (opcional)

            $mail->Subject  = 'Download de Publicação - Instituto Fidedigna'; // Assunto da mensagem

            $msg = "<img src='".getImg('instituto-fidegina-logo.jpg')."' /><br /><br />";
            $msg.= "Olá <strong>".$this->input->post('nome')."</strong><br /><br />";
            $msg.= "Foi solicitado o download do arquivo da publicação <a href='".getLink('publicacao/'.$data_insert['publicacao_id'].'/'.$data_insert['publicacao_url'])."' target='blank'>(veja a publicação aqui)</a>.<br /><br />";
            $msg.= "Segue o link para download do mesmo :<br /><br />";
            $msg.= "<a href='".getLink('novidades/download/'.$data_insert['hash_download'])."' target='blank'>Baixar o Arquivo</a>";
            $msg.= "<br /><br />Obrigado,<br /><br />";
            $msg.= "Instituto Fidedigna";

            $mail->Body = $msg;

            if($mail->Send()){
                $this->session->set_flashdata('form_download_publicacao_sucesso',true);
                redirect(getLink("publicacao/".$this->input->post('publicacao_id').'/'.$this->input->post('publicacao_url')."#download"));
            }

            $mail->ClearAllRecipients();
            $mail->ClearAttachments();
        }else{
            $this->session->set_flashdata('form_download_publicacao_erro',true);
            redirect(getLink("publicacao/".$this->input->post('publicacao_id').'/'.$this->input->post('publicacao_url')."#download"));
        }
    }

}
