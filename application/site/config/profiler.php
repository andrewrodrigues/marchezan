<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * MK CodeIgniter
 * fb.com/mkethur
 *
 * profiler.php
 *
 * Version: 1.0.0
 *
 * Date: 27 / 02 / 2014
 */

/*
| -------------------------------------------------------------------------
| Profiler Sections
| -------------------------------------------------------------------------
| This file lets you determine whether or not various sections of Profiler
| data are displayed when the Profiler is enabled.
| Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/profiling.html
|
*/


/* End of file profiler.php */
/* Location: ./application/config/profiler.php */