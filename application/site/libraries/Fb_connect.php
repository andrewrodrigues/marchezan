<?php

include APPPATH . 'libraries/facebook-php-sdk/src/facebook.php';

class Fb_connect extends Facebook
{
	public $user = null;
	public $userId = false;
	public $fb = false;
	public $fbSession = false;
	public $appKey = 0;

	public function __construct()
	{
		$CI = &get_instance();
		$CI->config->load('facebook', true);
		$config = $CI->config->item('facebook');

		parent::__construct($config);

		$this->userId = $this->getUser();

		$me = null;

		if($this->userId)
		{
			try
			{
				$me = $this->api('/me/feed');
				$this->user = $me;
			}
			catch (FacebookApiException $e)
			{
				error_log($e);
			}
		}
	}

	public function getPost()
	{
		return $this->api('/189721867820491/feed');
	}
}