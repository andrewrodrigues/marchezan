<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * MK CodeIgniter
 * fb.com/mkethur
 *
 * debug_helper.php
 *
 * Version: 1.0.0
 *
 * Date: 27 / 02 / 2014
 */


/**
 * debugVarAndDie
 * Retorna o debug da varíavel parametro e executa o die()
 *
 * @param $var Variavel
 */
function debugVarAndDie($var) {
  print('<pre>');
  var_dump($var);
  print('</pre>');
  die();
}



?>
