<?php

function dateFormatBox($date)
{
    list($ano, $mes, $dia) = explode('-', $date);
    $meses = array(
        '01' => 'janeiro',
        '02' => 'fevereiro',
        '03' => 'março',
        '04' => 'abril',
        '05' => 'maio',
        '06' => 'junho',
        '07' => 'julho',
        '08' => 'agosto',
        '09' => 'setembro',
        '10' => 'outubro',
        '11' => 'novembro',
        '12' => 'dezembro',
    );

    return '<span class="dia">'.$dia.'</span><span class="mes">'.$meses[$mes].'</span>';
}