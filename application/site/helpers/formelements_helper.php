<?php
/**
 * MK CodeIgniter
 * fb.com/mkethur
 *
 * formselements_helper.php
 *
 * Version: 1.0.0
 *
 * Date: 27 / 02 / 2014
 */

/**
 * Input_BooleanToInt
 * 
 * Transforma o valor de um input de True/False para 1/0.
 * 
 * @param boolean $data Valor True ou False.
 * @return string Retorna 1 se True, 0 se False.
 */
function Input_BooleanToInt($data){
  if($data==false){
    return '0';    
  }else{
    return '1';
  }
}

/**
 * checkSelectedbyPost
 * 
 * Pega o valor vindo do $_POST de um determinado Select e marca a option como selected.
 * (Função: Manter o úlimo campo selecionado mesmo após o $_POST).
 * 
 * @param string $select_name Nome do form element Select.
 * @param string $option Value do option do form element Select.
 * @return string Retorna selected="selected" no option do form element Select enviado via $_POST
 */
function checkSelectedbyPost($select_name,$option){
  if(!empty($_POST[$select_name])){
    if($_POST[$select_name]==$option){
      return 'selected="selected"';
    }
  }else{
    return '';
  }  
}


?>
