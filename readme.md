![FatorCMS](http://www.fatordigital.com.br/logo_fatorcms_234x55.png "FatorCMS")

-------
### Sobre
Web Content Management System desenvolvido em base do framework CodeIgniter 2.1.4 (Ellis Lab). 

----
### Instalação
- 1 . Configuração da URL base __function base_url()__ para os ambientes "localhost" (development) e "online" (production);
```sh
{root}/index.php - define('BASE_URL_DEV', 'url_local');
{root}/index.php - define('BASE_URL_PROD', 'url_publica');
```

- 2 . Configuração da conexão com o banco de dados
```sh
{root}/index.php  -  DB_CONFIG_DEV para development e DB_CONFIG_PROD para production.
array(
 'hostname'=> '', // endereço do servidor
 'port'=> '', // porta, padrao mysql 3306
 'username'=> '', // usuario
 'password'=> '', // senha
 'database'=> '' // nome do banco de dados
)
```

- 3 . Configuração de ambiente de desenvolvimento
```sh
// development : Debug padrão PHP e relatórios de erros dos sistema, ligados.
// production : Todos debugs e relatórios de erros ocultos.
{root}/index.php  -  define('ENVIRONMENT', 'nome_ambiente');
```

---   
### Documentação
Em desenvolvimento.

---
### Versão
FatorCMS 1.1.2

---
### Desenvolvedor
FatorDigital - _Markus Ethur_ ![Markus Ethur](http://s4.postimg.org/cxm9uh58p/546872_4085536257260_8036878_n.jpg)

